
## rollup 和 webpack 打包方式
> 参考：[为什么组件库打包用 Rollup 而不是 Webpack?](https://mp.weixin.qq.com/s/Eunt2-RVSx58DGDwW9WevA)

### rollup

- 它是基于 es module 的打包工具

- 它打包产物没有 runtime 代码，更简洁纯粹，能打包出 esm、cjs、umd 的产物，常用来做 js 库、组件库的打包。

- rollup 只有 plugin，没有 loader，因为它的 transform 方法就相当于 webpack 插件的 loader。

- vite 就是基于 rollup 来实现的，开发环境用 rollup 插件的 transform 来做代码转换，生产环境用 rollup 打包


