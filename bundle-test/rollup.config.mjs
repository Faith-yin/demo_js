// node >= 16

// @type 是 jsdoc 的语法，也就是 ts 支持的在 js 里声明类型的方式, @type 是 jsdoc 的语法，也就是 ts 支持的在 js 里声明类型的方式
/** @type {import("rollup").RollupOptions} */
export default {
  input: 'src/index.js',
  // 定产物的模块规范有 es module、commonjs、umd 三种
  output: [
    {
      file: 'dist-r/es/esm.js',
      format: 'esm'
    },
    {
      file: 'dist-r/lib/cjs.js',
      format: 'cjs'
    },
    {
      file: 'dist-r/dist/umd.js',
      format: 'umd',
      name: 'Yin'
    }
  ],
}
