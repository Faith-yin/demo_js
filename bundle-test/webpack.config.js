const path = require('path')

/** @type {import("webpack").Configuration} */
module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname, './src/index.js'),
  output: {
    path: path.resolve(__dirname, 'dist-w'),
    clean: true,
  },
}
