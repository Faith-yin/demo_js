import { BadRequestException, Body, Controller, Get, Inject, Post, Query, Req, UnauthorizedException } from '@nestjs/common';
import { AppService } from './app.service';
import { UserDto } from './dto/user.dto'
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express'

const users = [
  { username: 'yyy', password: '111', email: 'xxx@xxx.com'},
  { username: 'yyy2', password: '222', email: 'yyy@yyy.com'},
]

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // 注入 JwtService (nest 的依赖注入功能), 用这个 jwtService 生成 access_token 和 refresh_token
  @Inject(JwtService)
  private jwtSerive: JwtService

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('login')
  login(@Body() userDto: UserDto) {
    const user = users.find(el => el.username === userDto.username)
    if (!user) throw new BadRequestException('用户不存在')

    if (user.password !== userDto.password) throw new BadRequestException('密码错误')

    const accessToken = this.jwtSerive.sign(
      {
        username: user.username,
        email: user.email
      },{
        expiresIn: '0.5h'
      }
    )

    const refreshToken = this.jwtSerive.sign(
      {
        username: user.username
      },{
        expiresIn: '7d'
      }
    )

    return {
      userInfo: {
        username: user.username,
        email: user.email
      },
      accessToken,
      refreshToken
    }
  }

  @Get('test')
  test(@Req() req: Request) {
    const authorization = req.headers['authorization']
    if (!authorization) throw new UnauthorizedException('用户未登录')
    
    try {
      const token = authorization.split(' ')[1]
      const data = this.jwtSerive.verify(token)
      return 'success'
    } catch (error) {
      throw new UnauthorizedException('token 失效，请重新登录')
    }
  }

  @Get('refresh')
  refresh(@Query('token') token: string) {
    try {
      const data = this.jwtSerive.verify(token)
      const user = users.find(el => el.username === data.username)

      const accessToken = this.jwtSerive.sign(
        {
          username: user.username,
          email: user.email
        },{
          expiresIn: '0.5h'
        }
      )
  
      const refreshToken = this.jwtSerive.sign(
        {
          username: user.username
        },{
          expiresIn: '7d'
        }
      )

      return {
        accessToken,
        refreshToken
      }
    } catch (error) {
      throw new UnauthorizedException('token 失效，请重新登录')
    }
  }




}
