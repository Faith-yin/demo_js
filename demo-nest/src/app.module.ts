import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
// 引入 jwt 模块来生成 token
import { JwtModule } from '@nestjs/jwt'

@Module({
  imports: [
    // 注册下这个模块
    JwtModule.register({
      secret: 'ypf'
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
