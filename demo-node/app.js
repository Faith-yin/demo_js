const express = require("express")
const multer = require("multer")
const path = require("path")

const app = express()

// 自定义存储引擎
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/") // 文件保存目录
  },
  filename: (req, file, cb) => {
    const filename = file.originalname // 获取原始文件名
    const ext = path.extname(file.originalname) // 获取文件后缀
    cb(null, `${filename}_${Date.now()}${ext}`) // 组合文件名：唯一标识 + 后缀
  },
})
const upload = multer({ storage })

app.post("/api/upload", upload.single("file"), (req, res) => {
  if (!req.file) {
    return res.status(400).json({ message: "文件上传失败" })
  }

  const fileInfo = {
    filename: req.file.filename,
    path: req.file.path,
    size: req.file.size, // 文件大小（字节数）
    mimetype: req.file.mimetype,
  }

  res.json({ message: "文件上传成功", file: fileInfo })
})

app.listen(3000, () => {
  console.log("服务器运行在 http://localhost:3000")
})
