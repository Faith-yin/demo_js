// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: {
    enabled: true,
  },
  modules: ['@nuxt/ui'],
  extends: ['@nuxt/ui-pro'],
  app: {
    // 配置全局的头部信息
    head: {
      title: 'Nuxt App',
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      meta: [
        { name: 'description', content: '描述信息：这是一个 Nuxt demo 项目...' },
        { name: 'keywords', content: '关键词信息：Nuxt,NuxtUI Pro,Vue.js...' },
      ],
    },
  },
  experimental: {
    // 内置默认过渡效果
    viewTransition: true
  },
  runtimeConfig: {
    // 在客户端，只有 runtimeConfig.public 中的键才可用，并且该对象是可写和响应式的。
    public: {
      appTitle: '',
    }
  }
})
