
export default defineEventHandler(async (event) => {
    const id = getRouterParam(event, 'id')
    const res = await $fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
    return res
})
