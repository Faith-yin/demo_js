/**
 * 中间件处理程序，会应用在每一个api路由之前，它可以用来增加路由检测、增加请求header信息或者记录请求日志，
 * 也可以用来扩展event请求对应
 */
export default defineEventHandler((event) => {
    console.log('New request: ' + event) // 打印请求日志
})
