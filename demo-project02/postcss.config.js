/*
 * @Date: 2020-12-21 18:15:32
 * @information: postcss配置
 */

module.exports = {
  plugins: {
    "autoprefixer": {
      overrideBrowserslist: [
        "Android 4.1",
        "iOS 7.1",
        "Chrome > 31",
        "ff > 31",
        "ie >= 8"
      ]
    },
    "postcss-pxtorem": {
      rootValue: 16, // 基准尺寸
      propList: ['*'], // 属性选择器，*通用
      "exclude": /node_modules/
    }
  }

}
// module.exports = {
//   plugins: {
//     'autoprefixer': {browsers: 'last 5 version'}
//   }
// }
