/*
 * @Date: 2021-06-12 17:20:17
 * @information: axios配置
 */
import axios from 'axios'

// axios.defaults.baseURL = '/api' // 因为 vue.config.js 中设置了 /api 的代理

axios.interceptors.request.use(
  config => {
    const accessToken = localStorage.getItem('accessToken')
    if (accessToken) {
      config.headers.authorization = 'Bearer ' + accessToken
    }
    return config
  }
)

let refreshing = false
const queue = []

axios.interceptors.response.use(
  response => {
    return response.data || response
  },
  async (error) => {
    if (!error.response) return error
    const { data, config } = error.response

    if (refreshing) {
      // 存 config 和 resolve 是为了恢复原来的业务，因为是拦截器里面 return 了 promise ，所以原来的业务会一直等着，直到调用 resolve
      return new Promise((resolve) => {
        queue.push({
          config,
          resolve
        })
      })
    }

    if (data.statusCode === 401 && !config.url.includes('/refresh')) {
      refreshing = true
      
      const res = await refreshToken()
      if (res.status === 200) {

        queue.forEach(({config, resolve}) => {
          // 调用resolve返回之前的请求业务
          resolve(axios(config))
          // Promise.resolve(axios(config))
        })

        return axios(config)
      } else {
        alert('登录过期，请重新登录')
      }
    } else {
      return Promise.reject(error)
    }
  }
)

async function refreshToken () {
  const res = await axios.get('http://localhost:3000/refresh', {
    params: {
      token: localStorage.getItem('refreshToken')
    }
  })
  if (res.status === 200) {
    localStorage.setItem('accessToken', res.data.accessToken)
    localStorage.setItem('refreshToken', res.data.refreshToken)
  }
  return res
}

