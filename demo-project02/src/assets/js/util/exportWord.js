import JSZipUtils from "jszip-utils";
import docxtemplater from "docxtemplater";
import { saveAs } from "file-saver";
import PizZip from "pizzip";
// import PizZipUtils from "pizzip/utils"
import ImageModule from "docxtemplater-image-module-free";

/**
 * docxtemplater 语法：
 *
 * {%img} 图片
 * {#list}{/list} 循环、if判断
 * {#list}{/list}{^list}{/list} if else
 * {str} 文字
 */

/**
 * 按照模板导出 word
 * @param docUrl string 模板路径
 * @param docData Object 数据
 * @param fileName string 文件名称
 */
export const exportWord = (docUrl, docData, fileName) => {
  JSZipUtils.getBinaryContent(docUrl, (error, content) => {
    if (error) throw error;

    // 创建一个PizZip实例
    const zip = new PizZip(content);
    // 初始化图片配置
    const imageModule = new ImageModule(imageOpts);
    // 创建 docxtemplater 实例对象
    const doc = new docxtemplater();
    // 使用模块
    doc.attachModule(imageModule);
    // 加载实例
    doc.loadZip(zip).compile();

    // 去除未定义值所显示的 undefined
    doc.setOptions({
      nullGetter(part) {
        return "";
      },
    });
    // 设置模板变量的值，对象的键需要和模板上的变量名一致，值就是放在模板上的值
    doc.setData({ ...docData });

    try {
      // 用模板变量的值替换所有模板变量
      doc.render();
    } catch (error) {
      const e = {
        message: error.message,
        name: error.name,
        stack: error.stack,
        properties: error.properties,
      };
      console.error("err", { error: e });
      // 当使用json记录时，此处抛出错误信息
      throw error;
    }

    // 生成一个代表 docxtemplater 对象的 zip 文件（位于内存中）
    const out = doc.getZip().generate({
      type: "blob",
      mimeType:
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    });
    saveAs(out, fileName);
  });
};

const imageOpts = {
  getImage: function(tagValue, tagName) {
    // return new Promise(function (resolve, reject) {
    //   PizZipUtils.getBinaryContent(tagValue, function (error, content) {
    //     if (error) {
    //       return reject(error)
    //     }
    //     return resolve(content)
    //   })
    // })

    return base64DataURLToArrayBuffer(tagValue)
  },
  getSize: function(img, tagValue, tagName) {
    const sizeObj = getImgSize(tagValue)
    return [sizeObj.width, sizeObj.height]
  },
};

// 将base64 转成 ArrayBuffer
function base64DataURLToArrayBuffer(dataURL) {
  const base64Regex = /^data:image\/(png|jpg|svg|svg\+xml);base64,/;
  if (!base64Regex.test(dataURL)) {
    return false
  }
  const stringBase64 = dataURL.replace(base64Regex, "")
  let binaryString;
  if (typeof window !== "undefined") {
    binaryString = window.atob(stringBase64)
  } else {
    binaryString = new Buffer(stringBase64, "base64").toString("binary")
  }
  const len = binaryString.length
  const bytes = new Uint8Array(len)
  for (let i = 0; i < len; i++) {
    const ascii = binaryString.charCodeAt(i)
    bytes[i] = ascii
  }
  return bytes.buffer
}

function getImgSize(base64) {
  //确认处理的是png格式的数据
  if (base64.substring(0, 22) === "data:image/png;base64,") {
    // base64 是用四个字符来表示3个字节
    // 我们只需要截取base64前32个字符(不计开头那22个字符)便可（24 / 3 * 4）
    // 这里的data包含12个字符，9个字节，除去第1个字节，后面8个字节就是我们想要的宽度和高度
    const data = base64.substring(22 + 20, 22 + 32);
    const base64Characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    const nums = [];
    for (const c of data) {
      nums.push(base64Characters.indexOf(c));
    }
    const bytes = [];
    for (let i = 0; i < nums.length; i += 4) {
      bytes.push((nums[i] << 2) + (nums[i + 1] >> 4));
      bytes.push(((nums[i + 1] & 15) << 4) + (nums[i + 2] >> 2));
      bytes.push(((nums[i + 2] & 3) << 6) + nums[i + 3]);
    }
    const width =
      (bytes[1] << 24) + (bytes[2] << 16) + (bytes[3] << 8) + bytes[4];
    const height =
      (bytes[5] << 24) + (bytes[6] << 16) + (bytes[7] << 8) + bytes[8];
    return {
      width,
      height,
    };
  }
  throw Error("unsupported image type");
}
