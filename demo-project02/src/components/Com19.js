import { Table } from 'element-ui'

const TablePatched = {
  extends: Table,
  data() {
    return {
      username: 'yyy',
      age: 18
    }
  },
  mounted() {
    console.log('TablePatched', )
  },
  methods: {
    fn () {
      console.log('1111', )
    }
  },
}

export default {
  install (Vue) {
    Vue.component(Table.name, TablePatched)
  }
}
