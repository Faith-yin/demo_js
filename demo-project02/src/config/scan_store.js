/*
 * @Date: 2021-10-30 16:19:16
 * @information:
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let model = {
  modules: {}
}

const files = require.context('@/store', true, /.js$/)

files.keys().forEach(el => {
  const name = el.split('/').pop().replace(/\.js$/, '')
  if (name !== 'index') {
    model.modules[name] = files(el).default
  }
})

export default new Vuex.Store(model)


