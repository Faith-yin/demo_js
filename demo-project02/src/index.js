/**
 * @create 2025-02-21 20:22:01 星期五
 * @desc 加密压缩混淆vue组件代码，防止被直接查看源代码
 * 
 * 步骤：
 * 1. 执行附注命令，生成打包后的库文件。
 * 2. 使用在线网站对打包后的库文件进行混淆压缩。（例：https://www.lddgo.net/encrypt/js）
 * 3. 将混淆压缩后的代码引用到项目中使用。



 * 【附注】命令说明：
 * vue-cli-service build --mode prod --inline-vue --target lib --name com --dest dir-lib src/index.js
      参数/选项	说明
      vue-cli-service build	Vue CLI 的构建命令，用于打包项目或组件。
      --target lib	指定构建目标为库（Library），而不是应用（App）。
      --inline-vue	将 Vue 库内联到生成的代码中，而不是作为外部依赖。
      --name myLib	指定生成的库的全局变量名（UMD 格式）或文件名。
      src/index.js	指定入口文件，通常是导出组件或库的文件。

    vue-cli官方文档相关：https://cli.vuejs.org/zh/guide/build-targets.html?#%E5%BA%93
*/

// 导入组件，组件必须声明 name
import Com01 from './components/Com01.vue'
import _Vue from 'vue'

// 为组件提供 install 安装方法，供按需引入
[Com01].forEach(el => {
  el.install = function (Vue) {
    if (!Vue) {
      window.Vue = Vue = _Vue
    }
    Vue.component(el.name, el)
  }
})

// 默认导出组件
export {
  Com01
}
