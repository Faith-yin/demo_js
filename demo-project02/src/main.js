/*
 * @Date: 2020-08-25 09:51:45
 * @information: main
 */
import Vue from 'vue'
import App from './App.vue'
// import './registerServiceWorker'
import router from './router'
// import store from './config/scan_store'
import store from "@/store"
import '@/style/index.scss'
import ypfMethods from 'ypf-methods'
// rem配置
// import '@/assets/js/rem'

import '@/assets/js/initAxios'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

Vue.use(Antd);

Vue.use(ElementUI);

import Com19 from '@/components/Com19.js'
Vue.use(Com19)

Vue.prototype.$ypfMethods = ypfMethods


// 在子应用中通过 window.__MICRO_APP_ENVIRONMENT__ 判断是否在微前端环境中
if (window.__MICRO_APP_ENVIRONMENT__) {
  console.log('我在微前端环境中:', window.__MICRO_APP_ENVIRONMENT__, window)


	/**
	 * 子应用获取来自基座应用的数据有两种方式：
	 */
	// 方式1：直接获取数据
	const data = window.microApp.getData() // 返回基座下发的data数据
	console.log('方式1：来自基座应用的数据:', data)

	// 方式2：绑定监听函数
	window.microApp.addDataListener((data) => {
		console.log('方式2：addDataListener-来自基座应用的数据:', data)
		const { path } = data
		if (path) {
			router.push({ path }).catch(()=>{})
		}
	})


	/**
	 * 子应用向基座应用发送数据
	 */
	// dispatch只接受对象作为参数
	window.microApp.dispatch({ msg: '子应用发送的数据...' })

}


const app = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

// 微前端 microApp - 监听卸载操作：子应用被卸载时会接受到一个名为unmount的事件，在此可以进行卸载相关操作
window.addEventListener('unmount', function () {
  app.$destroy()
})
