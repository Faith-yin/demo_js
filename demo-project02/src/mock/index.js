const path = require("path")
const fs = require("fs")
const moment = require('moment')

/**
 * 统一返回格式
 */
function getMockData({code = 200, message = 'success', data = null}) {
    return {code, message, data}
}

/**
 * 读取本地文件
 */
function readMockData(url, params) {
    return new Promise((resolve, reject) => {
        const filePath = path.resolve(__dirname, `${url.replace('/mock/', '')}.js`)
        if (fs.existsSync(filePath)) {
            try {
                delete require.cache[filePath]
                const response = require(filePath)
                const mockData = typeof (response) === 'function' ? response(params) : response
                resolve(getMockData({data: mockData}))
            } catch (e) {
                reject(getMockData({code: 500, message: 'mock file read error'}))
            }
        } else {
            reject(getMockData({code: 404, message: 'mock file not found'}))
        }
    })
}

/**
 * 定义路由处理程序，通常用于响应请求
 */
function mock(app) {
    app.all(/\/mock\/.*/, async (req, res) => {
        const method = req.method.toLocaleUpperCase()
        const params = method === 'GET' ? req.query : req.body
        const logInfo = `Mock请求：${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}, ${method}, ${req.url}, ${JSON.stringify(params)} \r\n`
        console.log(logInfo)
        fs.appendFile(path.resolve(__dirname, 'Log.log'), logInfo, (err) => {
            if (err) {
                console.log('写入失败', err)
            } else {
                console.log('写入成功')
            }
        })
        try {
            const mockData = await readMockData(req.url, params)
            res.status(200)
            res.send(mockData)
        } catch (err) {
            res.status(200)
            res.send(err)
        }
    })
}

module.exports = mock
