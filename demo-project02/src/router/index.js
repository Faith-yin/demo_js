/*
 * @Date: 2020-08-25 09:51:45
 * @information: 路由
 */
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: 'HomePage'
  },
]

const views = require.context('@/views', true, /\.vue$/)
export const nameArr = []
views.keys().forEach(el => {
  // 获取组件配置
  const comConfig = views(el).default
  const name = el.split('/').pop().replace(/\.vue$/, '')
  nameArr.push({
    val: name,
    title: comConfig.title || ''
  })
  routes.push({
    path: '/' + name,
    name: name,
    component: () => import('@/views/' + name + '.vue'),
    meta: {
      title: comConfig.title || ''
    }
  })
})

console.log('🔥 p2 window.__MICRO_APP_BASE_ROUTE__', window.__MICRO_APP_BASE_ROUTE__, window)

const router = new VueRouter({
	// 👇 设置基础路由，子应用可以通过window.__MICRO_APP_BASE_ROUTE__获取基座下发的baseroute，如果没有设置baseroute属性，则此值默认为空字符串
	// (如果基座应用是history路由，子应用是hash路由，这一步可以省略)
  base: window.__MICRO_APP_BASE_ROUTE__ || '/',
  routes,
  mode: 'history',
})

router.beforeEach((to, from, next) => {
  next()
})

router.afterEach((to, from) => {
  document.title = `${to.meta?.title ? `${to.meta.title} | ` : ''}${process.env.VUE_APP_TITLE}`
})

export default router
