/*
 * @Date: 2021-10-30 16:07:30
 * @information:
 */
import * as type from '../mutation/index'

const base1 = {
  namespaced: true,
  state: {
    nameStore: ''
  },
  mutations: {
    modifyName (state, data) {
      state.nameStore = data
    }
  },
  actions: {

  }
}

export default base1
