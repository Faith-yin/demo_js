/*
 * @Date: 2021-10-30 16:07:40
 * @information:
 */
import * as type from '../mutation/index'

export default {
  namespaced: true,
  state: {
    ageStore: 0,
  },
  mutations: {
    modifyAge (state, data) {
      console.log('->', data)
      state.ageStore = data
    }
  }
}
