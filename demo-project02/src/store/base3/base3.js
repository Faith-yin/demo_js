import { getField, updateField } from 'vuex-map-fields'

const base3 = {
  namespaced: true,
  state: {
    userInfo: {
      name: 'yyy',
      age: 18,
      sex: 'boy',
    },
    deviceInfo: {
      platform: 'win',
      version: 11,
    }
  },
  mutations: {
    updateField,
    SET_userInfo: (state, data = {}) => {
      Object.assign(state.userInfo, data)
    },
    SET_deviceInfo: (state, data = {}) => {
      Object.assign(state.deviceInfo, data)
    }
  },
  getters: {
    getField
  }
}

export default base3




