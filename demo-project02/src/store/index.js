import Vue from 'vue'
import Vuex from "vuex"

import base1 from "@/store/base1/base1";
import base2 from "@/store/base2/base2";
import base3 from "@/store/base3/base3";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    base1,
    base2,
    base3
  }
})

