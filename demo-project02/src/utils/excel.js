import ExcelJS from 'exceljs'

export function downloadExcel (wbout, excelName) {
  const blob = new Blob([wbout], { type: 'application/octet-stream' })
  const link = document.createElement('a')
  link.href = URL.createObjectURL(blob)
  link.download = `${excelName}.xlsx`
  document.body.appendChild(link)
  link.click()
  document.body.removeChild(link)
  URL.revokeObjectURL(blob)
}

/**
 * Excel 自动配置并导出
 * 表头：文字加粗、合并、背景色、多级(暂支持最多两级)、固定
 * 列：固定冻结、宽度
 * @param {string} name 文件名称
 * @param {object[]} columns 表头 数据格式同antd-vue column
 * @param {object[]} tableData 表体
 * @param {string[]} mergeFieldArr - 可选 表体数据中需要行合并的字段
 */
export async function handleExcelExport (name, columns, tableData, mergeFieldArr = []) {
  const defaultColWidth = 18
  const isSecondLevel = columns.some(item => item.children?.length > 0)
  const colsArr = []
  const titleArr = [[]]
  const fieldArr = []
  isSecondLevel && titleArr.push([])
  // 表头
  let colI = 0
  columns.forEach(item => {
    if (item.children?.length) {
      item.children.forEach(el => {
        titleArr[0].push(item.title)
        titleArr[1][colI] = el.title
        colsArr.push(el.width ? el.width / 7 : defaultColWidth)
        fieldArr.push(el.dataIndex)
        colI++
      })
    } else {
      titleArr[0].push(item.title)
      isSecondLevel && titleArr[1].push(item.title)
      colsArr.push(item.width ? item.width / 7 : defaultColWidth)
      fieldArr.push(item.dataIndex)
      colI++
    }
  })
  // 表体
  const dataArr = []
  tableData.forEach(row => {
    const itemData = []
    columns.forEach(item => {
      if (item.children?.length) {
        item.children.forEach(el => {
          itemData.push(row[el.dataIndex] ?? '')
        })
      } else {
        itemData.push(row[item.dataIndex] ?? '')
      }
    })
    dataArr.push(itemData)
  })
  const sheetData = [...titleArr, ...dataArr]
  name = name ? name.replace(/\.xlsx$|\.xls$/g, '') : '数据表'
  // 新建工作表
  const workbook = new ExcelJS.Workbook()
  const worksheet = workbook.addWorksheet(name)
  // 样式
  const styleCfg = {
    alignment: {
      horizontal: 'center',
      vertical: 'middle',
      wrapText: true,
    },
    border: {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    },
  }
  // 添加数据: 表头+表体
  sheetData.forEach((item, rowIndex) => {
    const row = worksheet.addRow(item)
    row.eachCell((cell, colIndex) => {
      if (rowIndex === 0 || (isSecondLevel && rowIndex === 1)) {
        cell.style = {
          ...styleCfg,
          font: {
            bold: true
          },
          fill: {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'fafafa' },
          },
        }
      } else {
        cell.style = styleCfg
      }
    })
  })

  // 列宽
  worksheet.columns.forEach((column, cIndex) => {
    column.width = colsArr[cIndex] || defaultColWidth
  })

  // 合并: 按开始行，开始列，结束行，结束列合并。从1开始
  // 表头合并
  if (titleArr.length > 1) {
    titleArr[0].forEach((t, i, arr) => {
      const curI = arr.indexOf(t)
      const curColumn = columns.find(item => item.title === t)
      if (!curColumn || i !== curI) return
      if (curColumn.children?.length) {
        worksheet.mergeCells(1, i + 1, 1, i + curColumn.children.length)
      } else {
        worksheet.mergeCells(1, i + 1, 2, i + 1)
      }
    })
  }
  // 表体合并
  if (mergeFieldArr.length) {
    const cNum = isSecondLevel ? 2 : 1
    mergeFieldArr.forEach(f => {
      const colI = fieldArr.indexOf(f)
      tableData.forEach((item, i, arr) => {
        const startI = arr.findIndex(el => el[f] === item[f])
        const endI = arr.findLastIndex(el => el[f] === item[f])
        if (i === startI) {
          worksheet.mergeCells(startI + cNum + 1, colI + 1, endI + cNum + 1, colI + 1)
        }
      })
    })
  }

  // 冻结列
  worksheet.views = [
    {
      state: 'frozen',
      xSplit: columns.filter(item => item.fixed === 'left').length, // 冻结列数
      ySplit: isSecondLevel ? 2 : 1, // 冻结行数
    }
  ]
  // 保存Excel文件
  const buffer = await workbook.xlsx.writeBuffer()
  downloadExcel(buffer, name)
}

