/*
 * @Date: 2020-08-25 10:04:44
 * @information: vue-cli 3.x 配置文件
 */
const path = require('path')
const mock = require('./src/mock')

function resolve (dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  configureWebpack: { // 若为对象，则将此值合并到最终的配置中
    externals: {
      './cptable': 'var cptable'
    }
  },
  chainWebpack: (config) => { // 允许对内部的webpack配置进行更细粒度的修改
    config.resolve.alias
      .set('@$', resolve('src'))

    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .use('vue-svg-icon-loader')
      .loader('vue-svg-icon-loader')
  },
  // webpack-dev-server 相关配置
  devServer: {
    headers: {
      // 设置跨域支持
      'Access-Control-Allow-Origin': '*',
    },
    hot: true,
    host: '0.0.0.0',
    port: 8180,
    https: false,
    open: true,
    // 跨域配置
    proxy: {
      '/jp': { // 接口是以 /api 开头的需要代理
        // target: `https://api.mz-moe.cn`,
        target: `https://cache.video.iqiyi.com`, // 代理接口地址。实际请求接口地址会是：http://123.182.234.124:11601/jmgc-cbs-upgrade/api/xxx/xxx
        changeOrigin: true, // 默认false，是否需要改变原始主机头为目标URL
        ws: true, // 是否代理websockets
        // pathRewrite: {
        //   '^/api': '' // 重写请求。实际接口中并没有 /api，所以需要替换为空 ''
        // }
      }
    },
    // webpack-dev-server 提供在服务器内部所有其他中间件之前执行自定义中间件的能力。这可用于定义自定义处理程序
    before (app, server, compiler) {
      mock(app)
    },
  },
}


