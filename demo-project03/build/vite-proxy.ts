// 统一处理proxy
export function createProxy(config: Record<string, any>) {
  const proxyList: Record<string, object> = {}
  for (const key in config) {
    proxyList[key] = {
      target: config[key],
      ws: true,
      changeOrigin: true
    }
  }
  return proxyList
}
