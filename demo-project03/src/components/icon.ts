import { h, defineComponent, ref, computed } from 'vue'
import SVGInjectInstance from '@iconfu/svg-inject'
// Glob 导入: https://cn.vitejs.dev/guide/features.html#glob-import
const mxModule = import.meta.glob('../assets/icons/**/**.svg')
const module = import.meta.glob('../../src/assets/icons/**/**.svg')

export default defineComponent({
  name: 'Icon',
  props: {
    src: {
      required: true,
      type: String
    },
    isMxdir: {
      type: Boolean,
      ddefault: false
    }
  },
  setup(props, context) {
    const IconNode = ref()
    const icon = computed(() => props.src)
    const isMxDesignDir = computed(() => props.isMxdir)

    if (icon.value === 'none' || !icon.value) {
      IconNode.value = null
    } else {
      const { src, ...attrs } = context.attrs
      if (icon.value.indexOf('base64') !== -1) {
        IconNode.value = h('i', { ...attrs, class: 'anticon' }, [
          h('img', { src, style: 'width: 1em;height: 1em;vertical-align: initial;' })
        ])
      } else if (icon.value.indexOf('self-') === 0) {
        try {
          const svgModule = isMxDesignDir.value
          ? mxModule[`../assets/icons/${icon.value.replace('self-', '')}.svg`]
          : module[`../../src/assets/icons/${icon.value.replace('self-', '')}.svg`]
          svgModule().then(mod => {
            IconNode.value = h('i', { ...attrs, class: 'anticon' }, [
              h(mod.default, {
                style: 'width: 1em;height: 1em;fill: currentColor;'
              }),
            ])
          })
        } catch (error) {
          IconNode.value = null
        }
      } else if (icon.value.indexOf('http://a6-rficons.a6-dev.dev.pcep.cloud/rfIcons') === 0) {
        IconNode.value = h('i', { ...attrs, class: 'anticon' }, [
          h('img', {
            style: 'width: 1em;height: 1em;fill: currentColor;',
            src: icon.value.slice(39),
            onload: (e: Event) => {
              SVGInjectInstance(e.target)
            }
          })
        ])
      } else if (icon.value.indexOf('http://cp.oss.pcep.cnpc.com.cn/mx-icon') === 0) {
        IconNode.value = h('i', { ...attrs, class: 'anticon' }, [
          h('img', {
            style: 'width: 1em;height: 1em;',
            src: icon.value.slice(30),
            onload: (e: Event) => {
              SVGInjectInstance(e.target)
            }
          })
        ])
      } else if (/^(epai-|fa-rwp-)([\w.]+\/?)\S*/.test(icon.value)) {
        IconNode.value = h('i', { ...attrs, class: 'anticon' }, [
          h('img', {
            style: 'width: 1em;height: 1em;fill: currentColor;',
            src: `/rfIcons/npmIcons/${icon.value}.svg`,
            onload: (e: Event) => {
              SVGInjectInstance(e.target)
            }
          })
        ])
      } else if (/^(http|https):\/\/([\w.]+\/?)\S*/.test(icon.value)) {
        IconNode.value = h('i', { ...attrs, class: 'anticon' }, [
          h('img', {
            style: 'width: 1em;height: 1em;fill: currentColor;',
            src
          })
        ])
      } else {
        IconNode.value = null
      }
    }
    return {
      IconNode
    }
  },
  render() {
    return this.IconNode
  }
})
