import { h, defineComponent, ref, nextTick, watch, VNode, onMounted, onUnmounted } from 'vue'
import { NInputNumber, NInput, InputInst, NSelect, SelectInst, NDatePicker, NTooltip, SelectOption } from 'naive-ui'

export const ShowOrEdit = defineComponent({
  components: { NInput },
  props: {
    // 当前行数据
    row: {
      type: Object,
      default: () => ({})
    },
    value: {
      type: [String, Number],
      default: null,
      required: false
    },
    edit: {
      type: Boolean,
      required: true
    },
    editType: {
      type: Object,
      required: true
    },
    onUpdateValue: {
      type: [Function, Array],
      default: null,
      required: true
    },
    // 若为 numberInputType，其最小值
    minNumber: {
      type: Number,
      default: -Infinity
    },
    // 若为 numberInputType，其最大值
    maxNumber: {
      type: Number,
      default: Infinity
    },
    // 若为 numberInputType， 保留小数的位数
    numberDecimalLen: {
      type: Number,
      default: null
    },
    // 编辑状态下的点击时的校验函数，返回boolean
    checkVerifyFn: {
      type: Function,
      default: null
    }
  },
  setup(props) {
    const isEdit = ref(false)
    // watch(
    //   () => props.edit,
    //   newVal => {
    //     isEdit.value = newVal
    //   }
    // )
    const optionsShow = ref(false)
    const searchValue = ref('')

    // const inputRef = ref<InputInst | null>(null)
    const inputRef = ref<InputInst | any>(null)
    const selectInstRef = ref<SelectInst | null>(null)
    const inputValue = ref<any>(props.value)
    watch(
      () => props.value,
      newVal => {
        inputValue.value = newVal
      }
    )
    let wellNameList: any = []
    let wellList: any = []
    let oilfieldNameList: any = []

    function handleOnDClick(e: any, type: string) {
      const canEdit = props.editType.isEdit && props.edit ? true : false
      if (canEdit && props.checkVerifyFn && !props.checkVerifyFn(props.editType.fieldName)) {
        return
      }
      isEdit.value = canEdit
      nextTick(() => {
        if (props.editType.attrId) return
        if (type === 'textInput') {
          inputRef.value?.focus()
        }
        if (type === 'select' && !['wellNameList', 'logCompanyNameList'].includes(props.editType.optionsName)) {
          selectInstRef.value?.focus()
        }
      })
    }

    function handleChange(type?: string) {
      props.onUpdateValue && props.onUpdateValue(inputValue.value, type)
      isEdit.value = false
    }

    function handler(e: MouseEvent) {
      if (props.editType.attrId && props.editType.isEdit && isEdit.value) {
        const selectOp = document.querySelector('.n-base-select-menu--multiple') as HTMLElement
        const selectInput = document.querySelector('.n-base-selection-tags') as HTMLElement
        const textDicLabelArr = document.querySelectorAll('.text_dic-label')
        let isClickTextDic = false
        for (let i = 0; i < textDicLabelArr.length; i++) {
          if (textDicLabelArr[i].contains(e.target as HTMLElement)) {
            isClickTextDic = true
            break
          }
        }
        // 多选框点击区域外退出编辑状态
        if (
          selectOp &&
          !selectOp.contains(e.target as HTMLElement) &&
          selectInput &&
          !selectInput.contains(e.target as HTMLElement) &&
          !isClickTextDic
        ) {
          isEdit.value = false
        }
      }
    }

    onMounted(() => {
      if (props.editType.isEdit) {
        document.addEventListener('click', handler)
      }
    })
    onUnmounted(() => {
      if (props.editType.isEdit) {
        document.removeEventListener('click', handler)
      }
    })

    return () => {
      switch (props.editType.componentType) {
        case 'numberInputType':
          return h(
            'div',
            {
              onClick: (e: any) => {
                handleOnDClick(e, 'textInput')
              },
              style: 'width:100%;min-height:28px'
            },
            isEdit.value && props.row.isEdit !== false
              ? h<any>(NInputNumber, {
                  ref: inputRef,
                  size: 'small',
                  value: inputValue.value,
                  min: props.minNumber ?? props.editType.minNumber,
                  max: props.maxNumber ?? props.editType.maxNumber,
                  showButton: false,
                  onUpdateValue: (v: string | number) => {
                    let val = v
                    const digit = props.editType.numberDecimalLen ?? props.numberDecimalLen
                    if (
                      digit !== undefined &&
                      digit !== null &&
                      v !== null &&
                      String(v).split('.')[1]?.length > digit
                    ) {
                      const arr = String(v).split('.')
                      const decimal = arr[1].substring(0, digit)
                      val = decimal ? Number(`${arr[0]}.${decimal}`) : Number(arr[0])
                    }
                    inputValue.value = val
                  },
                  onBlur: handleChange
                })
              : inputValue.value
          )
        case 'textInputType':
          return h(
            'div',
            {
              onClick: (e: any) => {
                handleOnDClick(e, 'textInput')
              },
              style: 'width:100%;min-height:28px'
            },
            isEdit.value && props.row.isEdit !== false
              ? h<any>(NInput, {
                  ref: inputRef,
                  size: 'small',
                  value: inputValue.value,
                  onUpdateValue: (v: string | number) => {
                    inputValue.value = v
                    // isEdit.value = false
                  },
                  // onChange: handleChange,
                  onBlur: handleChange
                })
              : h(
                  NTooltip,
                  {
                    placement: 'top'
                  },
                  {
                    default: () => inputValue.value,
                    trigger: () =>
                      h(
                        'div',
                        {
                          style: {
                            width: '100%',
                            overflow: 'hidden',
                            whiteSpace: 'nowrap',
                            textOverflow: 'ellipsis'
                          }
                        },
                        {
                          default: () => inputValue.value
                        }
                      )
                  }
                )
          )
        case 'textAreaInputType':
          return h(
            'div',
            {
              onClick: (e: any) => {
                handleOnDClick(e, 'textInput')
              },
              style: 'width:100%;min-height:28px;'
            },
            isEdit.value
              ? h<any>(NInput, {
                  ref: inputRef,
                  size: 'small',
                  type: 'textarea',
                  autosize: true,
                  value: inputValue.value,
                  onUpdateValue: (v: string | number) => {
                    inputValue.value = v
                    // isEdit.value = false
                  },
                  // onChange: handleChange,
                  onBlur: handleChange
                })
              : h(
                  NTooltip,
                  {
                    placement: 'top'
                  },
                  {
                    default: () => inputValue.value,
                    trigger: () =>
                      h(
                        'div',
                        {
                          style: {
                            width: '100%',
                            overflow: 'hidden',
                            whiteSpace: 'nowrap',
                            textOverflow: 'ellipsis'
                          }
                        },
                        {
                          default: () => inputValue.value
                        }
                      )
                  }
                )
          )
        case 'selectorType':
          return h(
            'div',
            {
              onClick: (e: any) => {
                handleOnDClick(e, 'select')
              },
              style: 'width:100%;min-height:28px'
            },
            isEdit.value && props.row.isEdit !== false
              ? h<any>(NSelect, {
                    // style: {
                    //   'min-width': '100px'
                    // },
                    ref: selectInstRef,
                    filterable: props.editType.filterable || props.editType.filterableNORequest || false,
                    size: 'small',
                    value: inputValue.value,
                    consistentMenuWidth: false,
                    show:
                      props.editType.filterable && props.editType.optionsName === 'wellNameList'
                        ? optionsShow.value
                        : true,
                    options:
                      props.editType.optionsName === 'wellNameList'
                        ? wellNameList
                        : props.editType.optionList,
                    renderOption: ({ node, option }: { node: VNode; option: SelectOption }) =>
                      h(
                        NTooltip,
                        {
                          disabled: !props.editType.tooltip,
                          placement: 'right',
                          style: {
                            width: '150px'
                          }
                        },
                        {
                          trigger: () => node,
                          default: () => option.label
                        }
                      ),
                    onkeyup: (event: any) => { },
                    onSearch: (value: string) => {
                      searchValue.value = value
                    },
                    onUpdateValue: (v: string | number, option: any) => {
                      inputValue.value = v
                      isEdit.value = false
                      if (props.editType.filterable) {
                        const newData = wellList.filter((item: any) => {
                          item.oilfield = item.DATA_REGION
                          item.department = item.ORG_ID_REL_NAME
                          item.wellName = item.WELL_COMMON_NAME
                          item.wellPurpose = item.WELL_PURPOSE_REL_NAME
                          item.wellType = item.WELL_TYPE_REL_NAME
                          return item.WELL_COMMON_NAME === option.value
                        })
                        props.onUpdateValue && props.onUpdateValue(
                          {
                            oilfield: newData[0]?.DATA_REGION,
                            department: newData[0]?.ORG_ID_REL_NAME,
                            wellName: newData[0]?.WELL_COMMON_NAME,
                            wellId: newData[0]?.WELL_ID,
                            wellPurpose: newData[0]?.WELL_PURPOSE_REL_NAME,
                            wellType: newData[0]?.WELL_TYPE_REL_NAME
                          },
                          'wellName'
                        )
                        return
                      }
                      props.onUpdateValue && props.onUpdateValue(inputValue.value, '')
                    },
                    onBlur: handleChange
                })
              : inputValue.value
          )

        case 'dateSelectorType':
          return h(
            'div',
            {
              onClick: (e: any) => {
                handleOnDClick(e, 'select')
              },
              style: 'width:100%;min-height:28px'
            },
            isEdit.value && props.row.isEdit !== false
              ? h<any>(NDatePicker, {
                  ref: selectInstRef,
                  size: 'small',
                  type: props.editType.dateType || 'month',
                  formattedValue: inputValue.value?.toString(),
                  isDateDisabled: (current: number) => current > Date.now(),
                  valueFormat: props.editType.valueFormat || 'yyyyMM',
                  onUpdateFormattedValue: (v: string | number) => {
                    inputValue.value = v.toString()
                    isEdit.value = false
                  },
                  onUpdate: handleChange,
                  onBlur: handleChange
                })
              : inputValue.value
          )
        case 'dateTimeSelectorType':
          return h(
            'div',
            {
              onClick: (e: any) => {
                handleOnDClick(e, 'select')
              },
              style: 'width:100%;min-height:28px'
            },
            isEdit.value
              ? h<any>(NDatePicker, {
                  ref: selectInstRef,
                  size: 'small',
                  type: props.editType.dateType || 'month',
                  formattedValue: inputValue.value?.split(' ')[0].toString(),
                  isDateDisabled: (current: number) => current > Date.now(),
                  valueFormat: props.editType.valueFormat || 'yyyyMM',
                  onUpdateFormattedValue: (v: string | number) => {
                    inputValue.value = v.toString() + ' 00:00:00'
                    isEdit.value = false
                  },
                  onUpdate: handleChange,
                  onBlur: handleChange
                })
              : inputValue.value?.split(' ')[0]
          )

        default:
          return h(
            'div',
            {
              onClick: handleOnDClick,
              onBlur: handleChange,
              tabindex: -1,
              onUpdateValue: (v: string | number) => {
                inputValue.value = v
              }
            },
            inputValue.value
          )
      }
    }
  }
})
