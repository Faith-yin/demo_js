/**
 * ts文件中使用 render函数 定义vue组件
 */
import {defineComponent, h} from 'vue'

export const text1 = defineComponent({
  name: 'Text1',
  props: {
    title: {
      type: String,
      default: '默认title1'
    }
  },
  setup (props, context) {
    window['$username'] = 'yinpengfei'
  },
  render () {
    return h(
      'h1',
      {
        class: 'text1',
        style: { color: '#ff6700' }
      },
      this.title,
    )
  }
})


/**
 * ts文件中使用 setup 的 return 定义vue组件
 */
export const text2 = defineComponent({
  name: 'Text2',
  props: {
    title: {
      type: String,
      default: '默认title2'
    }
  },
  setup (props, context) {
    return () => {
      return h(
        'h2',
        {
          class: 'text2',
          style: { color: '#1fb19e' }
        },
        props.title
      )
    }
  }
})


