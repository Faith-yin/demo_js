import { createApp } from 'vue'
import App from './App.vue'
import { setupRouter } from './router'
import { setupStore } from './store'
import '@/style/base.less'
// 引入主题色全局变量配置
import '@/style/theme.css'
// 引入微前端
import microApp from '@micro-zoe/micro-app'
import './messageForward.js'
import './index.css'
import Bus from '@/utils/bus'

Bus.on('changeDemoMode', (e) => {
  console.log('changeDemoMode', e)
})

// 微前端初始化
microApp.start({
  'router-mode': 'native',
	// 预加载: 是指在应用尚未渲染时提前加载资源并缓存，从而提升首屏渲染速度
	preFetchApps: [
    { name: 'myMicroApp', url: 'http://localhost:8180' }
  ]
})

async function bootstrap() {
  const app = createApp(App)

  // 挂载路由
  await setupRouter(app)

  // 挂载store
  setupStore(app)

  // 挂载实例
  app.mount('#app', true)
}
bootstrap()
