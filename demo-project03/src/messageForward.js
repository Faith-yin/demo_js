window.addEventListener('message', (e) => {
  if (e.data && e.data.commandName) {
    // 子应用发送来的消息
    if (e.data.source === 'child') {
      const model = e.data
      Reflect.deleteProperty(model, 'source')
      window.parent.postMessage(model, '*')
    } else { // 父应用发送来的消息
      document.getElementById('com-iframe').contentWindow.postMessage(e.data, '*')
    }
  }
})