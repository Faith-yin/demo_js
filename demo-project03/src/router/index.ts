import {App} from "vue"
import {createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw, Router} from "vue-router"
import type {RouteLocationNormalized} from 'vue-router'

export const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/views/home.vue'),
    meta: {
      title: 'Home',
      hidden: true
    }
  },
  {
    path: '/page01',
    name: 'page01',
    component: () => import('@/views/page01.vue'),
    meta: {
      title: '更改svg图标'
    }
  },
  {
    path: '/page02',
    name: 'page02',
    component: () => import('@/views/page02.vue'),
    meta: {
      title: 'hook文件使用-表格编辑'
    }
  },
  {
    path: '/page03',
    name: 'page03',
    component: () => import('@/views/page03.vue'),
    meta: {
      title: 'script setup 语法糖'
    }
  },
  {
    // 👇 非严格匹配，/my-page/* 都指向 MyPage 页面
    path: '/page04/:page*', // vue-router@4.x path的写法为：'/my-page/:page*'
    name: 'page04',
    component: () => import('@/views/page04.vue'),
    meta: {
      title: '微前端 - microApp',
    }
  },
  {
    path: '/page05',
    name: 'page05',
    component: () => import('@/views/page05.vue'),
    meta: {
      title: 'handsontable 表格组件使用',
    }
  },
  {
    path: '/page06',
    name: 'page06',
    component: () => import('@/views/page06.vue'),
    meta: {
      title: '1. 在循环中使用异步请求 2. 假进度条插件fake-progress',
    }
  },
  {
    path: '/page07',
    name: 'page07',
    component: () => import('@/views/page07.vue'),
    meta: {
      title: '1、less 语法 2、@vueuse/core 使用',
    }
  },
  {
    path: '/page08',
    name: 'page08',
    component: () => import('@/views/page08.vue'),
    meta: {
      title: '1、使用render函数 2、引入用ts定义的组件',
    }
  },
  {
    path: '/page09',
    name: 'page09',
    component: () => import('@/views/page09.vue'),
    meta: {
      title: '1、自定义组件中使用v-model(有无参数) 2、父组件中通过@XXX接收子组件的响应方法，其实底层是通过props传给了子组件这个方法，所以可以在子组件中通过props接收这个：onXXX，并可以直接调用运行',
    }
  },
  {
    path: '/page10',
    name: 'page10',
    component: () => import('@/views/page10.vue'),
    meta: {
      title: '@antv/g6 流程图使用',
    }
  },
  {
    path: '/page11',
    name: 'page11',
    component: () => import('@/views/page11.vue'),
    meta: {
      title: 'IntersectionObserver 使用',
    }
  },
  {
    path: '/page12',
    name: 'page12',
    component: () => import('@/views/page12.vue'),
    meta: {
      title: '前端调用摄像头',
    }
  },
  {
    path: '/page13',
    name: 'page13',
    component: () => import('@/views/page13.vue'),
    meta: {
      title: '原生“跨组件”通信方式',
    }
  },
  {
    path: '/page14',
    name: 'page14',
    component: () => import('@/views/page14.vue'),
    meta: {
      title: 'naive-ui data-table 表格行合并错乱问题',
    }
  },
  {
    path: '/page15',
    name: 'page15',
    component: () => import('@/views/page15.vue'),
    meta: {
      title: '单个文件上传进度展示',
    }
  },

]

export const router = createRouter({
  history: createWebHistory(),
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {left: 0, top: 0}
    }
  },
  routes,
})

// 路由守卫
function createRouterGuard(router: Router) {
  // 全局前置守卫
  router.beforeEach((to: RouteLocationNormalized, from: RouteLocationNormalized) => {
  })

  // 全局后置守卫
  router.afterEach((to: RouteLocationNormalized, from: RouteLocationNormalized) => {
    // 设置标签页名称
    document.title = `${to?.meta?.title ? to.meta.title + ' | ' : ''}${import.meta.env.VITE_TITLE}`
  })
}

export async function setupRouter(app: App) {
  // 挂载路由
  app.use(router)

  // 守卫
  createRouterGuard(router)

  // 路由准备就绪
  await router.isReady()
}


