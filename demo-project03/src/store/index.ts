import { App } from "vue"
import { createStore } from 'vuex'

const store = createStore({
  state () {
    return {
      count: 0
    }
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
})


export const setupStore = (app: App) => {
  app.use(store)
}
