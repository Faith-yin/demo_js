/**
 * @create 2024-12-19 20:07:40 星期四
 * @desc mitt 事件总线
*/
import mitt from 'mitt'

export default mitt()
