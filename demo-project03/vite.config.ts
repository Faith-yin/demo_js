import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import { createProxy } from './build/vite-proxy'
import svgLoader from 'vite-svg-loader'
import Components from 'unplugin-vue-components/vite'
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers'

// https://cn.vitejs.dev/config/
export default defineConfig({
	// 用于加载 .env 文件的目录。可以是一个绝对路径，也可以是相对于项目根的路径
	envDir: 'env',
  plugins: [
    vue({
      template: {
        compilerOptions: {
          isCustomElement: tag => /^micro-app/.test(tag)
        }
      }
		}),
    svgLoader(),
    Components({
      resolvers: [NaiveUiResolver()]
    }),
  ],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src')
    }
  },
	// 公共基础路径
	// base: `/front/demo-project03/`,
	build: {
		// 打包文件指定输出路径
		// outDir: 'dist/front/demo-project03',
		// 自定义底层的 Rollup 打包配置
		rollupOptions: {
			output: {
				/**
				 * 更细粒度地控制代码分割策略，需使用使用 manualChunks 配置
				 */
				// 以对象的方式使用
				// manualChunks: {
				//	// 将path模块打包成一个chunk，名叫 buildPath
				// 	buildPath: ['path'],
				// },
				// 以函数的方式使用
				manualChunks: (id, meta) => {
					console.log('manualChunks---->', id, meta)
					// 将所有第三方包打包在一个chunk里，名称叫 vendor
					if (id.includes('node_modules')) {
						return 'vendor'
					} else if (id.includes('utils/base.ts')) {
						// 将自定义的一个文件打包成一个单独的文件
						return 'buildBase'
					}
				},
			}
		},
		// 用来指定使用哪种混淆器，默认是esbuild
		minify: 'terser',
		// 传递更多混淆设置选项
		terserOptions: {
			compress: {
				// 移除console
				drop_console: true,
				drop_debugger: true
			}
		}
	},
  server: {
    port: 3100,
    // 指定服务器应该监听哪个 IP 地址。 如果将此设置为 0.0.0.0 或者 true 将监听所有地址，包括局域网和公网地址
    host: true,
    open: true,
    proxy: {
      ...createProxy({
        '^/jp': 'https://cache.video.iqiyi.com',
        '^/api': 'http://localhost:3000'
      })
    }
  }


})
