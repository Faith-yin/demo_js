import './App.css';
import {getRenderRoutes} from './router'

function App() {
  return (
    getRenderRoutes()
  );
}

export default App;
