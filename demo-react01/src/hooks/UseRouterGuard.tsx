import {useEffect} from "react";
import {useLocation} from "react-router-dom";
import { routeConfig } from '@/router/routes'

/**
 * 实现路由守卫
 */
const UseRouterGuard = () => {
  const location = useLocation()

  useEffect(() => {
    // 当前路由进入时，这里是前置守卫的逻辑
    const currentRoute = routeConfig.find(route => route.path === location.pathname)
    if (currentRoute) {
      document.title = location.pathname.slice(1) + '：' + currentRoute.title
    }
    return () => {
      // 当前路由退出（卸载）时，这里是后置守卫的逻辑
      // console.log('这里是后置守卫的逻辑', )
    }
  }, [location, routeConfig])
}

export default UseRouterGuard


