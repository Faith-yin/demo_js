import {BrowserRouter, Routes, Route} from 'react-router-dom'
import {routeConfig} from './routes'

// 渲染路由组件
const renderRoutes = (routes: any[]) => {
  return routes.map((item, index) => {
    if (item && item.children) {
      return (
        <Route
          path={item.path}
          element={item.element}
          key={index}>
          {renderRoutes(item.children)}
        </Route>
      );
    } else {
      return (
        <Route path={item.path} element={item.element} key={index}></Route>
      )
    }
  })
}

// 获取路由组件
export const getRenderRoutes = () => {
  return (
    // basename 基本URL路径，默认为 "/"，若为‘/demo’，则url访问路径为 ‘/demo/home’
    <BrowserRouter basename='/'>
      <Routes>
        {renderRoutes(routeConfig)}
      </Routes>
    </BrowserRouter>
  )
}
