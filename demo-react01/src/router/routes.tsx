import {Router} from "../types/router";
import Home from '../views/Home'
import Page01 from "../views/Page01";
import Page02 from "../views/Page02";

// 路由配置
export const routeConfig: Router.Route[] = [
  {
    path: "/",
    name: 'Home',
    element: <Home />,
    exact: true,
    children: [],
    title: 'Home'
  },
  {
    path: '/Page01',
    name: 'Page01',
    element: <Page01 />,
    exact: true,
    title: '基础用法 useState, useEffect, PropsWithChildren, forwardRef, useImperativeHandle, useMemo, useCallback, 路由信息...'
  },
  {
    path: '/Page02',
    name: 'Page02',
    element: <Page02 />,
    exact: true,
    title: 'Page02'
  }
]


