import React from "react";

/**
 * route 配置类型定义
 */
declare namespace Router {
  interface Route {
    path: string
    element: React.ReactNode
    name: string
    exact?: boolean
    children?: route[]
    title: string
  }
}

