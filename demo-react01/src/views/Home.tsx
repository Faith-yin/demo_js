import React from "react";
import {Link} from "react-router-dom";
import '../style/home.scss'
import { routeConfig } from '@/router/routes'
import UseRouterGuard from "@/hooks/UseRouterGuard";

const Home: React.FC = () => {
  UseRouterGuard()

  return (
    <div id='home-page'>
      {
        routeConfig.slice(1).map((el, index) => (
          <Link to={el.path} key={index}>
            {el.name}：
            <span className='title'>{el.title}</span>
          </Link>
        ))
      }
    </div>
  )
}

export default Home
