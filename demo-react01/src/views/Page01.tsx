import React, {
  createContext,
  forwardRef,
  PropsWithChildren,
  useCallback, useContext,
  useEffect,
  useImperativeHandle,
  useMemo,
  useRef,
  useState
} from "react";
import {useLocation, useNavigate, useParams} from "react-router-dom";
import UseRouterGuard from '@/hooks/UseRouterGuard'

let i = 0
interface Com01Props {
  bookName: string,
  price?: number
}
interface Com02Props {
  // 定义你的props类型
}
interface Com02Ref {
  sayHello: () => void;
}

// 通过 React.createContext 创建 context 对象，通过 Provider 修改 context 的值
const ThemeContext = createContext('light')

/**
 * Page01
 * useState
 * 给子组件传值
 * ref：原生组件和子组件
 * Context：跨组件传递值
 */
const Page01: React.FC = () => {
  UseRouterGuard()
  const [show, setShow] = useState(false)
  const refEl = useRef(null)
  const com02Ref = useRef<Com02Ref>(null)
  const [bookName, setBookName] = useState('老人与海')
  const [theme, setTheme] = useState('dark')


  console.log('ref:',refEl, com02Ref)

  return (
    <div>
      <ThemeContext.Provider value={theme}>
        <div ref={refEl}>测试内容ref</div>

        <button onClick={() => setShow(!show)}>{show ? '隐藏Com01' : '显示Com01'}</button>
        {show && <hr/>}
        {show && <Com01 bookName={bookName}>这是子内容...</Com01>}

        <hr />

        <Com02 ref={com02Ref} />

        <hr />

        <Com03 />

        <hr />

        <Com04 />
      </ThemeContext.Provider>
    </div>
  )
}


/**
 * 子组件01
 * useEffect
 * useContext
 */
const Com01: React.FC<PropsWithChildren<Com01Props>> = (props) => {
  const [username, setUsername] = useState(`yyy-${i}`)
  const theme = useContext(ThemeContext)

  const change = () => {
    setUsername(`yyy-${++i}`)
  }

  useEffect(() => {
    console.log('com01 初始化。props: ', props)
    return () => {
      console.log('com01 初始化卸载',)
    }
  }, [])

  useEffect(() => {
    console.log('username 新值更新了：', username)
    return () => {
      console.log('username 旧值卸载了: ', username)
    }
  }, [username])

  return (
    <div>
      <div>Com01: </div>
      <div>
        <button onClick={change}>change</button>
        {username}
      </div>
      <div>当前主题：{theme}</div>
    </div>
  )
}


/**
 * 子组件02
 * forwardRef：使用ref
 */
const Com02 = forwardRef<Com02Ref, Com02Props>((props, ref) => {
  const [helloText, setHelloText] = useState('helloText')

  // 暴露出一些自定义属性、方法
  useImperativeHandle(ref, () => ({
    helloText,
    sayHello: () => {
      alert('Hello from Com02!')
    }
  }))

  return (
    <div>
      com02
    </div>
  )
})


/**
 * 子组件03
 * useMemo：用于缓存一个值，它会调用你传入的函数，deps 变化之后重新执行函数创建值
 * useCallback(作性能优化)：用于缓存一个函数，它不会调用你传入的函数，deps 变化之后重新缓存传入的函数
 */
const Com03 = () => {
  const [count, setCount] = useState(1)

  const handleCallBack = () => {
    console.log('useCallback 重新执行 handleCallBack', )
  }

  const computedValue = useMemo(() => {
    console.log('useMemo 重新执行：', count, count * 2)
    return count * 2
  }, [count])

  const memoizedCallback = useCallback(() => {
    // handleCallBack()
  }, [count])

  return (
    <div>
      <div>com03</div>
      <input
        type='number'
        value={count}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => setCount(Number(e.target.value))} />
      <span>计算数值：{computedValue}</span>

      <TestFn memoizedCallback={memoizedCallback} />
    </div>
  )
}

const TestFn = (props:any) => {
  console.log('testFn渲染-props:', props)
  return (
    <div>
      testFn
    </div>
  )
}


/**
 * com04
 * 路由信息
 */
const Com04 = () => {
  const location = useLocation()
  const params = useParams()
  const navigate = useNavigate()

  console.log('Com04 路由信息：', location, params, navigate)

  return (
    <div>
      com04
      <div>

      </div>
    </div>
  )
}


export default Page01
