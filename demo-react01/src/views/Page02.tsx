import React from "react";
import UseRouterGuard from "@/hooks/UseRouterGuard";

const Page02: React.FC = () => {
  UseRouterGuard()

  return (
    <div>Page02</div>
  )
}

export default Page02
