<!--
 * @Date: 2021-01-11 15:10:56
 * @information: readme
-->

## vue 3 + ts + element-plus

2021-01-10

> [vue3 相关文档](https://vue3js.cn/)

如项目名所示：这是一个 vue3.0 + ts + element-plus-beta 版本写的小 demo ，使用 vue3 倡导的 组合式 API，包括 vue3.0、vue-router4.0、vuex4.0 的基础用法...
