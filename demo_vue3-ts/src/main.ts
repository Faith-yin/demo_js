/*
 * @Date: 2021-01-10 11:25:42
 * @information: main
 */

import { createApp } from 'vue'
import App from './App.vue'
import router from './routers'
import store from './store'

import elementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';

import '@/style/common.scss'

import '@/config/axios.ts'

const app = createApp(App)

app.use(store).use(router).use(elementPlus).mount('#app')


// 以下出自vuex官方说明：https://next.vuex.vuejs.org/zh/guide/migrating-to-4-0-from-3-x.html#typescript-%E6%94%AF%E6%8C%81
import { ComponentCustomProperties } from 'vue'
import { Store } from 'vuex'

declare module '@vue/runtime-core' {
  // 声明自己的 store state
  interface State {
    // count: number
  }

  interface ComponentCustomProperties {
    $store: Store<State>
  }
}