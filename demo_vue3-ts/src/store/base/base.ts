/*
 * @Date: 2021-01-10 18:04:13
 * @information: base store
 */
import { addFormInterface, baseState } from "@/interfaces/base";

const state = {
  detailObj: {}
}

const getters = {
  get (state: baseState) {
    return state.detailObj
  }
}

const mutations = {
  modify (state: baseState, data: addFormInterface) {
    state.detailObj = data
  }
}

const actions = {

}


export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}

