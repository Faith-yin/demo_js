/*
 * @Date: 2021-10-30 13:34:48
 * @information: 注册store
 */
import { createStore } from 'vuex'

let model = {
  modules: {}
}

const files = require.context('@/store', true, /\.ts$/)

files.keys().forEach((el:any) => {
  const name:string = el.split('/').pop().replace(/\.ts$/, '')
  if (name !== 'index') {
    Reflect.set(model.modules, name, files(el).default)
  }
})


export default createStore(model)
