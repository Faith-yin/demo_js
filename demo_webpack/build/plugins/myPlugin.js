/*
 * @Date: 2022-01-06 14:22:07
 * @information: 自定义webpack插件
 * https://webpack.docschina.org/contribute/writing-a-plugin/: 一个简单的示例插件，生成一个叫做 assets.md 的新文件；文件内容是所有构建生成的文件的列表
 */
module.exports = class MyPlugin {
  static defaultOptions = {
    outputFile: 'assets.md',
  }

  constructor(options) {
    this.options = { ...MyPlugin.defaultOptions, ...options }
  }

  apply (compiler) {
    // webpack 模块实例，可以通过 compiler 对象访问，
    const { webpack } = compiler;

    // Compilation 对象提供了对一些有用常量的访问。
    const { Compilation } = webpack;

    // RawSource 是其中一种 “源码”("sources") 类型，用来在 compilation 中表示资源的源码
    const { RawSource } = webpack.sources;


    // compiler 钩子（即何时触发插件）
    // thisCompilation：初始化 compilation 时调用，在触发 compilation 事件之前调用
    compiler.hooks.thisCompilation.tap('MyPlugin', (compilation, callback) => {
      // 绑定到资源处理流水线（assets processing pipeline）
      compilation.hooks.processAssets.tap(
        {
          name: 'MyPlugin',
          // 用某个靠后的资源处理阶段，确保所有资源已被插件添加到compilation
          stage: Compilation.PROCESS_ASSETS_STAGE_SUMMARIZE
        },
        (assets) => {
          // assets 是一个包含 compilation 中所有资源(assets)的对象
          // 该对象的键值是资源路径，值是文件的源码

          // 遍历所有资源，生成 md 文件的内容
          const content = '# In this build:\n\n' +
            Object.keys(assets).map(filename => `- ${filename}`).join('\n')

          // 向 compilation 添加新的资源，这样 webpack 就会自动生成并输出到 output 目录
          // emitAssets: 写入文件系统(emitAssets)：在确定好输出内容后，根据配置确定输出的路径和文件名，把文件内容写入到文件系统
          compilation.emitAsset(this.options.outputFile, new RawSource(content))

        }

      )

    })
  }

}

