/*
 * @Date: 2021-10-21 09:17:59
 * @information: 公共配置，参考：https://juejin.cn/post/7014466035923288072
 */
const path = require('path')
// html-webpack-plugin将生成一个HTML5文件，在body中使用script标签引入你所有webpack生成的bundle
const HtmlWebpackPlugin = require('html-webpack-plugin')
// 抽离css
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
// vue加载器
const { VueLoaderPlugin } = require('vue-loader')
// 拷贝插件
const CopyPlugin = require("copy-webpack-plugin")
// 自定义插件
const MyPlugin = require('./plugins/myPlugin')


module.exports = {
  entry: path.resolve(__dirname, '../src/main.js'),
  output: {
    path: path.join(__dirname, '../dist'), // 打包后生成的文件夹
    filename: '[name].[contenthash:8].js', // 指列在 entry 中，打包后输出的文件的名称
    clean: true, // 每次构建都清除dist包
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.vue'], // 省略文件后缀
    alias: { // 配置别名
      '@': path.resolve(__dirname, '../src')
    }
  },
  // 防止将外部资源包(例如cdn引入的资源)打包到bundle中
  externals: {
    'jquery': 'jQuery',
    'vue': 'Vue',
    'vue-router': 'VueRouter'
  },
  // 插件（Plugins）是用来拓展Webpack功能的，包括：打包优化、资源管理、注入环境变量。插件使用：只需要require()它，然后把它添加到plugins数组中
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '../index.html'), // html模板
      filename: 'index.html'
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name]_[contenthash:8].css' // 抽离整的css文件名称
    }),
    new VueLoaderPlugin(),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, '../public'), // 定义要拷贝的源目录
          to: '', // 定义要拷贝到的目标目录，非必填，不填写则拷贝到打包的output输出地址中
        },
      ],
    }),
    new MyPlugin({ outputFile: '输出文件列表.md' })

  ],
  // loader用于对模块的源代码进行转换，都在module的rules中配置
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        include: [path.resolve(__dirname, '../src')],
      },
      {
        test: /\.css$/,
        // 使用的插件，从右向左解析
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
      {
        test: /\.(scss|sass)$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /(\.jsx|\.js)$/,
        use: ['babel-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpe?g|gif|svg|ico)(\?.*)?$/, // 加载图片资源
        type: 'javascript/auto', // 解决asset重复
        loader: 'url-loader', // 将文件转为base64内联到bundle中，如果超出限制的大小，则使用file-loader将文件移动到输出的目录中
        options: {
          esModule: false, // 解决html区域，vue模板引入图片路径问题
          limit: 10000,
          name: 'img/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,//加载视频资源
        loader: "url-loader",
        options: {
          limit: 10000,
          name: "media/[name].[hash:7].[ext]",
        },
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i, //加载字体资源
        loader: "url-loader",
        options: {
          limit: 10000,
          name: "fonts/[name].[hash:7].[ext]",
        },
      }

    ]
  }
}
