/*
 * @Date: 2021-10-22 15:48:55
 * @information: 开发环境配置
 */
const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')
const path = require('path')
const env = require('../config/dev.env')
const webpack = require('webpack')
// 友好的终端错误显示方式
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
const utils = require('./utils.js')

module.exports = merge(common, {
  stats: 'errors-only', // 去除控制台webpack打印的无用信息
  devServer: {
    hot: true,
    open: false,
    port: 8088,
    compress: true, // 开启gzip压缩
    static: { // 托管静态资源文件, 可通过数组的方式托管多个静态资源文件
      directory: path.join(__dirname, '../public')
    },
    client: {
      progress: false // 在浏览器端打印编译速度
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(env.NODE_ENV)
      }
    }),
    new FriendlyErrorsWebpackPlugin({
      // 运行成功
      compilationSuccessInfo: {
        messages: ['Your Application is: http://localhost:8088'],
        notes: ['有些附加说明要在成功编辑时显示']
      },
      // 运行错误 - //您可以收听插件转换和优先级的错误, 严重性可以是'错误'或'警告'
      onErrors: utils.createNotifierCallback(),
      clearConsole: true, // 是否每次编译之间清除控制台, 默认为true
    })

  ]
})

