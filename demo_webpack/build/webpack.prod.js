/*
 * @Date: 2021-10-22 15:49:07
 * @information: 生产配置
 */
const webpack = require('webpack')
const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')
const env = require('../config/prod.env.js')
const path = require('path')
// 将bundle内容展示为一个便捷的、交互式、可缩放的树状图形式。方便我们更直观了解代码的分离
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
// gzip压缩
const CompressionPlugin = require("compression-webpack-plugin")
// 压缩js，去除注释等..(PS: webpack5+已内置压缩js的插件功能，但若想自定义，需额外引包写配置。)
const TerserPlugin = require("terser-webpack-plugin")
// 打包进度显示
const ProgressBarPlugin = require('progress-bar-webpack-plugin')


module.exports = merge(common, {
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'js/[name].[chunkhash].js', // 指列在 entry 中，打包后输出的文件的名称
    chunkFilename: 'js/[name].[chunkhash].js', // 指未列在 entry 中，却又需要被打包出来的文件的名称
  },
  // 优化
  optimization: {
    /**
     * 拆分模块配置: webpack文档 https://webpack.docschina.org/plugins/split-chunks-plugin/
     */
    splitChunks: {
      /** 
       * 缓存组：定义分包规则，满足条件就将这些依赖提取到一个模块中，是splitChunks的关键配置
       * webpack就是经过这里判断如何拆分模块的。可以继承和/或覆盖来自 splitChunks.* 的任何选项
      **/
      cacheGroups: {
        // 自定义属性名，属性名可以自己取，只要不跟缓存组下其他定义过的属性同名就行，否则后面的拆分规则会把前面的配置覆盖掉
        vendor: {
          priority: 1, // 优先级配置，优先匹配优先级更高的规则，不设置的规则优先级默认为0
          test: /[\\/]node_modules[\\/]/, // 过滤 modules，默认为所有的 modules，可匹配模块路径或 chunk 名字，当匹配到某个 chunk 的名字时，这个 chunk 里面引入的所有 module 都会选中
          // 若为string类型：有三个值：all(默认值)、async、initial
          // all 优化所有模块的复用性
          // async 优化异步模块的复用性
          // initial 优化非异步模块的复用性，即只管初始化时就能获取的模块
          // 参考：https://zhuanlan.zhihu.com/p/136443552
          chunks: 'initial',
          name: 'chunk', // chunk 的文件名
          priority: -10,
          minSize: 0, // 表示被拆分出的 bundle 在拆分之前的体积的最小数值，只有 >= minSize 的 bundle 会被拆分出来
          minChunks: 1, // 表示在分割前，可被多少个chunk引用的最小值(默认为1)。当模块被不同模块（entry也算一个）引用的次数大于等于这个配置值时，才会被抽离出去
          reuseExistingChunk: true // 重复使用已经存在的块，若某个模块在之前已经被打包过了，后面打包时再遇到这个模块就直接使用之前打包的模块
        },
        // 拆分多次引用的包及文件
        common: {
          chunks: 'all',
          name: 'chunk-common', // 打包后的文件名
          minSize: 0,
          minChunks: 2, // 重复2次才能打包到此模块
          priority: -20,
          reuseExistingChunk: true
        },
        // 拆分第三方包moment
        moment: {
          name: 'chunk-moment',
          minChunks: 1,
          chunks: 'all',
          test (module) {
            return module.resource && module.resource.indexOf('moment') >= 0
          },
          priority: 10,
          enforce: true, // 默认false。告诉 webpack 忽略 splitChunks.minSize、splitChunks.minChunks、splitChunks.maxAsyncRequests 和 splitChunks.maxInitialRequests 选项，并始终为此缓存组创建 chunk
          reuseExistingChunk: true
        },
        // 拆分指定文件
        base: {
          name: 'chunk-base',
          test (module) {
            return module.resource && module.resource.indexOf('base') >= 0
          },
          priority: 10,
          enforce: true,
          reuseExistingChunk: true
        }
      },
    }
  },
  plugins: [
    // new BundleAnalyzerPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.html$/i, // 将HTML导出为字符串，处理HTML中引入的静态资源
        loader: 'html-loader'
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(env.NODE_ENV)
      }
    }),
    new CompressionPlugin({
      asset: '[path].gz[query]', // 目标资源名称。[file] 会被替换成原资源。[path] 会被替换成原资源路径，[query] 替换成原查询字符串
      algorithm: 'gzip',
      // test: new RegExp('\\.(js|css)$'),
      test: /\.(js|css)$/,
      threshold: 10240, // 只处理比这个值大的资源。按字节计算, 默认为0
      minRatio: 0.8, // 只有压缩率比这个值小的资源才会被处理, 默认为0.8
    }),
    new TerserPlugin({
      parallel: true, // 多进程
      terserOptions: {
        ecma: undefined,
        warnings: false,
        parse: {},
        compress: {
          drop_console: true,
          drop_debugger: true,
          pure_funcs: ['console.log'], // 移除console
        },
      },
    }),
    new ProgressBarPlugin({
      complete: "█",
    })


  ]
})



