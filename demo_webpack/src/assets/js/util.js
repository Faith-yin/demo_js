/*
 * @Date: 2021-12-23 10:24:42
 * @information: util
 */
export default {
  getName (val) {
    return val.split('-')[0] || 'unknown'
  }
}