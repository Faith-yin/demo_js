/*
 * @Date: 2021-10-21 09:13:52
 * @information: index.js
 */
import './style/base.css'
import './style/common.scss'


// 这是一段测试注释
let arr = [1, 2, 3, 4, 5]
arr = arr.map(el => el + 10)


const mark = arr.includes('11')

console.log('标志哦哦哦1: ', mark)

console.log('当前环境', process.env.NODE_ENV)


import Vue from 'vue'
import App from './App.vue'
import router from './routers/router'
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  // render: h => h(App)
  components: { App },
  template: '<App/>'
})

