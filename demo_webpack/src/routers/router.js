/*
 * @Date: 2021-10-23 12:44:05
 * @information:
 */
import VueRouter from 'vue-router'
import Vue from 'vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home.vue'),
    meta: {
      title: 'Home'
    }
  },
  {
    path: '/Mine/:age',
    name: 'Mine',
    component: () => import('@/views/Mine.vue'),
    meta: {
      title: 'Mine'
    }
  }
]

const router = new VueRouter({
  routes,
  // mode: 'history',
  // base: '/', // 默认为 /
})

export default router
