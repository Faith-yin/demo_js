/*
 * @Date: 2021-12-27 16:09:34
 * @information: fib
 */

/**
 * 斐波那契数列（Fibonacci sequence），又称黄金分割数列、因数学家列昂纳多·斐波那契（Leonardoda Fibonacci）以兔子繁殖为例子而引入，
 * 故又称为“兔子数列”，指的是这样一个数列：1、1、2、3、5、8、13、21、34、……在数学上，斐波那契数列以如下被以递推的方法定义：
 * F(1)=1，F(2)=1, F(n)=F(n - 1)+F(n - 2)（n ≥ 3，n ∈ N*），是一个最最最基本的算法题了。如何实现斐波那契数列
 */

//  递归
//  时间复杂度是O(2n)，空间复杂度取决于栈的深度
// function fib (n) {
//   if (Number.isInteger(n)) {
//     if (n <= 0) {
//       return -1
//     } else if (n === 1 || n === 2) {
//       return 1
//     } else {
//       return fib(n - 1) + fib(n - 2)
//     }
//   }
// }

// 循环 - 借助数组
// 时间复杂度O(n)，空间复杂度O(n)
// function fib (n) {
//   if (Number.isInteger(n)) {
//     let arr = []
//     if (n <= 0) {
//       return -1
//     } else {
//       arr[0] = arr[1] = 1
//       arr[2] = 2
//       for (let i = 3; i < n + 1; i++) {
//         arr[i] = arr[i - 1] + arr[i - 2]
//       }
//     }
//     return arr[n - 1]
//   }
// }

// 循环 - 借助两个变量
// 时间复杂度O(n)，空间复杂度O(1)
function fib (n) {
  if (Number.isInteger(n)) {
    let a, b, c
    if (n <= 0) {
      return -1
    } else {
      a = b = 1
      for (let i = 3; i < n + 1; i++) {
        c = a + b
        a = b
        b = c
      }
    }
    return c
  }
}

console.log(fib(9))
