/*
 * @Date: 2022-01-11 15:33:55
 * @information: 【leetcode 220题】【中等】
 */
/**
 * 在整数数组 nums 中，是否存在两个下标 i 和 j，使得 nums [i] 和 nums [j] 的差的绝对值小于等于 t ，且满足 i 和 j 的差的绝对值也小于等于 ķ 。
 * 如果存在则返回 true，不存在返回 false。
 */
/**
 * 示例
 * 输入: nums = [1,2,3,1], k = 3, t = 0
 * 输出: true
*/

const nums = [5, 10, 15, 20], k = 3, t = 0

/**
 * 双循环暴力解法
 */
function containNum (nums, k, t) {
  const len = nums.length
  for (let i = 0; i < len; i++) {
    for (let j = i + 1; j < Math.min(len, i + 1 + k); j++) {
      if (Math.abs(nums[i] - nums[j]) <= t) {
        return true
      }
    }
  }
  return false
}

console.time()
console.log('->', containNum(nums, k, t))
console.timeEnd() // 约8ms


/**
 * 桶排序、滑动窗口解法：https://leetcode-cn.com/problems/contains-duplicate-iii/solution/zui-xiang-xi-de-zhu-shi-jie-by-isaac_jin-uxqu/
 */
var containsNearbyAlmostDuplicate = function (nums, k, t) {
  // 根据nums中的元素分配桶 ID
  // 如果nums[i] 属于 [0,t] 那么会放入0号桶内（因为Math.floor）
  // 如果nums[i] 属于 [t+1,2t+1] 那么会放入1号桶内（因为Math.floor）
  // 如果nums[i] 属于 [2t+2,3t+2] 那么会放入2号桶内（因为Math.floor）
  // 如果nums[i] 属于 [3t+3,4t+3] 那么会放入3号桶内（因为Math.floor）
  // 每个桶里面放t+1个数组元素
  // **且只要放进同一个桶，那么桶中的元素一定满足Math.abs(nums[i] - nums[j]) <= t
  // 因为是按照大小的区间分桶的，例如0号桶中的元素都 属于 [0,t]
  function getId (x) {
    return Math.floor(x / (t + 1));
  }

  // 大桶，里面最多有k个小桶。（超过k的时候会删除第一个小桶，滑动窗口的概念
  // 所以在大桶里面的所有元素一定满足math.abs(i - j)<= k
  const map = new Map();

  for (let i = 0; i < nums.length; i++) {
    // m是当前遍历元素将要在的桶ID
    // 将数组中的元素放到小桶里面
    const m = getId(nums[i]);

    // 当前大桶中有这个小桶的话，说明满足Math.abs(nums[i] - nums[j]) <= t(在同一个小桶)
    // 此外，不光是同一个小桶满足<= t，**相邻桶元素也可能满足<= t**
    // 例如（ 属于 [0,t] 那么会放入0号桶内，属于 [t+1,2t+1] 那么会放入1号桶内）
    // 不过只存在于相邻的桶，不相邻的话例如（ 属于 [0,t] 放入0号桶，属于 [t+1,2t+1]1号桶，属于 [2t+2,3t+2] 那么会放入2号桶内）， 0号桶和2号桶最小的差距也是t+2
    if (map.has(m)) {
      // 从前向后遍历元素，此时大桶还没满，满足<= k
      // 在同一个小桶也会满足<=t
      return true;
      // 如果当前小桶的右边桶存在，且两个桶相差小于t
    } else if (map.has(m + 1) && Math.abs(map.get(m + 1) - nums[i]) <= t) {
      return true;
      // 如果当前小桶的左边桶存在，且两个桶相差小于t
    } else if (map.has(m - 1) && Math.abs(map.get(m - 1) - nums[i]) <= t) {
      return true;
    }
    // 将这个桶存到大桶里面
    // 小桶里面的元素是按大小排的
    // 注意 **小桶一次只能存一个** ，后来的会覆盖上一个，不过两个之间符合题意即return true
    map.set(m, nums[i]);
    // 如果i大于k，也就是大桶装满了，则需要把大桶里面的第一个小桶删了。
    if (i >= k) {
      map.delete(getId(nums[i - k]));
    }
  }
  return false;
}

console.time()
console.log('->', containsNearbyAlmostDuplicate(nums, k, t))
console.timeEnd() // 约0.7ms
