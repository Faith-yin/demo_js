/*
 * @Date: 2020-12-02 09:45:15
 * @information: 05 面试题
 */

/**
 * 0. 排序算法
 */

// （1）冒泡排序
// 时间复杂度O(n^2)，稳定排序算法
function bubbleSort (arr) {
	const len = arr.length
	// 外层循环用控制，排序进行多少轮
	for (let i = 0; i < len; i++) {
		// 内层循环用于每一轮的数据比较
		// 注意j的长度范围为 len-i-1
		for (let j = 0; j < len - i - 1; j++) {
			// 相邻元素，大的放后面
			if (arr[j] > arr[j + 1]) {
				// 交换位置
				const temp = arr[j]
				arr[j] = arr[j + 1]
				arr[j + 1] = temp
			}
		}
	}
	return arr
}
console.log('bubbleSort:', bubbleSort([4, 2, 6, 5, 1, 3]))

// （2）选择排序
// 时间复杂度O(n^2)，不稳定
function bubbleSort2 (arr) {
	// 定义index存储最小值的下标
	let index
	for (let i = 0; i < arr.length; i++) {
		index = i
		for (let j = i + 1; j < arr.length; j++) {
			// 寻找最小值
			if (arr[j] < arr[index]) {
				// 保存最小值的下标
				index = j
			}
		}
		// 如果index不是目前的头部元素，则交换两者
		if (index !== i) {
			[arr[i], arr[index]] = [arr[index], arr[i]]
		}
	}
	return arr
}
console.log('bubbleSort2:', bubbleSort2([4, 2, 6, 5, 1, 3]))

// （3）插入排序
// 时间复杂度为O(n²)，稳定
function bubbleSort3 (arr) {
  // 从第2个元素开始遍历
	for (let i = 1; i < arr.length; i++) {
		let j = i
		// 记录要插入的目标元素
		let target = arr[j]
		// 从target所在位置向前遍历，直至找到一个比目标元素小的元素，然后将目标元素插入到该元素之后
		while (j > 0 && arr[j - 1] > target) {
			// 将前一个元素向后移动一个位置
			arr[j] = arr[j - 1]
			j --
		}
		// 将目标元素移动至相应位置
		arr[j] = target
	}
	return arr
}
console.log('bubbleSort3:', bubbleSort3([4, 2, 6, 5, 1, 3]))

// （4）快速排序
// 时间复杂度为O(nlogn)，不稳定
function bubbleSort4 (arr) {
  if (arr.length <= 1) return arr
	// 找到中间节点
	let mid = Math.floor(arr.length / 2)
	// 以中间节点为基准点，比该节点大的值放到right数组中，否则放到left数组中
	let base = arr.splice(mid, 1)[0]
	let left = []
	let right = []
	arr.forEach(item => {
		item > base ? right.push(item) : left.push(item)
	})
	// 重新组合数组
	return bubbleSort4(left).concat(base, bubbleSort4(right))
}
console.log('bubbleSort4:', bubbleSort4([4, 2, 6, 5, 1, 3]))

// （5）归并排序
// 时间复杂度为O(nlogn)，稳定
function bubbleSort5 (arr) {
  let len = arr.length

  // 当每个子序列中仅有1个元素返回
  if (len <= 1) {
    return arr
  }
  // 将给定的列表分两半
	let num = Math.floor(len / 2)
	let left = bubbleSort5(arr.slice(0, num))
	let right = bubbleSort5(arr.slice(num, arr.length))
	return merge(left, right)

	function merge (left, right) {
		let [l, r] = [0, 0]
		let result = []
		// 从left 和 right区域中各取第一个元素，比较大小
		while(l < left.length && r < right.length) {
			// 将较小的元素添加到result中，然后从较小元素所在的区域内取出下一个元素，继续进行比较
			if (left[l] < right[r]) {
				result.push(left[l])
				l ++
			} else {
				result.push(right[r])
				r ++
			}
		}
		// 如果left 或 right 有一方为空，则直接将另一方的所有元素依次添加到result中
		result = result.concat(left.slice(l, left.length))
		result = result.concat(right.slice(r, right.length))
		return result
	}
}
console.log('bubbleSort5:', bubbleSort5([4, 2, 6, 5, 1, 3]))






/*
 * @Date: 2021-09-02 16:27:02
 * @information: 31
 */

/**
1、把任意正整数的每位数字拆分出来，放到数组
中，例如: 123， 结果为[1, 2,3]。要求:不能调用
任何现有函数。
 */

let n = 12345

function trans (num) {
  num = String(num)
  let arr = []
  for (let i = 0; i < num.length; i++) {
    arr[i] = Number(num[i])
  }
  return arr
}
// console.log('1 结果: ', trans(n))


/**
2、找出英文句子中最大单词长度，单词按空格分
割，例如: "this is a word",结果为4。要求，不能
调用任何现有函数。
 */

let w = "this is a word yyyyy"

function findLong (str) {
  let arr = [], num = 0, len = 0
  for (let i = 0; i < str.length; i++) {
    if (str[i] === ' ') {
      arr[num] = i
      num++
    } else if (i === 0) {
      arr[num] = i - 1
      num++
    } else if (i === str.length - 1) {
      arr[num] = i + 1
      num++
    }
  }
  for (let i = 1; i < arr.length; i++) {
    len = arr[i] - arr[i - 1] - 1 > len ? arr[i] - arr[i - 1] - 1 : len
  }
  return len
}
// console.log('2 结果: ', findLong(w))


/**
3、把两个从小到大排序的数组合并成一个从小到
大排序的数组。例如: [1,3, 8], [2, 3, 9, 10]合并结
果: [1,2,3,8, 9, 10]。要求算法复杂度0(m+n)
 */
let arr1 = [1, 3, 8], arr2 = [2, 3, 9, 10]

function merge (arr1, arr2) {
  // sort 的底层是冒泡排序 O（m*n）
  let arr = arr1.concat(arr2).sort((a, b) => a - b)
  return arr
}
// console.log('3 结果1: ', merge(arr1, arr2))

// 快速排序 O(m)
function merge2 (arr1, arr2) {
  if (arr2 && arr2.length) {
    arr1.push(...arr2)
  }
  if (!arr2 && arr1.length < 2) {
    return arr1
  }
  const piv = arr1[0] // 第一个元素作为基准值
  let lArr = [], rArr = [] // 左侧 < 基准值 右侧 > 基准值
  for (let i = 1; i < arr1.length; i++) {
    if (arr1[i] < piv) {
      lArr.push(arr1[i])
    } else {
      rArr.push(arr1[i])
    }
  }
  // 递归调用，结果返回新数组
  return [...merge2(lArr), piv, ...merge2(rArr)]
}
// console.log('3 结果2: ', merge2(arr1, arr2))
// console.log('3 结果2: ', merge2([9, 2, 1, 8, 13, 11]))


/**
4、求斐波拉契数列第n项。要求，不能用递归,
不能把前面的所有项存下来。斐波拉契数列例子:
1, 1, 2, 3, 5, 8。前两项为1,后面每项为其前面两项
之和。
 */
// 有如下递推的方法定义：F(1)=1,F(2)=1,F(n)=F(n-1)+F(n-2)(n>=2,n是正整数)
// 递归实现:
function fun0401 (n) {
  if (n < 0) throw Error('输入的数字不能小于0')
  if (n == 1 || n == 2) {
    return 1
  } else {
    return fun0401(n - 1) + fun0401(n - 2)
  }
}
// console.log('4 结果1: ', fun0401(11))

// 利用变量实现，只保存当前的值和前面两个的值
function fun0402 (n) {
  let pre = 1, cur = 1, data
  if (n < 0) throw Error('输入的数字不能小于0')
  if (n == 1 || n == 2) {
    return 1
  } else {
    for (let i = 3; i <= n; i++) {
      data = pre + cur
      pre = cur
      cur = data
    }
  }
  return data
}
// console.log('4 结果2: ', fun0401(10))


/**
5、找出文件目录里最大文件长度，包含子目录里
的。要用到的函数如下:
is_ _dir(f): 看f是否目录
files(f):找到目录f下的所有文件积子目录
len(f):得到文件f的长度。
 */






