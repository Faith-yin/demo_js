// https://liaoxuefeng.com/books/javascript/nodejs/basic-modules/stream/index.html

/**
 * crypto: crypto模块的目的是为了提供通用的加密和哈希算法
 */
const { randomInt, createHash } = require("node:crypto")

process.nextTick(() => {
  console.log("process.nextTick")
})

process.on("exit", () => {
  console.log("exit")
})

// console.log(global)

console.log("🔥 =======")

// console.log(process)

const n = randomInt(0, 100) // 0~100之间的随机数
console.log("🔥 0~100之间的随机数:", n)


// MD5是一种常用的哈希算法，用于给任意数据一个“签名”
const hash = createHash('md5');
// 可任意多次调用update():
hash.update('Hello, world!');
hash.update('Hello, nodejs!');
console.log(hash.digest('hex'));






