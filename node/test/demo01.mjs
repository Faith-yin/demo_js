/**
 * fs模块是Node.js官方提供的、用来操作文件的模块。
 */
import { readFile, writeFile, stat } from "node:fs"

readFile("text.txt", "utf-8", function (err, data) {
  if (err) {
    console.log(err)
  } else {
    console.log("text数据：", data)
  }
})

// let data = "Hello, Node.js"
// writeFile("text.txt", data, function (err) {
//   if (err) {
//     console.log(err)
//   }
// })

// stat: 获取文件信息
stat("text.txt", function (err, st) {
  if (err) {
    console.log(err)
  } else {
    // 是否是文件:
    console.log("isFile: " + st.isFile())
    // 是否是目录:
    console.log("isDirectory: " + st.isDirectory())
    if (st.isFile()) {
      // 文件大小:
      console.log("size: " + st.size)
      // 创建时间, Date对象:
      console.log("birth time: " + st.birthtime)
      // 修改时间, Date对象:
      console.log("modified time: " + st.mtime)
    }
  }
})

/**
 * stream模块是Node.js官方提供的、用来操作流的模块。
 */
import { createReadStream } from "node:fs"

// 打开流:
let rs = createReadStream("text.txt", "utf-8")

// 读取到数据:
rs.on("data", (chunk) => {
  console.log("---- chunk ----")
  console.log(chunk)
})

// 结束读取:
rs.on("end", () => {
  console.log("---- end ----")
})

// 出错:
rs.on("error", (err) => {
  console.log(err)
})
