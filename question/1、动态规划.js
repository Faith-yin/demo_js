/**
 * 思路：确定状态转移方程：不同规模的相同子问题之间的关系
 */



/**
 * 300. 最长递增子序列 (medium) https://leetcode.cn/problems/longest-increasing-subsequence/
 * 给你一个整数数组 nums ，找到其中最长严格递增子序列的长度。
 * 例如：[2, 3, 7, 18, 21] 是 [10, 9, 2, 5, 3, 7, 101, 18, 21] 的最长严格递增子序列
 * 
 * 状态转移方程：dp[i] = max(dp[i], dp[j] + 1) // i: 外层循环的下标，j：内层小于i的循环下标
 */
const lengthOfLIS = (nums) => {
  if (nums.length === 0) return 0
  let dp = Array(nums.length).fill(1)
  let result = 1

  for (let i = 1; i < nums.length; i++) {
    for (let j = 0; j < i; j++) {
      if (nums[i] > nums[j]) { // 当nums[i] > nums[j]，则构成一个上升对
        dp[i] = Math.max(dp[i], dp[j] + 1)
      }
    }
    result = Math.max(result, dp[i])
  }

  return result
}
console.log('最长递增子序列长度：', lengthOfLIS([10, 9, 2, 5, 3, 7, 101, 18, 21])) // 4


/**
 * 背包最大价值问题
 * 如下：有一些物品，每一个物品有自身的价值（value）和重量（weight），将这些物品（不限数量）放入一个
 * 有最大限重（其中物品总重不超过最大限重）的背包中，使背包中的物品价值最大化，求此最大价值。
 * 
 * 物品下标	 			0	 	1		2	  3	  4	
 * 价值（value） 	5		10	3		6		3		
 * 重量（weight）	2		5		1   4   3
 * 
 * 背包：最大限重（bagWeight）：6
 * 
 * 解答：
 * （1）图例
 * dp[i][j]表示在限额j的情况下，i范围内的最大价值，即最优解。
 * 												总重量限额j
 * 									 0	 1 	2 	3	 4	 5	 6
 * 物品下标i	0-0		 口  口  口  口 口  口  口 
 * 					  0-1	 	口  口  口  口 口  口  口 
 * 					  0-2	 	口  口  口  口 口  口  口 
 * 					  0-3	 	口  口  口  口 口  口  口 
 * 					  0-4	 	口  口  口  口 口  口  口 
 * 
 * （2）状态转移方程：
 * dp[i][j] = max(选第i件物品，不选第i件物品)
 * 					= max(value[i] + dp[i-1][j-weight[i]]，dp[i-1][j])
 * 空间复杂度: O(n*bagWeight)
 * 
 * 优化，只需要存储此行与上一行，滚动求值：next[j] = max(value[i] + result[j-weight[i]], result[j])
 * 空间复杂度: O(bagWeight)
 */

function getMaxValue (bagWeight, value, weight) {
	let result = []
	// 初始化第一行
	for (let j = 0; j <= bagWeight; j++) {
		result[j] = j >= weight[0] ? value[0] : 0
	}
	// 循环剩下的物品
	for (let i = 1; i < value.length; i++) {
		const next = []
		for (let j = 0; j <= bagWeight; j++) {
			if (j >= weight[i]) { // 背包的空间需足够
				next[j] = Math.max(value[i] + result[j - weight[i]], result[j])
			} else {
				next[j] = result[j]
			}
		}
		result = next
	}
	return result[bagWeight]
}
console.log('背包最大价值问题: ', getMaxValue(6, [5, 10, 3, 6, 3], [2, 5, 1, 4, 3]))


/**
 * 322. 凑零钱问题
 * 给你一个整数数组 coins ，表示不同面额的硬币，每种硬币的数量无限，
 * 再给一个总金额 amount，问你最少需要几枚硬币凑出这个金额，如果不可能凑出，算法返回 -1 。
 * 
 * 状态转移方程：dp[i] = min(dp[i], dp[i-j] + 1) // i: 总金额，j：硬币面值
 * 
 */
function coinChange (coins, amount) {
	// 初始化最优解数组
	const dp = Array(amount + 1).fill(amount + 1)
	dp[0] = 0
	// 自底向上使用 dp table 循环穷举在每种amount情况下（子问题）的最优解
	for (let i = 1; i <= amount; i++) {
		for (let j = 0; j < coins.length; j++) {
			if (coins[j] > i) continue
			dp[i] = Math.min(dp[i], dp[i-coins[j]] + 1)
		}
	}
	return dp[amount] === amount + 1 ? -1 : dp[amount]
}
console.log('1凑零钱问题: ', coinChange([1, 2, 5], 11))
console.log('2凑零钱问题: ', coinChange([1, 2, 3, 5], 18))


/**
 * 354. 俄罗斯套娃信封问题
 * 
给你一个二维整数数组 envelopes ，其中 envelopes[i] = [wi, hi] ，表示第 i 个信封的宽度和高度。
当另一个信封的宽度和高度都比这个信封大的时候，这个信封就可以放进另一个信封里，如同俄罗斯套娃一样。
请计算 最多能有多少个 信封能组成一组“俄罗斯套娃”信封（即可以把一个信封放到另一个信封里面）。
注意：不允许旋转信封。

输入：envelopes = [[5,4],[6,4],[6,7],[2,3]]
输出：3
解释：最多信封的个数为 3, 组合为: [2,3] => [5,4] => [6,7]。

解答：先对宽度 w 进行升序排序，如果遇到 w 相同的情况，则按照高度 h 降序排序；
之后把所有的 h 作为一个数组，在这个数组上计算 LIS（最长递增子序列） 的长度就是答案。
 */
function maxEnvelopes (envelopes) {
	// 按宽度升序排列，如果宽度一样，则按高度降序排列
	envelopes.sort((a, b) => a[0] === b[0] ? b[1] - a[1] : a[0] - b[0])
	// 对数组高度寻找LTS
	const height = envelopes.map(el => el[1])
	// 调用前文LTS方法
	return lengthOfLIS(height)
}
console.log('俄罗斯套娃信封问题: ', maxEnvelopes([[5,4], [6,4], [6,7], [2,3]]))


/**
 * 1425. 带限制的子序列和
给你一个整数数组 nums 和一个整数 k ，请你返回 非空 子序列元素和的最大值，
子序列需要满足：子序列中每两个 相邻 的整数 nums[i] 和 nums[j] ，
它们在原数组中的下标 i 和 j 满足 i < j 且 j - i <= k 。

数组的子序列定义为：将数组中的若干个数字删除（可以删除 0 个数字），剩下的数字按照原本的顺序排布。

输入：nums = [10,2,-10,5,20], k = 2
输出：37
解释：子序列为 [10, 2, 5, 20] 。

解答：dp + 单调队列
dp[i]: 以第 i 个元素结尾的最大和只和 i 之前，与 i 相差 k 以内的位置为结尾的最大和有关
状态转移方程：dp[i] = max(dp[j], 0) + nums[i] 
 */
const constrainedSubsetSum = function(nums, k) {
	let len = nums.length
	if (len === 0) return 0
	let dp = Array(len).fill(0)
	let max = -Infinity
	let queue = []

	for (let i = 0; i < len; i++) {
		let cur = nums[i]
		// 如果队首的 j 与 i 的差值大于 k，则不满足要求，弹出
		while (queue.length && i - queue[0] > k) queue.shift()
		
		// 此时队首的 j 即为最优的 j 值
		dp[i] = Math.max(dp[queue[0]] || 0, 0) + cur
		// 维护队列的单调性，不断从队尾弹出元素
		while (queue.length && dp[queue[queue.length - 1]] < dp[i]) queue.pop()
		// 将 i 作为之后的新 j 值放入队尾
		queue.push(i)
		max = Math.max(dp[i], max)
	}

	return max
}
console.log('带限制的子序列和: ', constrainedSubsetSum([10, 2, -10, 5, 20], 2))


/**
 * 368. 最大整除子集
给你一个由 无重复 正整数组成的集合 nums ，请你找出并返回其中最大的整除子集 answer ，
子集中每一元素对 (answer[i], answer[j]) 都应当满足：
answer[i] % answer[j] == 0 ，或
answer[j] % answer[i] == 0
如果存在多个有效解子集，返回其中任何一个均可。

输入：nums = [1,2,3]
输出：[1,2]
解释：[1,3] 也会被视为正确答案。

解答：
转移方程：0~i遍历：当nums[i] % nums[j] == 0，dp[i] = max(dp[i], dp[j] + 1)

例如 [1,2,3,4,5]
求出每个值对应的整除数个数(包括其本身)

1	 2  3  4  5
1  2  2  3  2
然后再根据个数数组由大到小倒推最大子集
 */

var largestDivisibleSubset = function(nums) {
	nums.sort((a, b) => a - b)
	let dp = Array(nums.length).fill(1)
	for (let i = 1; i < nums.length; i++) {
		for (let j = 0; j < i; j++) {
			if (nums[i] % nums[j] === 0) {
				dp[i] = Math.max(dp[i], dp[j] + 1)
			}
		}
	}
	// 倒推数组获得最大子集
	let maxSize = Math.max(...dp)
	let maxVal = nums[dp.indexOf(maxSize)]
	let arr = []
	for (let i = nums.length - 1; i >= 0 && maxSize > 0 ; i--) {
		if (dp[i] === maxSize && maxVal % nums[i] === 0) {
			arr.push(nums[i])
			maxVal = nums[i]
			maxSize --
		}
	}

	return arr
}
console.log('1最大整除子集: ', largestDivisibleSubset([1,2,3,4,5])) // [1,2,4]
console.log('2最大整除子集: ', largestDivisibleSubset([4,8,10,240])) // [4,8,240]


/**
 * 70 爬楼梯
 * 假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？

输入：n = 3
输出：3
解释：有三种方法可以爬到楼顶。
1. 1 阶 + 1 阶 + 1 阶
2. 1 阶 + 2 阶
3. 2 阶 + 1 阶

转移方程：dp[n] = dp[n-1] + dp[n-2]
 */
var climbStairs = function(n) {
	if (n === 1 || n === 2) return n
	let p = 1
	let q = 2
	for (let i = 3; i <= n; i++) {
		let sum = p + q
		p = q
		q = sum
	}
	return q
};
console.log('爬楼梯1: ', climbStairs(11)) // 144

// 一次可以爬m阶
function climbStairs2 (n, m) {
	const dp = new Array(n + 1).fill(0)
	dp[1] = 1
	dp[2] = 2
	for (let i = 3; i <= n; i++) {
		// 若m = 3，则dp[4] = dp[1] + dp[2] + dp[3]
		for (let j = i - 1; j >= Math.max(i - m, 1); j--) {
			dp[i] += dp[j]
		}
	}
	console.log('dp', dp)
	return dp[n]
}
console.log('爬楼梯2: ', climbStairs2(5, 3)) // 11





























