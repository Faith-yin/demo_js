/**
 * 二分查找法举例
 */
const binarySearch = (arr, target) => {
  let left = 0
  let right = arr.length - 1
  for (let i = 0; i < arr.length; i++) {
    let mid = Math.floor((left + right) / 2)
    if (arr[mid] === target) {
      return mid
    } else if (arr[mid] < target) {
      left = mid + 1
    } else {
      right = mid - 1
    }
  }
  return -1
}
const arr = [1, 3, 5, 7, 9, 11, 13]
const target = 7
console.log('二分查找法举例: ', binarySearch(arr, target))

/**
 * 300. 最长递增子序列 (medium) https://leetcode.cn/problems/longest-increasing-subsequence/
 * 给你一个整数数组 nums ，找到其中最长严格递增子序列的长度。
 * 例如：[2, 3, 7, 18, 21] 是 [10, 9, 2, 5, 3, 7, 101, 18, 21] 的最长严格递增子序列
 */
const lengthOfLIS = (nums) => {
  let n = nums.length
  if (n <= 1) return n

  // 存放最长上升子序列数组
  let tail = [nums[0]]
  for (let i = 0; i < n; i++) {
    if (nums[i] > tail[tail.length - 1]) { // 当nums中的元素比tail中的最后一个大时，可以放心push进tail
      tail.push(nums[i])
    } else { // 否则进行二分查找
      let left = 0
      let right = tail.length - 1
      while (left < right) {
        let mid = (left + right) >> 1
        if (tail[mid] < nums[i]) {
          left = mid + 1
        } else {
          right = mid
        }
      }
      tail[left] = nums[i] // 将nums[i]放置到合适的位置，此时前面的元素都比nums[i]小
    }
  }

  return tail.length
}
console.log('最长递增子序列长度：', lengthOfLIS([10, 9, 2, 5, 3, 7, 101, 18, 21])) // 4













