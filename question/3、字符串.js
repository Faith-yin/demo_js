
/** 字符串总结：
 * 
 * 语言特性方法（for循环..）
 * 双指针法
 * KMP（当出现字符串不匹配时，可以知道一部分之前已经匹配的文本内容，可以利用这些信息避免从头再去做匹配了）
 * 
 */

/**
 * 相关题目：
 * 344 反转字符串 - 语言特性 or 双指针
 * 541. 反转字符串II - for循环
 * 151. 翻转字符串里的单词 - 语言特性 or 自行编写对应函数
 * 剑指Offer 58-II. 左旋转字符串 - 语言特性
 * 剑指Offer 05. 替换空格 - 语言特性 or 双指针
 * 28. 实现 strStr() 函数 - 双指针 or KMP
 * 459.重复的子字符串 - for循环 or KMP
 * 
 */


/** 344 反转字符串
 * 编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 char[] 的形式给出。
不要给另外的数组分配额外的空间，你必须原地修改输入数组、使用 O(1) 的额外空间解决这一问题。
你可以假设数组中的所有字符都是 ASCII 码表中的可打印字符。

示例 1：
输入：["h","e","l","l","o"]
输出：["o","l","l","e","h"]

示例 2：
输入：["H","a","n","n","a","h"]
输出：["h","a","n","n","a","H"]
 */
// （1）语言特性方法
function reverseString (strArr) {
	const len = strArr.length
	for (let i = len - 1; i >= 0; i--) {
		const str = strArr[i]
		strArr.push(str)
	}
	strArr.splice(0, len)
}
const arr = ["h","e","l","l","o"]
reverseString(arr)
console.log('344 反转字符串1:', arr)

// （2）双指针
function reverseString2 (strArr) {
	for (let i = 0, j = strArr.length - 1; i < strArr.length / 2; i++, j--) {
		[strArr[i], strArr[j]] = [strArr[j], strArr[i]]
	}
}
const arr2 = ["H","a","n","n","a","h"]
reverseString2(arr2)
console.log('344 反转字符串2:', arr2)


/**
 * 541. 反转字符串II
 * 给定一个字符串 s 和一个整数 k，从字符串开头算起, 每计数至 2k 个字符，就反转这 2k 个字符中的前 k 个字符。
- 如果剩余字符少于 k 个，则将剩余字符全部反转。
- 如果剩余字符小于 2k 但大于或等于 k 个，则反转前 k 个字符，其余字符保持原样。

示例:
输入: s = "abcdefg", k = 2
输出: "bacdfeg"
 */
// for循环
function reverseStr (s, k) {
	const len = s.length
	const sArr = s.split('')
	for (let i = 0; i < len; i += k * 2) { // 每隔2k个字符的前k个字符反转
		let left = i, right = Math.min(i + k - 1, len - 1) // min 判断最后的长度是否在k内，取较小的值进行反转
		while (left < right) {
			[sArr[left], sArr[right]] = [sArr[right], sArr[left]]
			left++
			right--
		}
	}
	return sArr.join('')
}
console.log('541. 反转字符串II: ', reverseStr('abcdefg', 2))


/** 151.翻转字符串里的单词
给定一个字符串，逐个翻转字符串中的每个单词。

示例 1：
输入: "the sky is blue"
输出: "blue is sky the"

示例 2：
输入: "  hello world!  "
输出: "world! hello"
解释: 输入字符串可以在前面或者后面包含多余的空格，但是反转后的字符不能包括。

示例 3：
输入: "a good   example"
输出: "example good a"
解释: 如果两个单词间有多余的空格，将反转后单词间的空格减少到只含一个。
 */
// （1）使用语言特性
function reverseWords (s) {
	return s.trim().split(/\s+/).reverse().join(' ')
}
console.log('151.翻转字符串里的单词1:', reverseWords('a good   example '))

// （2）自行编写对应的函数 - 先整体反转再局部反转
function reverseWords2 (s) {
	const strArr = Array.from(s)
	// 移除多余空格
	removeExtraSpaces(strArr)
	// 翻转 - 整体反转
	reverse(strArr, 0, strArr.length - 1)
	let start = 0;

	// 局部反转
	for(let i = 0; i <= strArr.length; i++) {
		if (strArr[i] === ' ' || i === strArr.length) {
			reverse(strArr, start, i - 1);
			start = i + 1;
		}
	}

	 return strArr.join('')
}
// // 删除多余空格
function removeExtraSpaces (strArr) {
	let slowIndex = 0
	let fastIndex = 0
	// 移出开始位置和重复的空格
	while (fastIndex < strArr.length) {
		if (strArr[fastIndex] === ' ' && (fastIndex === 0 || strArr[fastIndex - 1] === ' ')) {
			fastIndex++
		} else {
			strArr[slowIndex++] = strArr[fastIndex++]
		}
	}
	// 移出末尾空格
	strArr.length = strArr[slowIndex - 1] === ' ' ? slowIndex - 1 : slowIndex
}
// 翻转从 start 到 end 的字符
function reverse (strArr, start, end) {
  let left = start;
  let right = end;

  while(left < right) {
    // 交换
    [strArr[left], strArr[right]] = [strArr[right], strArr[left]];
    left++;
    right--;
  }
}
console.log('151.翻转字符串里的单词2:', reverseWords2('a good   example '))



/** 剑指Offer58-II.左旋转字符串
字符串的左旋转操作是把字符串前面的若干个字符转移到字符串的尾部。
请定义一个函数实现字符串左旋转操作的功能。比如，输入字符串"abcdefg"和数字2，该函数将返回左旋转两位得到的结果"cdefgab"。

示例 1：
输入: s = "abcdefg", k = 2
输出: "cdefgab"

示例 2：
输入: s = "lrloseumgh", k = 6
输出: "umghlrlose"

限制：
1 <= k < s.length <= 10000
 */
// （1）语言特性1
function reverseLeftWords (s, k) {
	let i = 0
	while (i < k) {
		s += s[i]
		i++
	}
	return s.slice(k)
}
console.log('剑指Offer58-II.左旋转字符串:', reverseLeftWords('lrloseumgh', 6))

// （1）语言特性2 拼接两个字符，截取符合要求的部分
function reverseLeftWords1 (s, k) {
	return (s + s).slice(k, s.length + k)
}
console.log('剑指Offer58-II.左旋转字符串1:', reverseLeftWords1('lrloseumgh', 6))

// （1）语言特性2 拼接两个字符，截取符合要求的部分
function reverseLeftWords11 (s, k) {
	return s.slice(k) + s.slice(0, k)
}
console.log('剑指Offer58-II.左旋转字符串11:', reverseLeftWords11('lrloseumgh', 6))

// （2）先局部反转再整体反转
// 反转区间为前n的子串、反转区间为n到末尾的子串，反转整个字符串
function reverseLeftWords2 (s, k) {
	const sArr = Array.from(s)
	reverse(sArr, 0, k - 1)
	reverse(sArr, k, sArr.length - 1)
	reverse(sArr, 0, sArr.length - 1)
	return sArr.join('')
}
console.log('剑指Offer58-II.左旋转字符串2:', reverseLeftWords2('lrloseumgh', 6))



/**
 * 剑指Offer 05.替换空格
 * 请实现一个函数，把字符串 s 中的每个空格替换成"%20"。

示例 1： 输入：s = "We are happy."
输出："We%20are%20happy."
 */
// （1）语言特性
function replaceSpace (s) {
	const sArr = s.split('')
	for (let i = 0; i < sArr.length; i++) {
		sArr[i] === ' ' && (sArr[i] = '%20')
	}
	return sArr.join('')
}
console.log('剑指Offer 05.替换空格1:', replaceSpace("We are happy."))

// （2）双指针
function replaceSpace2 (s) {
	const sArr = s.split('')
	let count = 0
	// 计算空格数量
	for (let i = 0; i < sArr.length; i++) {
		sArr[i] === ' ' && count++
	}

  let left = sArr.length - 1
  let right = sArr.length + count * 2 - 1

  while(left >= 0) {
    if (sArr[left] === ' ') {
      sArr[right--] = '0'
      sArr[right--] = '2'
      sArr[right--] = '%'
      left--;
    } else {
      sArr[right--] = sArr[left--];
    }
  }

	return sArr.join('')
}
console.log('剑指Offer 05.替换空格2:', replaceSpace2("We are happy."))


/** 28. 实现 strStr() 函数
给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从0开始)。
如果不存在，则返回  -1。

示例 1: 输入: haystack = "hello", needle = "ll" 输出: 2
示例 2: 输入: haystack = "aaaaa", needle = "bba" 输出: -1
 */
// （1）双指针
function strStr (haystack, needle) {
	if (needle.length === 0) return 0
	let startIndex = -1
	for (let i = 0; i < haystack.length; i++) {
		if (haystack[i] === needle[0]) {
			let str1 = '', str2 = '', j = 0
			while (j < needle.length) {
				str1 += haystack[i + j]
				str2 += needle[j]
				j++
			}
			if (str1 === str2) {
				startIndex = i
				break
			}
		}
	}
	return startIndex
}
console.log('28. 实现 strStr():', strStr('leetcode', 'leeto')) // -1
console.log('28. 实现 strStr():', strStr('mississippi', 'issip')) // 4

// （2）KMP: 前缀表统一不减一
// KMP的经典思想就是:当出现字符串不匹配时，可以记录一部分之前已经匹配的文本内容，利用这些信息避免从头再去做匹配
function strStr2 (haystack, needle) {
	if (needle.length === 0) return 0;

	// 求模式串的前缀表
	const getNext = (needle) => {
		let next = [];
		let j = 0;
		next.push(j);

		for (let i = 1; i < needle.length; ++i) {
			while (j > 0 && needle[i] !== needle[j]) {
				j = next[j - 1];
			}
			if (needle[i] === needle[j]) {
				j++;
			}
			next.push(j);
		}
		return next;
	}

	let next = getNext(needle);
	let j = 0;
	for (let i = 0; i < haystack.length; ++i) {
		while (j > 0 && haystack[i] !== needle[j]) {
			j = next[j - 1];
		}
		if (haystack[i] === needle[j]) {
			j++;
		}
		if (j === needle.length) return (i - needle.length + 1);
	}

	return -1;
}
console.log('28. 实现 strStr()2:', strStr2('aabaabaaf', 'aabaaf')) // 4


/** 459.重复的子字符串
给定一个非空的字符串，判断它是否可以由它的一个子串重复多次构成。给定的字符串只含有小写英文字母，并且长度不超过10000。

示例 1:
输入: "abab"
输出: True
解释: 可由子字符串 "ab" 重复两次构成。

示例 2:
输入: "aba"
输出: False

示例 3:
输入: "abcabcabcabc"
输出: True
解释: 可由子字符串 "abc" 重复四次构成。 (或者子字符串 "abcabc" 重复两次构成。)
 */
/**
 * @param {string} s
 * @return {boolean}
 */
// （1）for循环
const repeatedSubstringPattern = function(s) {
	const len = s.length
	let childStr = ''
	for (let i = 1; i <= len / 2; i++) {
		childStr += s[i-1]
		if (s[0] === s[i]) {
			if (len % i === 0) {
				let repeatNum = len / i
				let newStr = ''
				while (repeatNum > 0) {
					newStr += childStr
					repeatNum--
				}
				if (newStr === s) {
					return true
				}
			}
		}
	}
	return false
}
console.log('459.重复的子字符串:', repeatedSubstringPattern('abcabcabcabc'))

// （2）KMP
const repeatedSubstringPattern2 = function(s) {
	if (s.length === 0 ) return false

	// 求前缀表
	const getNext = s => {
		const next = []
		let j = 0
		next.push(j)

		for (let i = 1; i < s.length; i++) {
			while (j >0 && s[i] !== s[j]) {
				j = next[j - 1]
			}
			if (s[i] === s[j]) {
				j++
			}
			next.push(j)
		}

		return next
	}
	
	const next = getNext(s)
	if (next[next.length - 1] !== 0 && s.length % (s.length - next[next.length - 1]) === 0) {
		return true
	}
	return false
}
console.log('459.重复的子字符串2:', repeatedSubstringPattern2('abcabcabcabc'))







