/**
 * 
 * 二分查找：在数组本身内部搜索。时间复杂度：O(logn)
 * left = 0; right = len - 1; mid = Math.floor((left + right) / 2)
 * 
 * 双指针法（快慢指针法）： 通过一个快指针和慢指针在一个for循环下完成两个for循环的工作
 * i = 0; j = 0 或 len - 1
 * 
 * 滑动窗口：就是不断的调节子序列的起始位置和终止位置，从而得出我们要想的结果
 * i = 0; j =0
 * 
 * 螺旋矩阵：循环不变量原则
 * 
 */

/**
 * 相关题目：
 * 704. 二分查找 - 二分查找
 * 35. 搜索插入位置 - 二分查找
 * 34. 在排序数组中查找元素的第一个和最后一个位置 - 二分查找
 * 69. x 的平方根 - 二分查找
 * 367. 有效的完全平方数 - 二分查找
 * 27. 移除元素 - 双指针法 or 双循环
 * 283. 移动零 - 双指针法 or 双循环
 * 977. 有序数组的平方 - 双指针法
 * 209. 长度最小的子数组 - 滑动窗口 or 双指针法
 * 904. 水果成篮 - Map or 滑动窗口
 * 76. 最小覆盖子串 - 滑动窗口
 * 59. 螺旋矩阵 II
 * 
 */



/** 704. 二分查找
给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target  ，写一个函数搜索 nums 中的 target，
如果目标值存在返回下标，否则返回 -1。(假设 nums 中的所有元素是不重复的)

示例 1:

输入: nums = [-1,0,3,5,9,12], target = 9     
输出: 4       
解释: 9 出现在 nums 中并且下标为 4  

示例 2:
输入: nums = [-1,0,3,5,9,12], target = 2     
输出: -1        
解释: 2 不存在 nums 中因此返回 -1 
 */
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
// 左闭右闭区间 [left, right]
var search = function(nums, target) {
	let left = 0, right = nums.length - 1
	while (left <= right) {
		// let mid = Math.floor((right - left) / 2) + left
		let mid = Math.floor((left + right) / 2)
		if (target < nums[mid]) {
			right = mid - 1
		} else if (target > nums[mid]) {
			left = mid + 1
		} else {
			return mid
		}
	}
	return -1
}
console.log('704. 二分查找:', search([-1,0,3,5,9,12], 9))


/** 35.搜索插入位置
给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
你可以假设数组中无重复元素。

示例 1:

输入: [1,3,5,6], 5
输出: 2

示例 2:
输入: [1,3,5,6], 2
输出: 1
示例 3:

输入: [1,3,5,6], 7
输出: 4

示例 4:
输入: [1,3,5,6], 0
输出: 0

解答：四种情况
- 目标值在数组所有元素之前
- 目标值等于数组中某一个元素
- 目标值插入数组中的位置
- 目标值在数组所有元素之后
 */
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
// 二分查找
var searchInsert = function(nums, target) {
	let left = 0, right = nums.length - 1
	while (left <= right) {
		let mid = Math.floor((left + right) / 2)
		if (nums[mid] < target) {
			left = mid + 1
		} else if (nums[mid] > target) {
			right = mid - 1
		} else {
			return mid
		}
	}
	return right + 1
}
console.log('35.搜索插入位置:', searchInsert([1,3,5,6], 2))


/** 34. 在排序数组中查找元素的第一个和最后一个位置
给定一个按照升序排列的整数数组 nums，和一个目标值 target。找出给定目标值在数组中的开始位置和结束位置。
如果数组中不存在目标值 target，返回 [-1, -1]。

进阶：你可以设计并实现时间复杂度为 $O(\log n)$ 的算法解决此问题吗？

示例 1：
输入：nums = [5,7,7,8,8,10], target = 8
输出：[3,4]

示例 2：
输入：nums = [5,7,7,8,8,10], target = 6
输出：[-1,-1]

示例 3：
输入：nums = [], target = 0
输出：[-1,-1]

解答： 三种情况
情况一：target 在数组范围的右边或者左边，例如数组{3, 4, 5}，target为2或者数组{3, 4, 5},target为6，此时应该返回{-1, -1}
情况二：target 在数组范围中，且数组中不存在target，例如数组{3,6,7},target为5，此时应该返回{-1, -1}
情况三：target 在数组范围中，且数组中存在target，例如数组{3,6,7},target为6，此时应该返回{1, 1}
 */
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
// 二分查找
var searchRange = function(nums, target) {
	// 寻找左边界
	const getLeftBorder = (nums, target) => {
		let left = 0, right = nums.length - 1, leftBorder = -2
		while (left <= right) {
			let mid = Math.floor((left + right) / 2)
			if (nums[mid] >= target) {
				right = mid - 1
				leftBorder = right
			} else {
				left = mid + 1
			}
		}
		return leftBorder
	}
	// 寻找右边界
	const getRightBorder = (nums, target) => {
		let left = 0, right = nums.length - 1, rightBorder = -2
		while (left <= right) {
			let mid = Math.floor((left + right) / 2)
			if (nums[mid] > target) {
				right = mid - 1
			} else {
				left = mid + 1
				rightBorder = left
			}
		}
		return rightBorder
	}

	// 情况1
	if (target < nums[0] || target > nums[nums.length - 1]) return [-1, -1]
	// 情况2
	let leftBorder = getLeftBorder(nums, target)
	let rightBorder = getRightBorder(nums, target)
	if (rightBorder - leftBorder > 1) {
		return [leftBorder + 1, rightBorder - 1]
	}
	// 情况3
	return [-1, -1]

};
console.log('34. 在排序数组中查找元素的第一个和最后一个位置:', searchRange([5,7,7,8,8,10], 8))


/** 69. x 的平方根 
给你一个非负整数 x ，计算并返回 x 的 算术平方根 。
由于返回类型是整数，结果只保留 整数部分 ，小数部分将被 舍去 。
注意：不允许使用任何内置指数函数和算符，例如 pow(x, 0.5) 或者 x ** 0.5 。

示例 1：
输入：x = 4
输出：2

示例 2：
输入：x = 8
输出：2
解释：8 的算术平方根是 2.82842..., 由于返回类型是整数，小数部分将被舍去。
 */
/**
 * @param {number} x
 * @return {number}
 */
// 二分查找
var mySqrt = function(x) {
	// res = x  0，1的算术平方根是它们本身
  let left = 1, right = Math.floor(x / 2), res = x
  while (left <= right) {
    let mid = Math.floor((left + right) / 2)
    if (mid * mid < x) {
      left = mid + 1
      res = mid
    } else if (mid * mid > x) {
      right = mid - 1
    } else {
      return mid
    }
  }
  return res
}
console.log('69. x 的平方根:', mySqrt(8))


/** 367. 有效的完全平方数
给你一个正整数 num 。如果 num 是一个完全平方数，则返回 true ，否则返回 false 。
完全平方数 是一个可以写成某个整数的平方的整数。换句话说，它可以写成某个整数和自身的乘积。
不能使用任何内置的库函数，如  sqrt 。

示例 1：
输入：num = 16
输出：true
解释：返回 true ，因为 4 * 4 = 16 且 4 是一个整数。

示例 2：
输入：num = 14
输出：false
解释：返回 false ，因为 3.742 * 3.742 = 14 但 3.742 不是一个整数。
 */
/**
 * @param {number} num
 * @return {boolean}
 */
// 二分查找
var isPerfectSquare = function(num) {
  if (num === 1) return true
  
  let left = 0, right = Math.floor(num / 2)
  while (left <= right) {
    let mid = Math.floor((left + right) / 2)
    if (mid * mid < num) {
      left = mid + 1
    } else if (mid * mid > num) {
      right = mid - 1
    } else {
      return true
    }
  }
  return false
};
console.log('367. 有效的完全平方数:', isPerfectSquare(1))


/** 27. 移除元素
给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。

示例 1：
输入：nums = [3,2,2,3], val = 3
输出：2, nums = [2,2]
解释：函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。
你不需要考虑数组中超出新长度后面的元素。例如，函数返回的新长度为 2 ，
而 nums = [2,2,3,3] 或 nums = [2,2,0,0]，也会被视作正确答案。

示例 2：
输入：nums = [0,1,2,2,3,0,4,2], val = 2
输出：5, nums = [0,1,3,0,4]
解释：函数应该返回新的长度 5, 并且 nums 中的前五个元素为 0, 1, 3, 0, 4。
注意这五个元素可为任意顺序。你不需要考虑数组中超出新长度后面的元素
 */
/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
// 双指针法（快慢指针法）
var removeElement = function(nums, val) {
	// k 慢指针 i快指针
	let k = 0
  for (let i = 0; i < nums.length; i++) {
		if (nums[i] !== val) {
			nums[k++] = nums[i]
		}
	}
	return k
}
console.log('27. 移除元素-双指针法（快慢指针法）:', removeElement([0,1,2,2,3,0,4,2], 2))

// 双循环法
var removeElement2 = function(nums, val) {
	let len = nums.length
  for (let i = 0; i < len; i++) {
		if (nums[i] === val) {
			for (let j = i + 1; j < len; j++) {
				nums[j - 1] = nums[j]
			}
			len--
			i--
		}
	}
	return len
}
console.log('27. 移除元素-双循环法:', removeElement2([0,1,2,2,3,0,4,2], 2))


/** 283. 移动零
给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
请注意 ，必须在不复制数组的情况下原地对数组进行操作。

示例 1:
输入: nums = [0,1,0,3,12]
输出: [1,3,12,0,0]

示例 2:
输入: nums = [0]
输出: [0]
 */
/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
//  双指针法（快慢指针法）
var moveZeroes = function(nums) {
  let len = nums.length
	// k 慢指针 i快指针
  let k = 0
  for (let i = 0; i < len; i++) {
    if (nums[i] !== 0) {
      nums[k++] = nums[i]
    }
  }
	// 慢指针剩余的元素填充0
  for (let i = k; i < len; i++) {
    nums[i] = 0
  }
}
let nums = [0,1,0,3,12]
moveZeroes(nums)
console.log('283. 移动零-双指针法（快慢指针法）:', nums)

// 双循环法
var moveZeroes2 = function(nums) {
  let len = nums.length
  for (let i = 0; i < len; i++) {
    if (nums[i] === 0) {
      for (let j = i + 1; j < len; j++) {
        nums[j - 1] = nums[j]
      }
			// 若移动之后的下一个元素是0，则i--重新走循环
			nums[i] === 0 && i--
      nums[len - 1] = 0
			len--
    }
  }
}
let nums2 = [0,1,0,3,12]
moveZeroes2(nums2)
console.log('283. 移动零-双循环法:', nums2)


/** 977. 有序数组的平方
给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。

示例 1：
输入：nums = [-4,-1,0,3,10]
输出：[0,1,9,16,100]
解释：平方后，数组变为 [16,1,0,9,100]
排序后，数组变为 [0,1,9,16,100]

示例 2：
输入：nums = [-7,-3,2,3,11]
输出：[4,9,9,49,121]

进阶：请你设计时间复杂度为 O(n) 的算法解决本问题
 */
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortedSquares = function (nums) {
  let arr = [], len = nums.length
  // 直接排序 时间复杂度：O(nlogn)
  // for (let i = 0; i < len; i++) {
  //   arr[i] = nums[i] * nums[i]
  // }
  // return arr.sort((a, b) => a - b)

  // 双指针法 时间复杂度：O(n)
  for (let i = 0, j = len - 1, pos = len - 1; i <= j;) {
    if (nums[i] * nums[i] > nums[j] * nums[j]) {
      arr[pos] = nums[i] * nums[i]
      i++
    } else {
      arr[pos] = nums[j] * nums[j]
      j--
    }
    pos--
  }
  return arr
}
console.log('977. 有序数组的平方:', sortedSquares([-4,-1,0,3,10]))



/**  209. 长度最小的子数组
给定一个含有 n 个正整数的数组和一个正整数 target 。
找出该数组中满足其和 ≥ target 的长度最小的 连续子数组 [numsl, numsl+1, ..., numsr-1, numsr] ，
并返回其长度。如果不存在符合条件的子数组，返回 0 。

示例 1：
输入：target = 7, nums = [2,3,1,2,4,3]
输出：2
解释：子数组 [4,3] 是该条件下的长度最小的子数组。

示例 2：
输入：target = 4, nums = [1,4,4]
输出：1

示例 3：
输入：target = 11, nums = [1,1,1,1,1,1,1,1]
输出：0


解答：
在本题中实现滑动窗口，主要确定如下三点：

窗口内是什么？
如何移动窗口的起始位置？
如何移动窗口的结束位置？

窗口就是 满足其和 ≥ s 的长度最小的 连续 子数组。
窗口的起始位置如何移动：如果当前窗口的值大于s了，窗口就要向前移动了（也就是该缩小了）。
窗口的结束位置如何移动：窗口的结束位置就是遍历数组的指针，也就是for循环里的索引。
 */
/**
 * @param {number} target
 * @param {number[]} nums
 * @return {number}
 */
var minSubArrayLen = function(target, nums) {
  // // 双循环 时间复杂度：O(n²)
  // let minLen = 0, sum = 0
  // for (let i = 0; i < nums.length; i++) {
  //   sum = 0
  //   for (let j = i; j < nums.length; j++) {
  //     sum += nums[j]
  //     if (sum >= target) {
  //       let subLen = j - i + 1
  //       minLen = minLen === 0 ? subLen : Math.min(minLen, subLen)
  //       break
  //     }
  //   }
  // }
  // return minLen

  // 滑动窗口 时间复杂度：O(n)
  let i = 0 // 滑动窗口起始位置
  let sum = 0 // 滑动窗口数值之和
  let minLen = Infinity // 滑动窗口的长度
  for (let j = 0; j < nums.length; j++) { // j 为滑动窗口结束位置
    sum += nums[j]
    while (sum >= target) {
      minLen = Math.min(minLen, j - i + 1)
      // 滑动窗口精髓：不断调整起始位置
      sum -= nums[i] 
      i++
    }
  }
  return minLen === Infinity ? 0 : minLen
}
console.log('209. 长度最小的子数组:', minSubArrayLen(7, [2,3,1,2,4,3]))



/** 904. 水果成篮
你正在探访一家农场，农场从左到右种植了一排果树。这些树用一个整数数组 fruits 表示，其中 fruits[i] 是第 i 棵树上的水果 种类 。

你想要尽可能多地收集水果。然而，农场的主人设定了一些严格的规矩，你必须按照要求采摘水果：

你只有 两个 篮子，并且每个篮子只能装 单一类型 的水果。每个篮子能够装的水果总量没有限制。
你可以选择任意一棵树开始采摘，你必须从 每棵 树（包括开始采摘的树）上 恰好摘一个水果 。采摘的水果应当符合篮子中的水果类型。每采摘一次，你将会向右移动到下一棵树，并继续采摘。
一旦你走到某棵树前，但水果不符合篮子的水果类型，那么就必须停止采摘。
给你一个整数数组 fruits ，返回你可以收集的水果的 最大 数目。

示例 1：
输入：fruits = [1,2,1]
输出：3
解释：可以采摘全部 3 棵树。

示例 2：
输入：fruits = [0,1,2,2]
输出：3
解释：可以采摘 [1,2,2] 这三棵树。
如果从第一棵树开始采摘，则只能采摘 [0,1] 这两棵树。

示例 3：
输入：fruits = [1,2,3,2,2]
输出：4
解释：可以采摘 [2,3,2,2] 这四棵树。
如果从第一棵树开始采摘，则只能采摘 [1,2] 这两棵树。

示例 4：
输入：fruits = [3,3,3,1,2,1,1,2,3,3,4]
输出：5
解释：可以采摘 [1,2,1,1,2] 这五棵树。
 */
/**
 * @param {number[]} fruits
 * @return {number}
 */
var totalFruit = function(fruits) {
  // 滑动窗口
  let sum = 0
  let left = 0 // 滑动窗口起始位置

  // 使用 Map 格式
  let map = new Map()
  for (let right = 0; right < fruits.length; right++) {
    map.set(fruits[right], (map.get(fruits[right]) || 0) + 1)
    while (map.size > 2) {
      map.set(fruits[left], map.get(fruits[left]) - 1)
      if (map.get(fruits[left]) === 0) {
        map.delete(fruits[left])
      }
      left++
    }
    sum = Math.max(sum, right - left + 1)
  }
  return sum


  // // 使用对象格式
  // let typeObj = [] // 水果类型及对应的个数
  // for (let right = 0; right < fruits.length; right++) {
  //   const el = fruits[right]
  //   typeObj[el] = (typeObj[el] || 0) + 1
  //   while (Object.keys(typeObj).length > 2) {
  //     typeObj[fruits[left]]--
  //     if (typeObj[fruits[left]] === 0) {
  //       Reflect.deleteProperty(typeObj, fruits[left])
  //     }
  //     left++
  //   }
  //   sum = Math.max(sum, right - left + 1)
  // }
  // return sum
};
console.log('904. 水果成篮:', totalFruit([1,2,3,2,2]))


/** 76. 最小覆盖子串
给你一个字符串 s 、一个字符串 t 。返回 s 中涵盖 t 所有字符的最小子串。如果 s 中不存在涵盖 t 所有字符的子串，
则返回空字符串 "" 。


注意：
对于 t 中重复字符，我们寻找的子字符串中该字符数量必须不少于 t 中该字符数量。
如果 s 中存在这样的子串，我们保证它是唯一的答案。
 

示例 1：
输入：s = "ADOBECODEBANC", t = "ABC"
输出："BANC"
解释：最小覆盖子串 "BANC" 包含来自字符串 t 的 'A'、'B' 和 'C'。

示例 2：
输入：s = "a", t = "a"
输出："a"
解释：整个字符串 s 是最小覆盖子串。

示例 3:
输入: s = "a", t = "aa"
输出: ""
解释: t 中两个字符 'a' 均应包含在 s 的子串中，
因此没有符合条件的子字符串，返回空字符串。
 */
/**
 * @param {string} s
 * @param {string} t
 * @return {string}
 */
// 滑动窗口
var minWindow = function(s, t) {
  // 维护两个hash表：一个维护当前t中每个字符的数量、一个维护滑动窗口中每个字符的数量
  // 还需要一个记录当前窗口中有效字符的数量count
  let ht = new Map()
  let hs = new Map()
  let str = ''

  // 初始化t的每个字符的数量表
  for (let i = 0; i < t.length; i++) {
    ht.set(t[i], (ht.get(t[i]) || 0) + 1)
  }

  // 循环字符串s，分三步走：
  // 1、将当前字符加入到窗口中，若加入后窗口长度小于t的长度，则字符是有效的 更新count的值
  // 2、若窗口开始字符的数量大于t所需要的此字符的数量，说明窗口中左侧有冗余字符，需右移窗口的左边界
  // 3、若count的值等于t的长度，说明窗口中已经覆盖了t中的所有字符，需要更新最后结果
  for (let i = 0, j = 0, count = 0; j < s.length; j++) {
    // i 起始位置, j 结束位置
    hs.set(s[j], (hs.get(s[j]) || 0) + 1)
    if (hs.get(s[j]) <= ht.get(s[j])) count++

    while (hs.get(s[i]) > (ht.get(s[i]) || 0)) {
      hs.set(s[i], hs.get(s[i]) - 1)
      i++
    }

    if (count === t.length) {
      if (!str || str.length > j - i + 1) {
        str = s.slice(i, j + 1)
      }
    }
  }

  return str
};
console.log('76. 最小覆盖子串:', minWindow('ADOBECODEBANC', 'ABC'))


/** 59. 螺旋矩阵 II
给你一个正整数 n ，生成一个包含 1 到 n2 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix 。

示例 1：
输入：n = 3
输出：[[1,2,3],[8,9,4],[7,6,5]]

示例 2：
输入：n = 1
输出：[[1]]

类似题目：
54.螺旋矩阵
 */
/**
 * @param {number} n
 * @return {number[][]}
 */
// 本题并不涉及到什么算法，就是模拟过程，但却十分考察对代码的掌控能力。
var generateMatrix = function(n) {
  // 定义二维数组
  const res = new Array(n).fill(0).map(() => new Array(n).fill(0))
  // 定义每循环一个圈的起始位置
  let startx = 0, starty = 0
  // 每个圈转几次
  let loop = Math.floor(n / 2)
  // 矩阵中间的位置
  let mid = Math.floor(n / 2)
  // 用来给矩阵中每一个空格复制
  let count = 1
  // 需要控制每一条遍历的长度，每次循环右边界收缩一位
  let offset = 1
  let i, j

  while (loop--) {
    i = startx
    j = starty

    // 下面开始的四个for循环就是模拟了一圈
    // 模拟填充上行从左到右(左闭右开)
    for (j = starty; j < n - offset; j++) {
      res[i][j] = count++
    }
    // 模拟填充右列从上到下(左闭右开)
    for (i = startx; i < n - offset; i++) {
      res[i][j] = count++
    }
    // 模拟填充下行从右到左(左闭右开)
    for (; j > starty; j--) {
      res[i][j] = count++
    }
    // 模拟填充左列从下到上(左闭右开)
    for (; i > startx; i--) {
      res[i][j] = count++
    }

    // 第二圈开始时，起始位置要各自加1，例如：第一圈起始位置是(0, 0)，第二圈起始位置是(1, 1)
    startx++
    starty++

    // offset 控制每一圈里每一条边遍历的长度
    offset++
  }

  // 如果n为奇数，需单独给矩阵最中间的位置赋值
  if (n % 2) {
    res[mid][mid] = count
  }

  return res
}
console.log('59. 螺旋矩阵 II:', generateMatrix(5))

/**
 * @param {number} n
 * @return {number[][]}
 */
var generateMatrix2 = function(n) {
  // 初始化二维数组
  let res = new Array(n).fill(0).map(() => new Array(n).fill(0))
  // 计数赋值
  let count = 1
  // // 维护上右下左四个边界，当上边界大于下边界 或 左边界大于右边界时，说明螺旋结束
  let up = 0, right = n - 1, down = n - 1, left = 0

  while (true) {
    for (let i = left; i <= right; i++) {
      res[up][i] = count++
    }
    if (++up > down) break

    for (let i = up; i <= down; i++) {
      res[i][right] = count++
    }
    if (--right < left) break

    for (let i = right; i >= left; i--) {
      res[down][i] = count++
    }
    if (--down < up) break

    for (let i = down; i >= up; i--) {
      res[i][left] = count++
    }
    if (++left > right) break

  }

  return res
}
console.log('59. 螺旋矩阵 II 2:', generateMatrix2(3))











































