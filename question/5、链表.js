/**
 * 总结：
链表的种类主要为：单链表，双链表，循环链表
链表的存储方式：链表的节点在内存中是分散存储的，通过指针连在一起。
链表是如何进行增删改查的。
数组和链表在不同场景下的性能分析。

 */

/**
 * 相关题目：
 * 707. 设计链表
 * 
 * 203.移除链表元素
 * 206.反转链表 - 双指针
 * 24. 两两交换链表中的节点
 * 19.删除链表的倒数第N个节点 - 快慢指针法
 * 面试题 02.07. 链表相交 （同：160.链表相交）
 * 142.环形链表II - Map 或 快慢指针法
 * 
 */


/**
 * JS 实现链表定义
 */
// 链表的单个节点的定义
class Node {
  constructor (val, next) {
    this.val = val ?? 0 // 数据
    this.next = next ?? null // 指针：下一节点的位置
  }
}

// 链表的定义
class LinkedList {
  constructor () {
    this.head = null
    this.length = 0
  }
  // 向链表尾部追加元素
  append (element) {
    let node = new Node(element)

    if (this.head === null) {
      this.head = node
    } else {
      let tail = this.head
      while (tail.next) {
        tail = tail.next
      }
      tail.next = node
    }

    this.length += 1
  }
}

// 创建一个链表
const linkedList = new LinkedList()
linkedList.append(1)
linkedList.append(2)
linkedList.append(3)
linkedList.append(4)
console.log('linkedList:', linkedList)


/** 707. 设计链表
 * 设计链表的实现。您可以选择使用单链表或双链表。单链表中的节点应该具有两个属性：val 和 next。
 * val 是当前节点的值，next 是指向下一个节点的指针/引用。如果要使用双向链表，则还需要一个属性 prev 以指示链表中的上一个节点。
 * 假设链表中的所有节点都是 0-index 的。
 * 
在链表类中实现这些功能：

get(index)：获取链表中第 index 个节点的值。如果索引无效，则返回-1。
addAtHead(val)：在链表的第一个元素之前添加一个值为 val 的节点。插入后，新节点将成为链表的第一个节点。
addAtTail(val)：将值为 val 的节点追加到链表的最后一个元素。
addAtIndex(index,val)：在链表中的第 index 个节点之前添加值为 val  的节点。如果 index 等于链表的长度，则该节点将附加到链表的末尾。如果 index 大于链表长度，则不会插入节点。如果index小于0，则在头部插入节点。
deleteAtIndex(index)：如果索引 index 有效，则删除链表中的第 index 个节点。

示例：

MyLinkedList linkedList = new MyLinkedList();
linkedList.addAtHead(1);
linkedList.addAtTail(3);
linkedList.addAtIndex(1,2);   //链表变为1-> 2-> 3
linkedList.get(1);            //返回2
linkedList.deleteAtIndex(1);  //现在链表是1-> 3
linkedList.get(1);            //返回3
 */

// 链表的单个节点的定义
function ListNode (val, next) {
  this.val = val === undefined ? 0 : val
  this.next = next === undefined ? null : next
}

// 初始化链表：长度 头节点
var MyLinkedList = function() {
  this.size = 0
  // 这里定义的头结点 是一个虚拟头结点，而不是真正的链表头结点
  this.head = new ListNode(0)
};

/** 
 * @param {number} index
 * @return {number}
 */
MyLinkedList.prototype.get = function(index) {
  if (index < 0 || index >= this.size) return -1
  let cur = this.head
  for (let i = 0; i <= index; i++) {
    cur = cur.next
  }
  return cur.val 
};

/** 
 * @param {number} val
 * @return {void}
 */
MyLinkedList.prototype.addAtHead = function(val) {
  this.addAtIndex(0, val)
};

/** 
 * @param {number} val
 * @return {void}
 */
MyLinkedList.prototype.addAtTail = function(val) {
  this.addAtIndex(this.size, val)
};

/** 
 * @param {number} index 
 * @param {number} val
 * @return {void}
 */
MyLinkedList.prototype.addAtIndex = function(index, val) {
  if (index > this.size) return
  index = Math.max(0, index)
  this.size++
  let pre = this.head
  for (let i = 0; i < index; i++) {
    pre = pre.next
  }
  let node = new ListNode(val)
  node.next = pre.next
  pre.next = node
};

/** 
 * @param {number} index
 * @return {void}
 */
MyLinkedList.prototype.deleteAtIndex = function(index) {
  if (index < 0 || index >= this.size) return
  this.size--
  let pre = this.head
  for (let i = 0; i < index; i++) {
    pre = pre.next
  }
  pre.next = pre.next.next
};
const list = new MyLinkedList()
list.addAtHead(1);
list.addAtTail(3);
list.addAtIndex(1,2);   //链表变为1-> 2-> 3
list.get(1);            //返回2
list.deleteAtIndex(1);  //现在链表是1-> 3
list.get(1);            //返回3
console.log('list:', list)













