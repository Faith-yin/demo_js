/**
 * 哈希表能解决什么问题呢，一般哈希表都是用来快速判断一个元素是否出现集合里。
 * 常见的三种哈希结构：数组、set(集合)、map(映射)
 */

/**
 * 相关题目：
 * 242. 有效的字母异位词 - 有限长度数组 
 * 383. 赎金信 - 有限长度数组 
 * 49. 字母异位词分组 - Map
 * 438. 找到字符串中所有字母异位词 - 滑动窗口、 - 有限长度数组 
 * 
 * 349. 两个数组的交集 - Set
 * 350.两个数组的交集 II - Map
 * 202. 快乐数 - Set 或 快慢指针法：使用环形链表的思想 说明出现闭环 退出循环
 * 1. 两数之和 - Map
 * 454. 四数相加 II - Map
 * 15. 三数之和 - 双指针：要求返回的是数值的话，可以使用双指针法
 * 18. 四数之和 - 与 15 类似，在其基础上再套一层 for 循环
 * 
 */



/** 242. 有效的字母异位词
给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。

示例 1:
输入: s = "anagram", t = "nagaram"
输出: true

示例 2:
输入: s = "rat", t = "car"
输出: false
 */
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function(s, t) {
  // 排序比较
  // return s.length === t.length && [...s].sort().join() === [...t].sort().join()

  // 哈希表
  if (s.length !== t.length) return false
  // 维护一个长度为 26 的频次数组
  let arr = Array(26).fill(0)
  let base = 'a'.charCodeAt()
  // 先遍历记录字符串 s 中字符出现的频次，然后遍历字符串 t，减去 arr 中对应的频次，如果出现 0，则说明 t 包含一个不在 s 中的额外字符，返回 false
  for (const i of s) {
    arr[i.charCodeAt() - base]++
  }
  for (const i of t) {
    if (!arr[i.charCodeAt() - base]) return false
    arr[i.charCodeAt() - base]--
  }
  return true
}


/** 383. 赎金信
 * 给你两个字符串：ransomNote 和 magazine ，判断 ransomNote 能不能由 magazine 里面的字符构成。
如果可以，返回 true ；否则返回 false 。
magazine 中的每个字符只能在 ransomNote 中使用一次。

示例 1：
输入：ransomNote = "a", magazine = "b"
输出：false

示例 2：
输入：ransomNote = "aa", magazine = "ab"
输出：false

示例 3：
输入：ransomNote = "aa", magazine = "aab"
输出：true
 */
/**
 * @param {string} ransomNote
 * @param {string} magazine
 * @return {boolean}
 */
var canConstruct = function(ransomNote, magazine) {
  if (ransomNote.length > magazine.length) return false

  // 利用长度为 26 的频次数组，逐一记录并逐一判断
  const arr = Array(26).fill(0)
  const base = 'a'.charCodeAt()
  for (const i of magazine) {
    arr[i.charCodeAt() - base]++
  }

  for (const i of ransomNote) {
    if (!arr[i.charCodeAt() - base]) return false
    arr[i.charCodeAt() - base]--
  }

  return true
}


/** 49. 字母异位词分组
给你一个字符串数组，请你将 字母异位词 组合在一起。可以按任意顺序返回结果列表。

字母异位词 是由重新排列源单词的字母得到的一个新单词，所有源单词中的字母通常恰好只用一次。

示例 1:
输入: strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
输出: [["bat"],["nat","tan"],["ate","eat","tea"]]

示例 2:
输入: strs = [""]
输出: [[""]]

示例 3:
输入: strs = ["a"]
输出: [["a"]]
 */
/**
 * @param {string[]} strs
 * @return {string[][]}
 */
//  由于互为字母异位词的两个字符串包含的字母相同，因此对两个字符串分别进行排序之后得到的字符串一定是相同的，故可以将排序之后的字符串作为哈希表的键
var groupAnagrams = function(strs) {
  let map = new Map()

  for (let i = 0; i < strs.length; i++) {
    const sortStr = [...strs[i]].sort().join('')
    if (map.get(sortStr)) {
      map.get(sortStr).push(strs[i])
    } else {
      map.set(sortStr, [strs[i]])
    }
  }

  return [...map.values()]
}


/** 438. 找到字符串中所有字母异位词
给定两个字符串 s 和 p，找到 s 中所有 p 的 异位词 的子串，返回这些子串的起始索引。不考虑答案输出的顺序。

异位词 指由相同字母重排列形成的字符串（包括相同的字符串）。

示例 1:
输入: s = "cbaebabacd", p = "abc"
输出: [0,6]
解释:
起始索引等于 0 的子串是 "cba", 它是 "abc" 的异位词。
起始索引等于 6 的子串是 "bac", 它是 "abc" 的异位词。

示例 2:
输入: s = "abab", p = "ab"
输出: [0,1,2]
解释:
起始索引等于 0 的子串是 "ab", 它是 "ab" 的异位词。
起始索引等于 1 的子串是 "ba", 它是 "ab" 的异位词。
起始索引等于 2 的子串是 "ab", 它是 "ab" 的异位词。
 */
/**
 * @param {string} s
 * @param {string} p
 * @return {number[]}
 */
// 滑动窗口
var findAnagrams = function(s, p) {
  const sLen = s.length, pLen = p.length

  if (sLen < pLen) return []
  
  const arr = []
  // 维护一个s窗口，与目标p中长度相同，每次判断窗口中元素是否都与p窗口中相同。
  const sCount = new Array(26).fill(0)
  const pCount = new Array(26).fill(0)
  const base = 'a'.charCodeAt()
  
  // 初始化
  for (let i = 0; i < pLen; i++) {
    sCount[s[i].charCodeAt() - base]++
    pCount[p[i].charCodeAt() - base]++
  }

  if (sCount.toString() === pCount.toString()) {
    arr.push(0)
  }

  for (let i = 0; i < sLen - pLen; i++) {
    sCount[s[i].charCodeAt() - base]--
    sCount[s[i + pLen].charCodeAt() - base]++

    if (sCount.toString() === pCount.toString()) {
      arr.push(i + 1)
    }
  }

  return arr
};





