/**
 * 队列(queue)是先进先出，栈(stack)是先进后出。
 * 
 * 匹配问题都是栈的强项。
 * 
 * 
 * Tip: 递归的实现是[栈]：
 * 每一次递归调用都会把函数的局部变量、参数值和返回地址等压入调用栈中，然后递归返回的时候，
 * 从栈顶弹出上一次递归的各项参数，所以这就是递归为什么可以返回上一层位置的原因。
 */


/**
 * 相关题目：
 * 232. 用栈实现队列
 * 225. 用队列实现栈
 * 
 * 20. 有效的括号 - 栈：先进后出
 * 1047. 删除字符串中的所有相邻重复项 - 栈：匹配问题都是栈的强项 or 原地解法（双指针模拟栈）
 * 150. 逆波兰表达式求值 - 栈：先进后出
 * 239. 滑动窗口最大值 - 自定义单调队列
 * 347. 前 K 个高频元素 - 
 * 71. 简化路径
 * 
 */

/** 20. 有效的括号
给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。

有效字符串需满足：
左括号必须用相同类型的右括号闭合。
左括号必须以正确的顺序闭合。
每个右括号都有一个对应的相同类型的左括号。

示例 1：
输入：s = "()"
输出：true

示例 2：
输入：s = "()[]{}"
输出：true

示例 3：
输入：s = "(]"
输出：false
 */

/**
 * @param {string} s
 * @return {boolean}
 */
// 使用栈：先进后出
var isValid = function(s) {
  const stack = []
  const map = {
    '(': ')',
    '[': ']',
    '{': '}',
  }

  for (let i = 0; i < s.length; i++) {
    if (map[s[i]]) {
      stack.push(s[i])
    } else if (s[i] === map[stack.pop()]) {
      continue
    } else {
      return false
    }
  }

  return !stack.length
};


/** 1047. 删除字符串中的所有相邻重复项
给出由小写字母组成的字符串 S，重复项删除操作会选择两个相邻且相同的字母，并删除它们。

在 S 上反复执行重复项删除操作，直到无法继续删除。
在完成所有重复项删除操作后返回最终的字符串。答案保证唯一。

示例：
输入："abbaca"
输出："ca"

解释：
例如，在 "abbaca" 中，我们可以删除 "bb" 由于两字母相邻且相同，这是此时唯一可以执行删除操作的重复项。
之后我们得到字符串 "aaca"，其中又只有 "aa" 可以执行重复项删除操作，所以最后的字符串为 "ca"。
 */

/**
 * @param {string} s
 * @return {string}
 */
// （1）栈：匹配问题都是栈的强项
var removeDuplicates = function(s) {
  // let stack = []
  // for (let i = 0; i < s.length; i++) {
  //   if (stack.length && s[i] === stack[stack.length - 1]) {
  //     stack.pop()
  //     continue
  //   }
  //   stack.push(s[i])
  // }
  // return stack.join('')


  // （2）原地解法（双指针模拟栈）
  s = [...s];
  let top = -1; // 指向栈顶元素的下标
  for(let i = 0; i < s.length; i++) {
      if(top === -1 || s[top] !== s[i]) { // top === -1 即空栈
          s[++top] = s[i]; // 入栈
      } else {
          top--; // 推出栈
      }
  }
  s.length = top + 1; // 栈顶元素下标 + 1 为栈的长度
  return s.join('');
};


/** 150. 逆波兰表达式求值
给你一个字符串数组 tokens ，表示一个根据 逆波兰表示法 表示的算术表达式。

请你计算该表达式。返回一个表示表达式值的整数。

注意：
有效的算符为 '+'、'-'、'*' 和 '/' 。
每个操作数（运算对象）都可以是一个整数或者另一个表达式。
两个整数之间的除法总是 向零截断 。
表达式中不含除零运算。
输入是一个根据逆波兰表示法表示的算术表达式。
答案及所有中间计算结果可以用 32 位 整数表示。
 
示例 1：
输入：tokens = ["2","1","+","3","*"]
输出：9
解释：该算式转化为常见的中缀算术表达式为：((2 + 1) * 3) = 9

示例 2：
输入：tokens = ["4","13","5","/","+"]
输出：6
解释：该算式转化为常见的中缀算术表达式为：(4 + (13 / 5)) = 6

示例 3：
输入：tokens = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
输出：22
解释：该算式转化为常见的中缀算术表达式为：
  ((10 * (6 / ((9 + 3) * -11))) + 17) + 5
= ((10 * (6 / (12 * -11))) + 17) + 5
= ((10 * (6 / -132)) + 17) + 5
= ((10 * 0) + 17) + 5
= (0 + 17) + 5
= 17 + 5
= 22
 */

/**
 * @param {string[]} tokens
 * @return {number}
 */
// 栈
var evalRPN = function(tokens) {
  const operatorMap = new Map([
    ['+', (a, b) => a + b],
    ['-', (a, b) => a - b],
    ['/', (a, b) => Math.trunc(a / b)],
    ['*', (a, b) => a * b],
  ])
  const stack = []

  for (let i = 0; i < tokens.length; i++) {
    const s = tokens[i]
    if (operatorMap.has(s)) {
      const b = stack.pop()
      const a = stack.pop()
      stack.push(operatorMap.get(s)(a, b))
    } else {
      stack.push(Number(s))
    }
  }

  return stack.pop()
};


/** 239. 滑动窗口最大值
给你一个整数数组 nums，有一个大小为 k 的滑动窗口从数组的最左侧移动到数组的最右侧。
你只可以看到在滑动窗口内的 k 个数字。滑动窗口每次只向右移动一位。返回 滑动窗口中的最大值 。

示例 1：
输入：nums = [1,3,-1,-3,5,3,6,7], k = 3
输出：[3,3,5,5,6,7]
解释：
滑动窗口的位置                最大值
---------------               -----
[1  3  -1] -3  5  3  6  7       3
 1 [3  -1  -3] 5  3  6  7       3
 1  3 [-1  -3  5] 3  6  7       5
 1  3  -1 [-3  5  3] 6  7       5
 1  3  -1  -3 [5  3  6] 7       6
 1  3  -1  -3  5 [3  6  7]      7

示例 2：
输入：nums = [1], k = 1
输出：[1]
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
//  自定义单调队列
var maxSlidingWindow = function(nums, k) {
  class MonoQueue {
    constructor () {
      this.queue = []
    }
    enqueue (value) {
      let back = this.queue[this.queue.length - 1]
      while (back !== undefined && back < value) {
        this.queue.pop()
        back = this.queue[this.queue.length - 1]
      }
      this.queue.push(value)
    }
    dequeue (value) {
      let front = this.front()
      if (front === value) {
        this.queue.shift()
      }
    }
    front () {
      return this.queue[0]
    }
  }

  const monoQueue = new MonoQueue()
  let i = 0, j = 0
  const resArr = []
  while (j < k) {
    monoQueue.enqueue(nums[j++])
  }
  resArr.push(monoQueue.front())
  while (j < nums.length) {
    monoQueue.enqueue(nums[j])
    monoQueue.dequeue(nums[i])
    resArr.push(monoQueue.front())
    i++
    j++
  }

  return resArr
};


/** 347. 前 K 个高频元素
给你一个整数数组 nums 和一个整数 k ，请你返回其中出现频率前 k 高的元素。你可以按 任意顺序 返回答案。

示例 1:
输入: nums = [1,1,1,2,2,3], k = 2
输出: [1,2]

示例 2:
输入: nums = [1], k = 1
输出: [1]
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
//  Map
var topKFrequent = function(nums, k) {
  const map = new Map()
  for (let i = 0; i < nums.length; i++) {
    map.set(nums[i], (map.get(nums[i]) || 0) + 1)
  }
  return [...map].sort((a, b) => b[1] - a[1]).slice(0, k).map(el => el[0])
}


/**
 * 71. 简化路径
给你一个字符串 path ，表示指向某一文件或目录的 Unix 风格 绝对路径 （以 '/' 开头），请你将其转化为更加简洁的规范路径。

在 Unix 风格的文件系统中，一个点（.）表示当前目录本身；此外，两个点 （..） 表示将目录切换到上一级（指向父目录）；
两者都可以是复杂相对路径的组成部分。任意多个连续的斜杠（即，'//'）都被视为单个斜杠 '/' 。 
对于此问题，任何其他格式的点（例如，'...'）均被视为文件/目录名称。

请注意，返回的 规范路径 必须遵循下述格式：

始终以斜杠 '/' 开头。
两个目录名之间必须只有一个斜杠 '/' 。
最后一个目录名（如果存在）不能 以 '/' 结尾。
此外，路径仅包含从根目录到目标文件或目录的路径上的目录（即，不含 '.' 或 '..'）。
返回简化后得到的 规范路径 。

示例 1：
输入：path = "/home/"
输出："/home"
解释：注意，最后一个目录名后面没有斜杠。 

示例 2：
输入：path = "/../"
输出："/"
解释：从根目录向上一级是不可行的，因为根目录是你可以到达的最高级。

示例 3：
输入：path = "/home//foo/"
输出："/home/foo"
解释：在规范路径中，多个连续斜杠需要用一个斜杠替换。

示例 4：
输入：path = "/a/./b/../../c/"
输出："/c"
 */

/**
 * @param {string} path
 * @return {string}
 */
//  栈: 把当前目录压入栈中,遇到..弹出栈顶,最后返回栈中元素.
var simplifyPath = function(path) {
  const pathArr = path.split('/')
  const stack = []
  for (const item of pathArr) {
    if (item === '..') {
      stack.length && stack.pop()
    } else if (item && item !== '.') {
      stack.push(item)
    }
  }
  return '/' + stack.join('/')
};




