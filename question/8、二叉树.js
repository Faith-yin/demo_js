
/**
 * 二叉树的种类：满二叉树和完全二叉树
 * 
遍历方式：

- 广度优先遍历（广度优先搜索在二叉树上的应用：即层序遍历。）
  即逐层地，从左到右访问所有节点。

-  深度优先遍历
  前序遍历 - 遍历顺序：中左右： 1. 递归法； 2. 迭代法（压栈顺序：右左中）
  中序遍历 - 遍历顺序：左中右： 1. 递归法； 2. 迭代法（压栈顺序：左中右）
  后序遍历 - 遍历顺序：左右中： 1. 递归法； 2. 迭代法（压栈顺序：左右中）

> 遍历思想：迭代法 - 栈，层序遍历 - 队列。


  递归法解题思路：
  求深度：由上至下：前序遍历(中左右)
  求高度：由下至上：后序遍历(左右中)

 */


/**
 * 相关题目：
 * 
 * 统一迭代法：
 * 144. 二叉树的前序遍历
 * 94. 二叉树的中序遍历
 * 145. 二叉树的后序遍历
 * 
 * 层序遍历：
 * 102. 二叉树的层序遍历
 * 107. 二叉树的层序遍历 II
 * 199. 二叉树的右视图
 * 637. 二叉树的层平均值
 * 429. N 叉树的层序遍历
 * 515.在每个树行中找最大值
 * 116.填充每个节点的下一个右侧节点指针
 * 117.填充每个节点的下一个右侧节点指针II
 * 104.二叉树的最大深度
 * 111.二叉树的最小深度
 * 
 * 226. 翻转二叉树 - 迭代法（广度优先遍历、深度优先遍历）、递归法
 * 101. 对称二叉树 - 迭代法（广度优先遍历队列、栈）、递归法
 * 100. 相同的树 - 迭代法（广度优先遍历）、递归法
 * 572. 另一棵树的子树 - 递归法、KMP
 * 222. 完全二叉树的节点个数 - 广度优先遍历、递归法、利用完全二叉树的性质
 * 110. 平衡二叉树 - 递归法（求高度-由下至上：后序遍历）
 * 257. 二叉树的所有路径 - 递归法、迭代法
 * 
 * 
 * 
 */

/**
 * 二叉树的定义：链表定义
 */
function TreeNode(val, left, right) {
  this.val = (val === undefined ? 0 : val)
  this.left = (left === undefined ? null : left)
  this.right = (right === undefined ? null : right)
}

/**
 * 144. 二叉树的前序遍历
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
//  前序遍历：中左右
var preorderTraversal = function(root) {
  // 递归
  // const res = []
  // const dfs = root => {
  //   if (root === null) return
  //   // 前序遍历从父节点开始
  //   res.push(root.val)
  //   // 递归左子树
  //   dfs(root.left)
  //   // 递归右子树
  //   dfs(root.right)
  // }
  // dfs(root)
  // return res


  // 迭代: 压栈顺序：右左中
  const res = [], stack = []
  if (root) stack.push(root)
  while (stack.length) {
    const node = stack.pop()
    if (!node) {
      res.push(stack.pop().val)
      continue
    }
    if (node.right) stack.push(node.right) // 右
    if (node.left) stack.push(node.left) // 左
    stack.push(node) // 中
    stack.push(null)
  }
  return res
}


/**
 * 94. 二叉树的中序遍历
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
//  中序遍历：左中右
var inorderTraversal = function (root) {
  // 递归
  // const res = []
  // const dfs = root => {
  //   if (root === null) return
  //   dfs(root.left)
  //   res.push(root.val)
  //   dfs(root.right)
  // }
  // dfs(root)
  // return res


  // 迭代：压栈顺序：右中左
  const stack = [], res = []
  if (root) stack.push(root)
  while (stack.length) {
    const node = stack.pop()
    if (!node) {
      res.push(stack.pop().val)
      continue
    }
    if (node.right) stack.push(node.right) // 右
    stack.push(node) // 中
    stack.push(null)
    if (node.left) stack.push(node.left) // 左
  }
  return res
}


/**
 * 145. 二叉树的后序遍历
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
//  后序遍历：左右中
var postorderTraversal = function(root) {
  // 递归法
  // const res = []
  // const dfs = root => {
  //   if (root === null) return
  //   dfs(root.left)
  //   dfs(root.right)
  //   res.push(root.val)
  // }
  // dfs(root)
  // return res


  // 迭代法：压栈顺序：中右左
  const stack = [], res = []
  if (root) stack.push(root)
  while (stack.length) {
    const node = stack.pop()
    if (!node) {
      res.push(stack.pop().val)
      continue
    }
    stack.push(node) // 中
    stack.push(null)
    if (node.right) stack.push(node.right) // 右
    if (node.left) stack.push(node.left) // 左
  }
  return res
}


/**
 * 102. 二叉树的层序遍历
 * 
 * 给你二叉树的根节点 root ，返回其节点值的 层序遍历 。 （即逐层地，从左到右访问所有节点）。
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
//  广度优先遍历：队列
var levelOrder = function(root) {
  const queue = [], res = []
  if (root === null) return res
  queue.push(root)

  while (queue.length) {
    // 当期那层级所有节点的数量
    const len = queue.length
    // 当前层级所有节点的值
    const curLevelValArr = []
    for (let i = 0; i < len; i++) {
      const node = queue.shift()
      curLevelValArr.push(node.val)
      // 存放下一级的节点
      node.left && queue.push(node.left)
      node.right && queue.push(node.right)
    }
    res.push(curLevelValArr)
  }
  return res
}


/**
 * 226. 翻转二叉树
给你一棵二叉树的根节点 root ，翻转这棵二叉树，并返回其根节点。

输入：root = [4,2,7,1,3,6,9]
输出：[4,7,2,9,6,3,1]
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var invertTree = function(root) {
  if (root === null) return null

  // 迭代法：广度优先遍历
  const queue = []
  queue.push(root)
  while (queue.length) {
    const len = queue.length
    for (let i = 0; i < len; i++) {
      let node = queue.shift();
      [node.left, node.right] = [node.right, node.left]
      node.left && queue.push(node.left)
      node.right && queue.push(node.right)
    }
  }
  return root


  // 迭代法：深度优先遍历
  // const queue = []
  // queue.push(root)
  // while(queue.length) {
  //   const node = queue.shift();
  //   [node.left, node.right] = [node.right, node.left];
  //   node.left && queue.push(node.left)
  //   node.right && queue.push(node.right)
  // }
  // return root


  // 递归法
  // ;[root.left, root.right] = [root.right, root.left];
  // invertTree(root.left)
  // invertTree(root.right)
  // return root
}


/**
 * 101. 对称二叉树
给你一个二叉树的根节点 root ， 检查它是否轴对称。

输入：root = [1,2,2,3,4,4,3]
输出：true
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isSymmetric = function(root) {
  if (root === null) return false

  // 迭代法：广度优先遍历 - 队列
  const queue = [root.left, root.right]
  let node1, node2
  while (queue.length) {
    node1 = queue.shift()
    node2 = queue.shift()
    if (node1 === null && node2 === null) continue
    if (node1 === null || node2 === null) return false
    if (node1.val !== node2.val) return false
    queue.push(node1.left)
    queue.push(node2.right)
    queue.push(node1.right)
    queue.push(node2.left)
  }
  return true


  // // 递归
  // const recur = (node1, node2) => {
  //   if (node1 === null && node2 === null) return true
  //   if (node1 === null || node2 === null) return false
  //   if (node1.val !== node2.val) return false
  //   const isSame1 = recur(node1.left, node2.right)
  //   const isSame2 = recur(node1.right, node2.left)
  //   return isSame1 && isSame2
  // }
  // return recur(root.left, root.right)


  // // 迭代法：广度优先遍历 - 栈
  // const stack = [root.left, root.right]
  // let node1, node2
  // while (stack.length) {
  //   node1 = stack.pop()
  //   node2 = stack.pop()
  //   if (node1 === null && node2 === null) continue
  //   if (node1 === null || node2 === null) return false
  //   if (node1.val !== node2.val) return false
  //   stack.push(node1.left)
  //   stack.push(node2.right)
  //   stack.push(node1.right)
  //   stack.push(node2.left)
  // }
  // return true
}


/** 100. 相同的树
给你两棵二叉树的根节点 p 和 q ，编写一个函数来检验这两棵树是否相同。

如果两个树在结构上相同，并且节点具有相同的值，则认为它们是相同的。
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {boolean}
 */
var isSameTree = function (p, q) {
  // // （1）队列：广度优先遍历
  // if (p === null && q === null) return true
  // if (p === null || q === null) return false

  // const queue1 = [p], queue2 = [q]
  // while (queue1.length) {
  //   const len = queue1.length
  //   if (len !== queue2.length) return false

  //   for (let i = 0; i < len; i++) {
  //     const pNode = queue1.shift()
  //     const qNode = queue2.shift()
  //     if (pNode.val !== qNode.val) return false
  //     if (pNode.left && qNode.left) {
  //       queue1.push(pNode.left)
  //       queue2.push(qNode.left)
  //     }
  //     if (pNode.right && qNode.right) {
  //       queue1.push(pNode.right)
  //       queue2.push(qNode.right)
  //     }
  //     if (
  //       pNode.left  && !qNode.left ||
  //       !pNode.left && qNode.left ||
  //       pNode.right  && !qNode.right ||
  //       !pNode.right && qNode.right
  //     ) return false
  //   }
  // }

  // return true


  // (2)递归：深度优先遍历
  if (p === null && q === null) {
    return true
  } else if (p === null || q === null) {
    return false
  } if (p.val !== q.val) {
    return false
  } else {
    return isSameTree(p.left, q.left) && isSameTree(p.right, q.right)
  }
};


/** 572. 另一棵树的子树
给你两棵二叉树 root 和 subRoot 。检验 root 中是否包含和 subRoot 具有相同结构和节点值的子树。如果存在，返回 true ；否则，返回 false 。

二叉树 tree 的一棵子树包括 tree 的某个节点和这个节点的所有后代节点。tree 也可以看做它自身的一棵子树。
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} subRoot
 * @return {boolean}
 */
var isSubtree = function (root, subRoot) {
  // （1）递归：深度优先遍历 
  const check = (s, t) => {
    if (!s && !t) return true
    if (!s || !t || s.val !== t.val) return false
    return check(s.left, t.left) && check(s.right, t.right)
  }

  const dfs = (s, t) => {
    if (!s) return false
    return check(s, t) || dfs(s.left, t) || dfs(s.right, t)
  }

  return dfs(root, subRoot)


  // // (2)KMP: 深度优先搜索序列上做串匹配
  // // 求出next数组
  // const getNext = (patternStr) => {
  //   const next = new Array(patternStr.length)
  //   let j = -1
  //   next[0] = j
  //   for (let i = 1; i < patternStr.length; i++) {
  //     while (j >= 0 && patternStr[i] !== patternStr[j + 1]) {
  //       j = next[j]
  //     }
  //     if (patternStr[i] === patternStr[j + 1]) {
  //       j++
  //     }
  //     next[i] = j
  //   }
  //   return next
  // }

  // // 递归获得二叉树的前序数组
  // const getPreOrder = (node, arr) => {
  //   if (!node) {
  //     arr.push(null)
  //     return
  //   }
  //   arr.push(node.val)
  //   getPreOrder(node.left, arr)
  //   getPreOrder(node.right, arr)
  // }

  // // 获得先序遍历
  // const rootPrevious = [], subRootPrevious = []
  // getPreOrder(root, rootPrevious)
  // getPreOrder(subRoot, subRootPrevious)
  // // 获得next数组
  // const next = getNext(subRootPrevious)
  // let i = 0; j = next[0]
  // // KMP算法
  // for (; i < rootPrevious.length; i++) {
  //   while (j >= 0 && rootPrevious[i] !== subRootPrevious[j + 1]) {
  //     j = next[j]
  //   }
  //   if (rootPrevious[i] === subRootPrevious[j + 1]) {
  //     j++
  //   }
  //   // 找到相应子串
  //   if (j === subRootPrevious.length - 1) return true
  // }
  // // 没有找到相应子串
  // return false

};


/** 222. 完全二叉树的节点个数
给你一棵 完全二叉树 的根节点 root ，求出该树的节点个数。

完全二叉树 的定义如下：在完全二叉树中，除了最底层节点可能没填满外，其余每层节点数都达到最大值，
并且最下面一层的节点都集中在该层最左边的若干位置。若最底层为第 h 层，则该层包含 1~ 2h 个节点。
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var countNodes = function(root) {
  if (!root) return 0
  
  // // (1) 广度优先遍历
  // let res = 0
  // const queue = [root]
  // while (queue.length) {
  //   const len = queue.length
  //   for (let i = 0; i < len; i++) {
  //     const node = queue.shift()
  //     res++
  //     node.left && queue.push(node.left)
  //     node.right && queue.push(node.right)
  //   }
  // }
  // return res


  // //（2）递归法
  // // 1. 确定递归函数参数
  // const getNodeSum = node => {
  //   // 2. 确定终止条件
  //   if (!node) return 0
  //   // 3. 确定单层递归逻辑
  //   const leftNum = getNodeSum(node.left)
  //   const rightNum = getNodeSum(node.right)
  //   return leftNum + rightNum + 1
  // }
  // return getNodeSum(root)


  // (3) 利用完全二叉树的性质
  let left = root.left
  let right = root.right
  let leftDepth = 0, rightDepth = 0
  while (left) {
    left = left.left
    leftDepth++
  }
  while (right) {
    right = right.right
    rightDepth++
  }
  if (leftDepth === rightDepth) {
    return Math.pow(2, leftDepth + 1) - 1
  }
  return countNodes(root.left) + countNodes(root.right) + 1
};


/** 110. 平衡二叉树
给定一个二叉树，判断它是否是高度平衡的二叉树。

本题中，一棵高度平衡二叉树定义为：
一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过 1 。
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isBalanced = function(root) {
  // 递归法  递归三部曲
  // 1. 确定递归参数
  const getDepth = node => {
    // 2. 确定终止条件
    if (!node) return 0
    // 3. 确定单层递归逻辑
    let leftDepth = getDepth(node.left) //左子树高度
    // 当判定左子树不为平衡二叉树时，直接返回-1
    if (leftDepth === -1) return -1
    let rightDepth = getDepth(node.right) //右子树高度
    // 当判定右子树不为平衡二叉树时，直接返回-1
    if (rightDepth === -1) return -1
    if (Math.abs(leftDepth - rightDepth) > 1) {
      return -1
    } else {
      return 1 + Math.max(leftDepth, rightDepth)
    }
  }
  return getDepth(root) !== -1
};


/** 257. 二叉树的所有路径
给你一个二叉树的根节点 root ，按 任意顺序 ，返回所有从根节点到叶子节点的路径。

叶子节点 是指没有子节点的节点。
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {string[]}
 */
var binaryTreePaths = function (root) {
  if (!root) return []

  // // 递归法
  // const res = []
  // // 1. 确定递归函数、参数
  // const getPath = (node, curPath) => {
  //   // 2. 确定终止提哦啊见，到叶子节点就终止
  //   if (node.left === null && node.right === null) {
  //     curPath += node.val
  //     res.push(curPath)
  //     return
  //   }
  //   // 3. 确定单层递归逻辑
  //   curPath += node.val + '->'
  //   node.left && getPath(node.left, curPath)
  //   node.right && getPath(node.right, curPath)
  // }
  // getPath(root, '')
  // return res


  // 迭代法
  const stack = [root]
  const res = [], paths = ['']
  while(stack.length) {
    const node = stack.pop()
    let path = paths.pop()
    if (!node.left && !node.right) {
      res.push(path + node.val)
      continue
    }
    path += node.val + '->'
    if (node.right) {
      stack.push(node.right)
      paths.push(path)
    }
    if (node.left) {
      stack.push(node.left)
      paths.push(path)
    }
  }
  return res
}















