/**
 * 1. 概念
 * 回溯法也可以叫做回溯搜索法，它是一种搜索的方式。
 *
 * 回溯是递归的副产品，只要有递归就会有回溯。所以以下讲解中，回溯函数也就是递归函数，指的都是一个函数。
 *
 * 因为回溯的本质是穷举，穷举所有可能，然后选出我们想要的答案，如果想让回溯法高效一些，可以加一些剪枝的操作，但也改不了回溯法就是穷举的本质。
 *
 *
 * 2. 回溯法能解决的问题
 *
  - 组合问题：N个数里面按一定规则找出k个数的集合    -> 收集树形结构中叶子节点的结果
  - 切割问题：一个字符串按一定规则有几种切割方式    -> 收集树形结构中叶子节点的结果
  - 子集问题：一个N个数的集合里有多少符合条件的子集 -> 收集树形结构中树的所有节点的结果
  - 排列问题：N个数按一定规则全排列，有几种排列方式
  - 棋盘问题：N皇后，解数独等等
 *
 */


/**
 * 相关题目：
 *
 * 77. 组合
 * 216. 组合总和 III
 * 17. 电话号码的字母组合
 * 39. 组合总和
 * 40. 组合总和 II
 * 131. 分割回文串
 * 93. 复原 IP 地址
 * 78. 子集
 * 90. 子集 II
 * 491. 递增子序列
 * 46. 全排列
 * 47. 全排列 II
 * 332. 重新安排行程*
 * 51. N 皇后*
 * 37. 解数独*
 *
 *
 *
 */


/** 回溯法模板
 *
 * 回溯问题抽象为树形结构：for循环是横向遍历，递归是纵向遍历
 *
 * function backtracking(参数) {
 *   if (终止条件) {
 *     存放结果;
 *     return;
 *   }
 *
 *   for (选择：本层集合中元素（树中节点孩子的数量就是集合的大小）) {
 *     处理节点;
 *     backtracking(路径，选择列表); // 递归
 *     回溯，撤销处理结果
 *   }
 * }
 *
 */


/** 77. 组合
给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。
 */
/**
 * @param {number} n
 * @param {number} k
 * @return {number[][]}
 */
//  回溯三部曲
const combine = function (n, k) {
  const result = [] // 存放符合条件结果的集合
  const path = [] // 用来存放符合条件结果
  // (1)递归函数的返回值以及参数
  const backtracking = (n, k, startIndex) => {
    // (2)回溯函数终止条件
    if (path.length === k) {
      result.push(path.slice())
      return
    }
    // (3)单层搜索的过程
    for (let i = startIndex; i <= n - (k - path.length) + 1; i++) { // 剪枝
      path.push(i) // 处理节点
      backtracking(n, k, i + 1) // 递归
      path.pop() // 回溯，撤销处理的节点
    }
  }
  backtracking(n, k, 1)
  return result
}
console.log(combine(4, 3))


/** 216. 组合总和 III
 * 找出所有相加之和为 n 的 k 个数的组合，且满足下列条件：
 *
 * 只使用数字1到9
 * 每个数字 最多使用一次
 * 返回 所有可能的有效组合的列表 。该列表不能包含相同的组合两次，组合可以以任何顺序返回
 */
/**
 * @param {number} k
 * @param {number} n
 * @return {number[][]}
 */
// 回溯三部曲
const combinationSum3 = function(k, n) {
  const result = []
  const path = []
  // (1)递归函数的返回值以及参数
  const backtracking = (n, k, startIndex) => {
    // (2)回溯函数终止条件
    if (path.length === k) {
      const sum = path.reduce((a, b) => a + b, 0)
      if (sum === n) {
        result.push(path.slice())
      }
      return
    }
    // (3)单层搜索的过程
    for (let i = startIndex; i <= 9 - (k - path.length) + 1; i++) { // 剪枝
      path.push(i)
      backtracking(n, k, i + 1)
      path.pop()
    }
  }
  backtracking(n, k, 1)
  return result
}
console.log(combinationSum3(3, 9))


/** 17. 电话号码的字母组合
* 给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。
*
* 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。

* 示例 1：
* 输入：digits = "23"
* 输出：["ad","ae","af","bd","be","bf","cd","ce","cf"]
*/
/**
 * @param {string} digits
 * @return {string[]}
 */
const letterCombinations = function(digits) {
  if (!digits) return []
  const result = []
  const path = []
  const letterMap = [
    '',
    '',
    'abc',
    'def',
    'ghi',
    'jkl',
    'mno',
    'pqrs',
    'tuv',
    'wxyz',
  ]
  const backtracking = (n, k, startIndex) => {
    if (path.length === k) {
      result.push(path.join(''))
      return
    }
    for (let i = 0; i < letterMap[n[startIndex]].length; i++) {
      path.push(letterMap[n[startIndex]][i])
      backtracking(n, k, startIndex + 1)
      path.pop()
    }
  }
  backtracking(digits, digits.length, 0)
  return result
}
console.log('17. 电话号码的字母组合', letterCombinations('23'))


/** 39. 组合总和
给你一个 无重复元素 的整数数组 candidates 和一个目标整数 target ，找出 candidates 中可以使数字和为目标数 target 的 所有 不同组合 ，并以列表形式返回。你可以按 任意顺序 返回这些组合。

candidates 中的 同一个 数字可以 无限制重复被选取 。如果至少一个数字的被选数量不同，则两种组合是不同的。 

对于给定的输入，保证和为 target 的不同组合数少于 150 个。

示例 1：
输入：candidates = [2,3,6,7], target = 7
输出：[[2,2,3],[7]]
解释：2 和 3 可以形成一组候选，2 + 2 + 3 = 7 。注意 2 可以使用多次。
7 也是一个候选， 7 = 7 。
仅有这两种组合。

示例 2：
输入: candidates = [2,3,5], target = 8
输出: [[2,2,2,2],[2,3,3],[3,5]]
 */
/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
// 回溯三部曲
const combinationSum = function(candidates, target) {
  const result = []
  const path = []
  candidates = candidates.sort((a, b) => a -b)
  // (1)递归函数的返回值及参数
  const backtracking = (n, k, sum, startIndex) => {
    // (2)递归函数终止条件
    if (sum === k) {
      result.push(path.slice())
      return
    } else if (sum > k) {
      return
    }
    // (3)单层搜索逻辑
    for (let i = startIndex; i < n.length && sum + n[i] <= k; i++) { // 剪枝
      sum += n[i]
      path.push(n[i])
      backtracking(n, k, sum, i) // 此处是i，非i+1，表示元素可重复
      sum -= n[i]
      path.pop()
    }
  }
  backtracking(candidates, target, 0, 0)
  return result
}


/** 40. 组合总和 II
给定一个候选人编号的集合 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。

candidates 中的每个数字在每个组合中只能使用 一次 。
注意：解集不能包含重复的组合。 

示例 1:
输入: candidates = [10,1,2,7,6,1,5], target = 8,
输出:
[
  [1,1,6],
  [1,2,5],
  [1,7],
  [2,6]
]
 */
/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
//  回溯三部曲
const combinationSum2 = function(candidates, target) {
  const result = []
  const path = []
  candidates = candidates.sort((a, b) => a - b)
  const backtracking = (n, k, sum, startIndex) => {
    if (sum > k) return
    if (sum === k) {
      result.push(path.slice())
      return
    }
    for (let i = startIndex; i < n.length && sum + n[i] <= k; i++) { // 剪枝
      // used[i - 1] == true，说明同一树枝candidates[i - 1]使用过
      // used[i - 1] == false，说明同一树层candidates[i - 1]使用过
      // 要对同一树层使用过的元素进行跳过
      if (i > 0 && n[i] === n[i - 1] && !used[i - 1]) continue
      sum += n[i]
      path.push(n[i])
      used[i] = true
      backtracking(n, k, sum, i+1)
      used[i] = false
      sum -= n[i]
      path.pop()
    }
  }
  const used = Array(candidates.length).fill(false)
  backtracking(candidates, target, 0, 0, used)
  return result
}


/** 131. 分割回文串
给你一个字符串 s，请你将 s 分割成一些子串，使每个子串都是 回文串 。返回 s 所有可能的分割方案。

回文串: 是正着读和反着读都一样的字符串。

示例 1：
输入：s = "aab"
输出：[["a","a","b"],["aa","b"]]
 */
/**
 * @param {string} s
 * @return {string[][]}
 */
// 回溯三部曲
var partition = function(s) {
  const result = []
  const path = []
  const len = s.length
  // 双指针判断是否回文串
  const checkFn = (s, l, r) => {
    for (let i = l, j = r; i < j; i++, j--) {
      if (s[i] !== s[j]) return false
    }
    return true
  }
  const backtracking = (s, startIndex) => {
    if (startIndex >= len) {
      result.push(path.slice())
      return
    }
    for (let i = startIndex; i < len; i++) {
      if (!checkFn(s, startIndex, i)) continue
      path.push(s.slice(startIndex, i + 1))
      backtracking(s, i + 1)
      path.pop()
    }
  }
  backtracking(s, 0)
  return result
}
console.log('131. 分割回文串', partition("efe"))


/** 93. 复原 IP 地址
有效 IP 地址 正好由四个整数（每个整数位于 0 到 255 之间组成，且不能含有前导 0），整数之间用 '.' 分隔。

例如："0.1.2.201" 和 "192.168.1.1" 是 有效 IP 地址，但是 "0.011.255.245"、"192.168.1.312" 和 "192.168@1.1" 是 无效 IP 地址。
给定一个只包含数字的字符串 s ，用以表示一个 IP 地址，返回所有可能的有效 IP 地址，这些地址可以通过在 s 中插入 '.' 来形成。你 不能 重新排序或删除 s 中的任何数字。你可以按 任何 顺序返回答案。

输入：s = "101023"
输出：["1.0.10.23","1.0.102.3","10.1.0.23","10.10.2.3","101.0.2.3"]
*/
/**
 * @param {string} s
 * @return {string[]}
 */
// 回溯三部曲
var restoreIpAddresses = function(s) {
  const result = []
  const path = []
  // 1、递归函数的参数及返回值
  const backtracking = (s, startIndex) => {
    // 2、递归函数的终止条件
    if (path.length === 4 && path.join('').length === s.length) {
      result.push(path.join('.'))
      return
    }
    if (path.length >= 4) return
    // 3、确定单层搜索逻辑
    for (let i = startIndex; i < s.length; i++) {
      const str = s.slice(startIndex, i + 1)
      const n = Number(str)
      if (str.length > String(n).length || n > 255) continue
      path.push(n)
      backtracking(s, i + 1)
      path.pop() // 回溯
    }
  }
  backtracking(s, 0)
  return result
}
console.log('93. 复原 IP 地址', restoreIpAddresses('101023'))


/** 78. 子集
给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。

解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。

输入：nums = [1,2,3]
输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
 */
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
// 回溯三部曲
var subsets = function(nums) {
  const result = []
  const path = []
  const backtracking = (nums, startIndex) => {
    result.push(path.slice())
    // 终止条件可以不加 因为本来我们就要遍历整棵树
    if (startIndex >= nums.length) return
    for (let i = startIndex; i < nums.length; i++) {
      path.push(nums[i])
      backtracking(nums, i + 1)
      path.pop()
    }
  }
  backtracking(nums, 0)
  return result
}
console.log('78. 子集', subsets('123'))


/** 90. 子集 II
给你一个整数数组 nums ，其中可能包含重复元素，请你返回该数组所有可能的子集（幂集）。

解集 不能 包含重复的子集。返回的解集中，子集可以按 任意顺序 排列。

输入：nums = [1,2,2]
输出：[[],[1],[1,2],[1,2,2],[2],[2,2]]
 */
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
// 回溯三部曲
var subsetsWithDup = function(nums) {
  const result = []
  const path = []
  nums = nums.sort((a, b) => a - b)
  const used = Array(nums.length).fill(false)
  const backtracking = (nums, startIndex, used) => {
    result.push(path.slice())
    if (startIndex >= nums.length) return
    for (let i = startIndex; i < nums.length; i++) {
      // used[i-1] == true 同一树枝 nums[i-1] 使用过
      // used[i-1] == false 同一树层 nums[i-1] 使用过
      // 对同一层使用过的元素进行跳过 
      // （1）剪枝方法 1
      // if (i > 0 && nums[i] === nums[i - 1] && !used[i - 1]) continue
      // （2）剪枝方法 1
      if (i > startIndex && nums[i] === nums[i - 1]) continue
      path.push(nums[i])
      used[i] = true
      backtracking(nums, i + 1, used)
      used[i] = false
      path.pop()
    }
  }
  backtracking(nums, 0, used)
  return result
}


/** 491. 递增子序列
给你一个整数数组 nums ，找出并返回所有该数组中不同的递增子序列，递增子序列中 至少有两个元素 。你可以按 任意顺序 返回答案。

数组中可能含有重复元素，如出现两个整数相等，也可以视作递增序列的一种特殊情况。

示例 1：
输入：nums = [4,6,7,7]
输出：[[4,6],[4,6,7],[4,6,7,7],[4,7],[4,7,7],[6,7],[6,7,7],[7,7]]

解：本题求自增子序列，是不能对原数组进行排序的，排完序的数组都是自增子序列了。所以不能使用之前的去重逻辑！
 */
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
//  回溯三部曲
var findSubsequences = function(nums) {
  const result = []
  const path = []

  const backtracking = (nums, startIndex) => {
    if (path.length > 1) {
      result.push(path.slice())
      // 注意这里不要加return，要取树上的节点
    }
    // 使用set对本层元素进行去重
    const set = new Set()
    for (let i = startIndex; i < nums.length; i++) {
      // 剪枝
      if (path.length && nums[i] < path[path.length - 1] || set.has(nums[i])) continue
      set.add(nums[i])
      path.push(nums[i])
      backtracking(nums, i + 1)
      path.pop()
    }
  }
  backtracking(nums, 0)
  return result
}
findSubsequences('491. 递增子序列', [1,2,3,1,1])


/** 46. 全排列
给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。

示例 1：
输入：nums = [1,2,3]
输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
 */
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
//  回溯三部曲
var permute = function(nums) {
  const result = []
  const path = []
  const backtracking = (nums) => {
    if (path.length === nums.length) {
      result.push(path.slice())
      return
    }
    for (let i = 0; i < nums.length; i++) {
      if (path.includes(nums[i])) continue
      path.push(nums[i])
      backtracking(nums)
      path.pop()
    }
  }
  backtracking(nums)
  return result
}


/** 47. 全排列 II
给定一个可包含重复数字的序列 nums ，按任意顺序 返回所有不重复的全排列。

示例 1：
输入：nums = [1,1,2]
输出：[ [1,1,2], [1,2,1], [2,1,1] ]
 */
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
//  回溯三部曲
var permuteUnique = function(nums) {
  const result = []
  const path = []
  nums = nums.sort((a, b) => a - b)

  const backtracking = (nums) => {
    if (!nums.length) {
      result.push(path.slice())
      return
    }
    // 使用set对本层元素进行去重
    const set = new Set()
    for (let i = 0; i < nums.length; i++) {
      if (set.has(nums[i])) continue
      set.add(nums[i])
      path.push(nums[i])
      const newNums = nums.slice()
      newNums.splice(i, 1)
      backtracking(newNums)
      path.pop()
    }
  }
  backtracking(nums)
  return result
}


/** 332. 重新安排行程*
给你一份航线列表 tickets ，其中 tickets[i] = [fromi, toi] 表示飞机出发和降落的机场地点。请你对该行程进行重新规划排序。

所有这些机票都属于一个从 JFK（肯尼迪国际机场）出发的先生，所以该行程必须从 JFK 开始。如果存在多种有效的行程，请你按字典排序返回最小的行程组合。

例如，行程 ["JFK", "LGA"] 与 ["JFK", "LGB"] 相比就更小，排序更靠前。
假定所有机票至少存在一种合理的行程。且所有的机票 必须都用一次 且 只能用一次。

示例 1：
输入：tickets = [["MUC","LHR"],["JFK","MUC"],["SFO","SJC"],["LHR","SFO"]]
输出：["JFK","MUC","LHR","SFO","SJC"]
 */
/**
 * @param {string[][]} tickets
 * @return {string[]}
 */
// 直觉上来看 这道题和回溯法没有什么关系，更像是图论中的深度优先搜索。
// 实际上确实是深搜，但这是深搜中使用了回溯的例子，在查找路径的时候，如果不回溯，怎么能查到目标路径呢。
var findItinerary = function(tickets) {
  const result = ['JFK']
  const map = {}

  for (const ticket of tickets) {
    const [from, to] = ticket
    if (!map[from]) {
      map[from] = []
    }
    map[from].push(to)
  }

  for (const city in map) {
    // 对到达城市列表排序
    map[city].sort()
  }

  const backtracking = () => {
    if (result.length === tickets.length + 1) return true
    if (!map[result[result.length - 1]]?.length) return false
    for (let i = 0; i < map[result[result.length - 1]].length; i++) {
      const city = map[result[result.length - 1]][i]
      // 删除已走过航线，防止死循环
      map[result[result.length - 1]].splice(i, 1)
      result.push(city)
      if (backtracking()) return true
      // 回溯
      result.pop()
      map[result[result.length - 1]].splice(i, 0, city)
    }
  }

  backtracking()
  return result
}
console.log('332. 重新安排行程:', findItinerary([["MUC","LHR"],["JFK","MUC"],["SFO","SJC"],["LHR","SFO"]]))


/** 51. N 皇后*
按照国际象棋的规则，皇后可以攻击与之处在同一行或同一列或同一斜线上的棋子。

n 皇后问题 研究的是如何将 n 个皇后放置在 n×n 的棋盘上，并且使皇后彼此之间不能相互攻击。

给你一个整数 n ，返回所有不同的 n 皇后问题 的解决方案。

每一种解法包含一个不同的 n 皇后问题 的棋子放置方案，该方案中 'Q' 和 '.' 分别代表了皇后和空位。

示例 1：
输入：n = 4
输出：[[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]
解释：如上图所示，4 皇后问题存在两个不同的解法。
 */
/**
 * @param {number} n
 * @return {string[][]}
 */
var solveNQueens = function(n) {
  const result = []

  const isValid = (row, col, chessBoard, n) => {
    // 检查列
    for (let i = 0; i < row; i++) { // 这是一个剪枝
      if (chessBoard[i][col] === 'Q') return false
    }
    // 检查 45度角是否有皇后
    for (let i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--) {
      if (chessBoard[i][j] === 'Q') return false
    }
    // 检查 135度角是否有皇后
    for (let i = row - 1, j = col + 1; i >= 0 && j < n; i--, j++) {
      if (chessBoard[i][j] === 'Q') return false
    }
    return true
  }
  
  const transformChessBoard = (chessBoard) => {
    const chessBoardBack = []
    chessBoard.forEach(row => {
      let rowStr = ''
      row.forEach(value => {
        rowStr += value
      })
    chessBoardBack.push(rowStr)
    })
    return chessBoardBack
  }

  // n 为输入的棋盘大小
  // row 是当前递归到棋盘的第几行了
  const backtracking = (row, chessBoard) => {
    if (row === n) {
      result.push(transformChessBoard(chessBoard))
      return
    }
    for (let col = 0; col < n; col++) {
      if (isValid(row, col, chessBoard, n)) { // 验证合法就可以放
        chessBoard[row][col] = 'Q' // 放置皇后
        backtracking(row + 1, chessBoard)
        chessBoard[row][col] = '.' // 回溯，撤销皇后
      }
    }
  }

  const chessBoard = Array(n).fill([]).map(() => Array(n).fill('.'))
  backtracking(0, chessBoard)
  return result
}
console.log('51. N 皇后:', solveNQueens(4))

/**
 * 37. 解数独*
 */
/**
 * @param {character[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var solveSudoku = function(board) {
  const isValid = (row, col, val, board) => {
    const len = board.length
    // 行不能重复
    for (let i = 0; i < len; i++) {
      if (board[row][i] === val) return false
    }
    // 列不能重复
    for (let i = 0; i < len; i++) {
      if (board[i][col] === val) return false
    }
    const startRow = Math.floor(row / 3) * 3
    const startCol = Math.floor(col / 3) * 3

    for (let i = startRow; i < startRow + 3; i++) {
      for (let j = startCol; j < startCol + 3; j++) {
        if (board[i][j] === val) return false
      }
    }

    return true
  }

  const backtracking = () => {
    for (let i = 0; i < board.length; i++) {
      for (let j = 0; j < board[0].length; j++) {
        if (board[i][j] !== '.') continue
        for (let val = 1; val <= 9; val++) {
          if (isValid(i, j, `${val}`, board)) {
            board[i][j] = `${val}`
            if (backtracking()) return true
            board[i][j] = '.'
          }
        }
        return false
      }
    }
    return true
  }

  backtracking(board)
}










