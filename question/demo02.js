/**
 * ./imgs/01-07.png
 */

/**
 * 1. 获取 0 - 100 的质数
 * 质数是只能被 1 和 他本身 整除的数字
 */
function getPrimeNumber () {
  const arr = []
  const judge = (n) => {
    let isPrime = true
    for (let j = 2; j < n; j++) {
      if (n % j === 0) {
        isPrime = false
        break
      }
    }
    return isPrime
  }
  for (let i = 2; i <= 100; i++) {
    judge(i) && arr.push(i)
  }
  return arr
}
const arr1 = getPrimeNumber()
console.log('1:', arr1.length, arr1)

/**
 * 2. 产生一个长度为 100 的整数数组，为数组中的每一项随机填充 1 - 100 之间的数字并保证不重复，尽可能考虑优化效率
 */
function fillArray () {
  const arr = []
  for (let i = 0; i < 100; i++) {
    // 生成1到100的随机数: 原理是 随机数和最大值减最小值的差相乘 最后再加上最小值
    const randomNum = Math.floor(Math.random() * (100 - 1)) + 1
    !arr.includes(randomNum) && arr.push(randomNum)
  }
  return arr
}
console.log('2:', fillArray())

/**
 * 3. 请编写一个高效率的函数找出字符串中第一个无重复的字符，并统计每个重复字符的重复次数
 * 例如 “total” 中，第一个无重复的字符是 “o”；“teeter” 中，第一个无重复的字符是 “r”
 */
function findStr (str) {
  const arr = str.split('')
  let map = {}
  arr.forEach(el => {
    map[el] ? map[el]++ : map[el] = 1
  })
  const noRepeatStr = Object.keys(map).find(key => map[key] === 1)
  return {
    noRepeatStr,
    map
  }
}
console.log('3:', findStr('total'), findStr('teeter'))

/**
 * 4. 定义 Fibonacci(斐波那契数列) 数列为符合如下条件的数列
 * f(n) = 1, n = 1
 *        2, n = 2
 *        f(n-1) + f(n-2), n>2
 * 数列形如：1 2 3 5 6 13 21 ...。 也就是当 n 为 1 时是 1，n 为 2 时是 2，n大于 2 时，是前面两个数字之和
 * 要求写一个函数：输入 n，输出数列的第 n 项是多少
 */
// 利用变量实现，只保存当前的值和前面两个的值。时间复杂度O(n), 空间复杂度O(1)
function fib (n) {
  let pre = 1, cur = 1, sum = 0
  // Number.isInteger(n) 是否是整数
  if (Number.isInteger(n)) {
    if (n < 0) {
      return -1
    } else {
      for (let i = 3; i <= n; i++) {
        sum = pre + cur
        pre = cur
        cur = sum
      }
    }
    return sum
  }
}
console.log('4:', fib(10))

/**
 * 5. 设计一个函数：输入一个递增排序的数组和一个数字 s，在数组中查找两个数，使得它们的和正好是 s
 */
function findTwoNum (arr, s) {
  for (let i = 0; i < arr.length; i++) {
    let leftIndex = i
    let rightIndex = arr.length - 1
    let leftVal = arr[leftIndex]
    let rightVal = arr[rightIndex]

    while (leftIndex < rightIndex) {
      if (leftVal + rightVal === s) {
        return [leftVal, rightVal]
      } else if (leftVal + rightVal < s) {
        leftIndex++
      } else if (leftVal + rightVal > s) {
        rightIndex--
      }
    }
  }
  return null
}
console.log('5:', findTwoNum([1,2,3,4,5,6], 8))

/**
 * 6. 求解：一元钱可以买两瓶汽水，两个汽水瓶可以换一瓶汽水，请问，五元钱最多可以喝到多少瓶汽水？
 */
// 将数字不断整除 和 除余 2，中间的整除数 和 余数 即最终结果
function getSumPing (n) {
  let cur = n
  let sum = n * 2 + cur
  while (cur > 1) {
    const remainder = cur % 2
    cur = Math.floor(cur / 2)
    sum = sum + cur + remainder
  }
  return sum
}
console.log('6:', getSumPing(5))

/**
 * 7. 实现输入一个无序整数数组查找第 k 大的数据
 */
function findXMax (arr, k) {
  let result = null
  let data = arr.slice()
  for (let i = 0; i < k; i++) {
    const min = Math.min(...data)
    const delIndex = data.indexOf(min)
    if (i === k - 1) {
      result = min
    } else {
      data.splice(delIndex, 1)
    }
  }
  return result
}
console.log('7: ', findXMax([10, 5, 8, 200, 88, 2, 0, -5, 2], 4))

/**
 * 8. 有一张地图，用 2 维矩阵 map[m][n] 表示。矩阵中每个元素的值为 1 或 0，为 1 表示有障碍物，为 0 表示无障碍物。
 * 地图中任意两个相邻（上下左右的相邻，不包括对角线）的无障碍物的节点可以相通。
 * 现在给定地图中任意两个节点，求两点之间最短的路径。如果通过的话，通过控制台用 printf 输出一条最短路径的序列，并返回路径的步数
 */
// https://blog.csdn.net/qq_43665821/article/details/119969624
// todo
function getSequence (arr) {
  // 假定点分别为 A(x1, y1), B(x2, y2)
  // let x1, x2, y1, y2
  // const minStepSum = Math.abs(x2 - x1) + Math.abs(y2 - y1) - 1
  // return minStepSum
}
const arr = [
  [ 1, 0, 0, 0, 0 ],
  [ 0, 0, 0, 1, 0 ],
  [ 0, 0, 1, 1, 0 ],
  [ 0, 0, 1, 1, 0 ],
  [ 0, 0, 0, 0, 0 ],
]
console.log('8: ', getSequence(arr))


/**
 * 9. var的变量声明提升特性。以下代码打印结果
 */
// HI AJ
// 首先全局声明了变量name。执行进入匿名函数后，里面的局部变量name的var声明被提至函数首行(即var name，var的变量声明提升特性)，然后判断name类型，自然而然是undefined
var name = 'World';
(function () {
  if (typeof name === 'undefined') {
    var name = 'AJ'
    console.log('HI ' + name)
  } else {
    console.log('Hello ' + name)
  }
})()

// World string
// 对比参数下列语句：首先全局声明了变量name2。执行进入匿名函数后，获取全局变量name2
var name2 = 'World';
(function () {
  console.log('name2:',name2, typeof name2)
})()


/**
 * 10. js的引用赋值。以下代码打印结果
 */
// https://ke.com
// 函数中的参数o 和 webSite 对象指向同一引用地址
// 所以第一行对对象中的变量赋值都会附上，但第二行对o更改了引用地址，故第三行只是对o中的变量赋值
function changeObjProperty(o) {
  o.siteUrl = 'https://ke.com'
  o = new Object()
  o.siteUrl = 'https://lianjia.com'
}
let webSite = new Object()
changeObjProperty(webSite)
console.log(webSite.siteUrl)


/**
 * 11. 避免回流 与 重绘
 * 1、减少对DOM操作，documentFragment
 * 2、避免对css进行单个修改
 * 3、将需要多次回流的元素脱离文档流
 * 4、避免使用css的js表达式
 * 5、使用css3硬件加速，可以让transform、opacity、filters等动画效果不会引起回流重绘
 */


/**
 * 12. react性能优化是哪个周期函数
 * shouldComponentUpdate 这个方法用来判断是否需要调用render方法重新描绘dom。
 * 因为dom的描绘非常消耗性能，如果我们能在shouldComponentUpdate方法中能够写出更优化的dom diff算法，可以极大的提高性能
 */


/**
 * 13. webpack中的loader 和 plugin的区别
 * - loader 是文件加载器，用来预处理文件能，并对这些文件进行一些处理，诸如编译、压缩等，最终一起打包到指定的文件中
 * - plugin 赋予了 webpack 各种灵活的功能，例如打包优化、资源管理、环境变量注入等，目的是解决 loader 无法实现的其他事
 */


/**
 * 14. 如何优化webpack配置
 * （1）优化前端性能：https://vue3js.cn/interview/webpack/performance.html#%E4%BA%8C%E3%80%81%E5%A6%82%E4%BD%95%E4%BC%98%E5%8C%96
 * - 资源压缩（gzip 压缩，js、css、html、图片、文件大小压缩）
 * - webpack 分包（提取公共代码、分隔代码）
 *
 * （2）提高构建速度：https://vue3js.cn/interview/webpack/improve_build.html#%E4%BA%8C%E3%80%81%E5%A6%82%E4%BD%95%E4%BC%98%E5%8C%96
 * - 优化loader配置
 * - 优化resolve.alias
 * - 使用 cache-loader
 * - terser 启动多线程
 * - 合理使用 sourceMap
 */


/**
 * 15. 现有一种情况，每次输入一个字母，就打印出最后输入的三个字母（如不足三个，用0补足）
 * 如：
 * 输入 a，则打印 a, 0, 0
 * 再输入 b，则打印 b，a，0
 * 再输入 c，则打印 c，b，a
 * 再输入 d，则打印 d，c，b
 * 再输入 c，则打印 c，d，b
 * 再输入 c，则打印 c，d，b
 *
 * 再输入 a，则打印 a，c，d
 * ...
 */
// 运用闭包保存外部变量的作用域实现
// 扩展：更多作用域说明见 https://mp.weixin.qq.com/s/2R58yPHmroE8VzR48p5SIw
function getThreeChar () {
  let arr = []
  return (char) => {
    arr.unshift(char)
    arr = [...new Set(arr)]
    const len = arr.length
    if (len < 3) {
      for (let i = 0; i < 3 - len; i++) {
        arr.push(0)
      }
    }
    return arr.slice(0, 3).join(',')
  }
}

const fn = getThreeChar()
console.log('15 a 0 0: ',fn('a'))
console.log('15 b a 0: ',fn('b'))
console.log('15 c b a: ',fn('c'))
console.log('15 d c b: ',fn('d'))
console.log('15 c d b: ',fn('c'))
console.log('15 c d b: ',fn('c'))
console.log('15 a c d: ',fn('a'))


/**
 * 16. 清除字符串前后的空格（兼容所有浏览器）
 */
function clearSpace (str) {
  return str.replace(/^\s*|\s*$/g, '')
}
console.log(clearSpace('   测试  问 本    '))


/**
 * 17. 写出遍历A节点的父节点下所有子节点的代码
 */
const pEle = document.getElementById('A').parentElement
const childEle = pEle.children
for (let i = 0; i < childEle.length; i++) {
  // console.log(childEle[i])
}


/**
 * 18. 数组去重方法 6 种：demo-project02/src/views/Page03.vue
 */









