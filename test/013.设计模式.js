/**
 * @date 2023-05-13 15:22:53
 * @desc 设计模式应用实例
 */

/**
 * 工厂模式
 */
class ProductA {
  constructor(name) {
    this.name = name
  }
  produce () {
    console.log('produce A is producing..')
    return `produce A: ${this.name}`
  }
}

class ProductB {
  constructor(name) {
    this.name = name
  }
  produce () {
    console.log('produce B is producing..')
    return `product B: ${this.name}`
  }
}

class Factory {
  create (type, name) {
    switch (type) {
      case 'A':
        return new ProductA(name)
      case 'B':
        return new ProductB(name)
      default:
        throw new Error('不存在的产品类型')
    }
  }
}

// 使用
const factory = new Factory()
const productA = factory.create('A', 'productA')
const productB = factory.create('B', 'productB')
productA.produce() // produce A is producing..
productB.produce() // produce B is producing..

/**
 * 单例模式
 */
class Singleton {
  constructor() {
    if (typeof Singleton.instance === 'object') {
      return Singleton.instance
    }
    this.name = 'Singleton'
    Singleton.instance = this
    return this
  }
}
const singleton1 = new Singleton()
const singleton2 = new Singleton()
console.log('对比：', singleton1 === singleton2) // true


/**
 * 外观模式
 */
function model1 () {
  // do something...
}
function model2 () {
  // do something...
}
function use () {
  model1()
  model2()
}

/**
 * 代理模式
 */
class Image {
  constructor(url) {
    this.url = url
    this.loadImage()
  }
  loadImage() {
    console.log(`Loading image from ${this.url}`)
  }
}

class ProxyImage {
  constructor(url) {
    this.url = url
  }
  loadImage() {
    if (!this.image) {
      this.image = new Image(this.url)
    }
    console.log(`Displaying cached image from ${this.url}`)
  }
}
const image1 = new Image('https://example.com/image1.jpg')
const proxyImage1 = new ProxyImage('https://example.com/image1.jpg')

proxyImage1.loadImage(); // Loading image from https://example.com/image1.jpg
proxyImage1.loadImage(); // Displaying cached image from https://example.com/image1.jpg

/**
 * 策略模式
 */
function Strategy (type,a,b) {
  const Strategyer = {
    add: function (a, b) {
      return a + b
    },
    subtract: function (a, b) {
      return a - b
    },
    multip: function (a, b) {
      return a / b
    },
  }
  return Strategyer[type](a, b)
}

/**
 * 迭代器模式
 */
const item = [1, 2, 3, 4, 5]
function Iterator(items) {
  this.items = items;
  this.index = 0;
}
Iterator.prototype = {
  hasNext: function () {
    return this.index < this.items.length;
  },
  next: function () {
    return this.items[this.index++];
  }
}
// use
const iterator = new Iterator(item);
while(iterator.hasNext()){
  console.log('迭代器：',iterator.next()); // 1, 2, 3, 4, 5
}

function Range(start, end) {
  return {
    [Symbol.iterator]: function () {
      return {
        next() {
          if (start < end) {
            return { value: start++, done: false }
          }
          return { value: end, done: true }
        }
      }
    }
  }
}
// use
for (const el of Range(1, 5)) {
  console.log('el:', el) // 1, 2, 3, 4
}

/**
 * 观察者模式
 */
// 被观察者
function Subject() {
  this.observers = []
}
Subject.prototype = {
  // 订阅
  subscribe: function (observer) {
    this.observers.push(observer)
  },
  // 取消订阅
  unsubscribe: function (observerToRemove) {
    this.observers = this.observers.filter(observer => {
      return observer !== observerToRemove
    })
  },
  // 事件触发
  fire: function () {
    this.observers.forEach(observer => {
      observer.call()
    })
  }
}
// use
const subject = new Subject()
const observer1 = () => {
  console.log('observer1 触发了...')
}
const observer2 = () => {
  console.log('observer2 触发了...')
}
subject.subscribe(observer1)
subject.subscribe(observer2)
subject.fire() // observer1 触发了...    observer2 触发了...

/**
 * 中介者模式
 */
// 聊天室成员类
class Member {
  constructor(name) {
    this.name = name
    this.chatroom = null
  }
  // 发送消息
  send (message, toMember) {
    this.chatroom.send(message, this, toMember)
  }
  // 接收消息
  receive (message, fromMember) {
    console.log(`${fromMember.name} to ${this.name}: ${message}`)
  }
}
// 聊天室类
class Chatroom {
  constructor() {
    this.members = {}
  }
  // 增加成员
  addMember (member) {
    this.members[member.name] = member
    member.chatroom = this
  }
  // 发送消息
  send (message, fromMember, toMember) {
    toMember.receive(message, fromMember)
  }
}
// use
const chatroom = new Chatroom()
const John = new Member('John')
const Tom = new Member('Tom')
chatroom.addMember(John)
chatroom.addMember(Tom)
John.send('Hi Tom!', Tom) // John to Tom: Hi Tom!

/**
 * 访问者模式
 */
// 财务报表类
class Report {
  constructor(income, cost, profit) {
    this.income = income
    this.cost = cost
    this.profit = profit
  }
}
// 老板类
class Boss {
  get (data) {
    console.log(`老板访问报表数据，盈利：${data}`)
  }
}
// 财务人员类
class Account {
  get (num1, num2) {
    console.log(`财务人员访问报表数据，收入：${num1}，支出: ${num2}`)
  }
}
// 访问者类
function vistor(data, person) {
  const handle = {
    Boss: function (data) {
      person.get(data.profit)
    },
    Account: function (data) {
      person.get(data.income, data.cost)
    }
  }
  handle[person.constructor.name](data)
}
// use
const report = new Report(1000, 500, 200);
vistor(report, new Account()) // 财务人员访问报表数据，收入：1000，支出: 500
vistor(report, new Boss()) // 老板访问报表数据，盈利：200

























