/*
 * @Date: 2021-01-02 16:24:51
 * @information: 防抖 和 节流
 */

const test = (val) => {
  console.log(val, ' 测试执行...')
}


/**
 * 防抖（Debounce）是指在一段时间内，多次触发同一个函数，只执行最后一次触发的函数调用。
 * 它适用于需要等待一段时间后执行的操作，比如搜索框输入联想功能。防抖可以避免频繁触发函数，减少不必要的计算和请求。
 */
function debounce (fn, delay) {
  let timer
  return function () {
    clearTimeout(timer)
    timer = setTimeout(() => {
      fn.apply(this, arguments)
    }, delay)
  }
}

const fn = debounce(test, 1000)
fn('防抖1')
fn('防抖2')


/**
 * 节流（Throttle）是指在一段时间内，多次触发同一个函数，只执行一次函数调用。
 * 它适用于需要限制函数执行频率的场景，比如滚动事件的处理。节流可以控制函数的执行频率，避免过多的计算和渲染。
 */
function throttle (fn, delay) {
  let timer
  return function () {
    if (!timer) {
      timer = setTimeout(() => {
        fn.apply(this, arguments)
        timer = null
      }, delay)
    }
  }
}

const fn2 = throttle(test, 1000)
fn2('节流1')
fn2('节流2')




