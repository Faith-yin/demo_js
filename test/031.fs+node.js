/*
 * @Date: 2021-09-02 16:27:02
 * @information: fs node
 * 参考: node 文档：http://nodejs.cn/learn/nodejs-accept-arguments-from-the-command-line
 */
const moment = require('moment')
const path = require('path')
// 目录及文件模块
const fs = require('fs')
// 操作系统模块
const os = require('os')
// cowsay 软件包提供了一个命令行程序，可以执行该程序以使母牛说些话（以及其他动物也可以说话）: npx cowsay 我裂了
const cowsay = require('cowsay')

/**
 * 1
 */
const data = fs.readFileSync(path.resolve('32.demo.js'), 'utf8')
console.log('onRead', data)

fs.appendFileSync(path.resolve('32.demo.js'), `\n// 这是追加的内容 - ${moment().format('YYYY-MM-DD HH:mm:ss')}`)


/**
 * 2
 */
console.log('1--->', __dirname)
console.log('2--->', __filename)


/**
 * 3
 */
const obj = {
  name: {
    field: 'yyy',
    id: 09,
    desc: {
      info: {
        key: '1234'
      }
    }
  },
  age: 18
}
console.log(obj)
// 这样在控制台打印深层对象
console.log(JSON.stringify(obj, null, 2))


/**
 * 4
 */
// node命令携带参数： node 31.demo.js username=ypf age=18
// 获取参数值的方法是使用 Node.js 中内置的 process 对象，它公开了 argv 属性，该属性是一个包含所有命令行调用参数的数组
// 第一个参数是 node 命令的完整路径，第二个参数是正被执行的文件的完整路径，所有其他的参数从第三个位置开始
console.log(process.argv)


/**
 * 5
 * package.json 版本管理:
 * 如果写入的是 〜0.13.0，则只更新补丁版本：即 0.13.1 可以，但 0.14.0 不可以。
 * 如果写入的是 ^0.13.0，则要更新补丁版本和次版本：即 0.13.1、0.14.0、依此类推。
 * 如果写入的是 0.13.0，则始终使用确切的版本。
 */


/**
 * 6 异步队列任务执行次序中，优先执行微任务
 * node 中的 process.nextTick 是微任务，setTimeout 是宏任务
 */
setTimeout(() => {
  console.log('setTimeout')
}, 0);
process.nextTick(() => {
  console.log('process.nextTick')
})





