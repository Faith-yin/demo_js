/*
 * @Date: 2021-11-20 14:50:11
 * @information: 33 js函数重载效果实现
 * 参考：
 * https://juejin.cn/post/7031525301414805518#comment
 * https://www.cnblogs.com/yugege/p/5539020.html
 */

/**
 * 1. 利用 switch case arguments 实现重载效果
 */
function fn () {
  switch (arguments.length) {
    case 1:
      var [name] = arguments
      console.log(`我是${name}`)
      break;
    case 2:
      var [name, age] = arguments
      console.log(`我是${name},今年${age}岁`)
      break;
    case 3:
      var [name, age, sport] = arguments
      console.log(`我是${name},今年${age}岁,喜欢运动是${sport}`)
      break;
  }
}



/**
 * 2. 利用 `闭包` 实现重载效果
 */
function addMethod (obj, name, fun) {
  // 把前一次添加的方法存在一个临时变量里
  let old = obj[name]
  // 重写 obj[name] 的方法
  obj[name] = function () {
    if (fun.length === arguments.length) { // 如果调用object[name]方法时，传入的参数个数跟预期的一致，则直接调用
      return fun.apply(this, arguments)
    } else if (typeof old === 'function') { // 否则，判断并调用 old
      return old.apply(this, arguments)
    }
  }
}

let people = {
  values: ["Dean Edwards", "Sam Stephenson", "Alex Russell", "Dean Tom"]
}

/* 下面开始通过addMethod来实现对people.find方法的重载 */

// 不传参数时，返回peopld.values里面的所有元素
addMethod(people, "find", function () {
  return this.values;
});

// 传一个参数时，按first-name的匹配进行返回
addMethod(people, "find", function (firstName) {
  var ret = [];
  for (var i = 0; i < this.values.length; i++) {
    if (this.values[i].indexOf(firstName) === 0) {
      ret.push(this.values[i]);
    }
  }
  return ret;
});

// 传两个参数时，返回first-name和last-name都匹配的元素
addMethod(people, "find", function (firstName, lastName) {
  var ret = [];
  for (var i = 0; i < this.values.length; i++) {
    if (this.values[i] === (firstName + " " + lastName)) {
      ret.push(this.values[i]);
    }
  }
  return ret;
});

// 测试：
console.log(people.find()); // ["Dean Edwards", "Alex Russell", "Dean Tom"]
console.log(people.find("Dean")); // ["Dean Edwards", "Dean Tom"]
console.log(people.find("Dean Edwards")); // ["Dean Edwards"]









