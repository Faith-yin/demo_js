/*
 * @Date: 2021-12-24 10:33:09
 * @information: AST
 * Babel插件就是作用于抽象语法树：
 * Babel 的三个主要处理步骤分别是： 解析（parse），转换（transform），生成（generate）
 */
// parser生成AST树
const esprima = require('esprima')
// traverse对AST树遍历,进行增删改查
const estraverse = require('estraverse')
// generator将更新后的AST转化成代码
const escodegen = require('escodegen')

const code = "const name = 'ypf'"

const ast = esprima.parseScript(code)
console.log(111, ast)

estraverse.traverse(ast, {
  enter: (node) => {
    // console.log('node -> ', node)
    node.name = 'age'
    node.value = 18
  }
})
console.log(222, ast)

const transformCode = escodegen.generate(ast)
console.log(333, transformCode)


