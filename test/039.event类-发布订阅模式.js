/*
 * @Date: 2021-12-27 11:40:07
 * @information: event类 - 发布订阅模式
 * 发布订阅模式 与 观察者模式区别见: test/img/039
 */

class Event {
  constructor() {
    // 储存事件的数据结构
    // 为查找迅速， 使用对象（字典）
    this._cache = {}
  }

  // 监听事件
  on (type, callback) {
    const fns = (this._cache[type] = this._cache[type] || [])
    if (fns.indexOf(callback) === -1) {
      fns.push(callback)
    }
    return this
  }

  // 触发事件
  emit (type, data) {
    const fns = this._cache[type]
    if (Array.isArray(fns)) {
      fns.forEach(el => {
        el(data)
      })
    }
    return this
  }

  // 解绑事件
  off (type, callback) {
    const fns = this._cache[type]
    if (Array.isArray(fns)) {
      if (callback) {
        const index = fns.indexOf(callback)
        index !== -1 && (fns.splice(index, -1))
      } else { // 全部清空
        fns.length = 0
      }
    }
    return this
  }

}

const e = new Event()

e.on('fn', (e) => {
  console.log('触发了', e)
})

e.emit('fn', 18)

e.off('fn')

e.emit('fn', 20)

