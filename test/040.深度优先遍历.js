/*
 * @Date: 2022-01-10 10:05:29
 * @information: 深度优先遍历
 * 参考：https://juejin.cn/post/7049882206000381965
 */
const obj = {
  key: 'AA',
  value: 'A',
  children: [
    {
      key: 'BB',
      value: 'B',
      children: [
        {
          key: 'DD',
          value: 'D',
          children: [
            {
              key: 'HH',
              value: 'H',
              children: []
            }
          ]
        },
        {
          key: 'EE',
          value: 'E',
          children: []
        }
      ]
    },
    {
      key: 'CC',
      value: 'C',
      children: [
        {
          key: 'FF',
          value: 'F',
          children: []
        },
        {
          key: 'GG',
          value: 'G',
          children: []
        }
      ]
    }
  ]
}

/**
 * 深度遍历查找
 * @param {*} tree 树形数据
 * @param {*} target 想要查找的目标
 */
function findValue (tree, target) {
  // 模拟栈，管理节点
  let stack = [tree]
  while (stack.length) {
    // 栈顶节点出栈
    let node = stack.pop()
    // 查找到目标，退出
    if (node.key == target) return node.value
    // 将候选顶点入栈，进行下一次循环
    if (node.children?.length) {
      stack.push(...node.children.reverse())
    }
  }
  return ''
}

console.log(findValue(obj, 'FF'))
