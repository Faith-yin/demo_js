/*
 * @Date: 2022-01-10 17:08:48
 * @information: 手写promise
 * 参考：https://juejin.cn/post/6883121706123132936
 */
class MyPromise {
  constructor(executor) {
    // 初始化state为等待态
    this.state = 'pending'
    // 成功的值
    this.value = undefined
    // 失败的原因
    this.reason = undefined

    // 成功态、失败态回调函数队列，同步调用then时将对应态的函数注册进去, 在状态变更的时候调用
    this.onFulfilledCallbacks = []
    this.onRejectedCallbacks = []

    const resolve = value => {
      // state改变,resolve调用就会失败
      if (this.state === 'pending') {
        // resolve调用后，state转化为成功态
        this.state = 'fulfilled'
        // 储存成功的值
        this.value = value
        // 成功态回调函数依次执行
        this.onFulfilledCallbacks.forEach(fn => fn(this.value))

      }
    }
    const reject = reason => {
      // state改变,reject调用就会失败
      if (this.state === 'pending') {
        // reject调用后，state转化为失败态
        this.state = 'rejected'
        // 储存失败的原因
        this.reason = reason
        // 失败态回调函数依次执行
        this.onRejectedCallbacks.forEach(fn => fn(this.reason))
      }
    }
    // 如果executor执行报错，直接执行reject
    try {
      executor(resolve, reject)
    } catch (err) {
      reject(err)
    }
  }

  then (onFulfilled, onReject) {
    console.log('执行then函数', onFulfilled, onReject)
    // 实现值穿透，当then中传入的不是函数，则这个promise返回上一个promise的值
    onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : val => val
    onReject = typeof onReject === 'function' ? onReject : reason => reason

    // 保存前一个promise的this
    const that = this
    return new MyPromise((resolve, reject) => {
      // 封装前一个promise成功时执行的函数
      const fulfilled = () => {
        try {
          const result = onFulfilled(that.value) // 承前
          return result instanceof MyPromise ? result.then(resolve, reject) : resolve(result) // 启后
        } catch (err) {
          reject(err)
        }
      }

      // 封装前一个promise失败时执行的函数
      const rejected = () => {
        try {
          const result = onReject(that.reason)
          return result instanceof MyPromise ? result.then(resolve, reject) : reject(result)
        } catch (err) {
          reject(err)
        }
      }

      switch (that.state) {
        case 'pending':
          that.onFulfilledCallBacks.push(fulfilled)
          that.onRejectedCallbacks.push(rejected)
          break
        case 'fulfilled':
          fulfilled()
          break
        case 'rejected':
          rejected()
          break
      }
    })
  }

  // Promise.prototype.catch就是Promise.prototype.then(null, onRejected)的别名
  catch (onRejected) {
    console.log('执行catch函数')
    return this.then(null, onRejected)
  }

  finally (callback) {
    return this.then(
      value => {
        return MyPromise.resolve(callback()).then(() => {
          return value
        })
      },
      err => {
        return MyPromise.resolve(callback()).then(() => {
          throw err
        })
      })
  }

  static resolve (value) {
    // 如果是promise实例，直接返回
    if (value instanceof MyPromise) {
      return value
    } else {
      // 如果不是promise实例，返回一个新的promise对象，状态为fulfilled
      return new MyPromise((resolve, reject) => resolve(value))
    }
  }

  static reject (reason) {
    // Promise.reject方法的参数会原封不动地作为reject的参数
    return new MyPromise((resolve, reject) => reject(reason))
  }

  /**
   * Promise.all() 接受一个数组，返回一个promise对象, 数组成员不一定都是promise，需要使用Promise.resolve()处理。
   */
  static all (promiseArr) {
    const len = promiseArr.length
    const values = new Array(len)

    let count = 0 // 记录已经成功的promise个数
    return new MyPromise((resolve, reject) => {
      for (let i = 0; i < len; i++) {
        // Promise.resolve()处理，确保每一个都是promise实例
        MyPromise.resolve(promiseArr[i]).then(
          val => {
            values[i] = val
            count++
            if (count === len) resolve(values) // 如果全部执行完，改变promise的状态为fulfilled
          },
          err => {
            reject(err)
          }
        )
      }
    })
  }

  static race (promiseArr) {
    return new MyPromise((resolve, reject) => {
      promiseArr.forEach(item => {
        MyPromise.resolve(item).then(
          val => resolve(val),
          err => reject(err)
        )
      })
    })
  }

}


// 1. 链式调用
const p = new MyPromise((resolve, reject) => {
  reject('yyy')
}).then(res => {
  console.log('成功了:', res)
}).catch(err => {
  console.log('失败了:', err)
}).finally(() => {
  console.log('finally')
})


// 2. 执行抛错
// function fun () {
//   // return MyPromise.reject(1)
//   return new MyPromise((resolve, reject) => {
//     reject(1)
//   })

//   // return new Promise((resolve, reject) => {
//   //   reject(1)
//   // })
// }

// async function fun2 () {
//   try {
//     let ff = await fun()
//     console.log('成功了', ff)
//   } catch (error) {
//     console.log('出错了', error)
//   }
// }
// fun2()