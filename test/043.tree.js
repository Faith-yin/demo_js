/**
* @date 2022-04-30 14:59:38 星期六
* @information 树形数据和扁平化数据的相互转化
*/

const data = [{
	"dataId": "GEUTJD100000000",
	"key": "PROJECT_ID",
	"selectAll": false,
	"id": "8u839csjwrzo4wvjhbmkbdy4",
	"name": "冀东油田",
	"datasetId": "72edb2e665ead2a512e3c954238ea55b",
	"children": [{
		"dataId": "GEUTJD100000387",
		"key": "PROJECT_ID",
		"selectAll": true,
		"id": "3usmc2c7vtsxahhjdsrecqva",
		"name": "南堡滩海",
		"datasetId": "72edb2e665ead2a512e3c954238ea55b",
		"children": []
	}, {
		"dataId": "GEUTJD100000001",
		"key": "PROJECT_ID",
		"selectAll": false,
		"id": "kr496zqilrkzepjyj68vplgm",
		"name": "南堡陆地",
		"datasetId": "72edb2e665ead2a512e3c954238ea55b",
		"children": [{
			"dataId": "GEUTDG100000450\t",
			"key": "PROJECT_ID",
			"selectAll": true,
			"id": "3qvbboi80twp4dabsp219mof",
			"name": "\t堡古2区块",
			"datasetId": "72edb2e665ead2a512e3c954238ea55b",
			"children": []
		}]
	}]
}]

function findSelected (data, callback) {
  // for (const item of data) {
  //   if (item.selectAll) return item
  //   const el = findSelected(item.children)
  //   if (el) return el
  // }

  data.forEach((item, i) => {
    if (item.selectAll) {
      return callback(item)
    } else {
      return findSelected(item.children, callback)
    }
  })
}

// const res = findSelected(data, (item) => {
//   console.log('结果 item: ', item)
// })

// console.log('结果 res: ', res)


function fun (callback) {
  [1, 2, 3].forEach(item => {
    if (item === 1) {
      return callback(item)
    } else {
      console.log('循环: ', item)
    }
  })
}

fun((item) => {
  console.log('结果: ', item)
})


// data E:\前端相关\demo_js\test\assets\js\unit.js

// 格式化数据-树形数据扁平化
function treeToFlat (data, geounitId) {
  const res = []
  data.forEach((item, index, arr) => {
    const obj = {
      geounitName: item.geounitName,
      geounitId: item.geounitId
    }
    if (geounitId && obj.geounitId !== geounitId) {
      Reflect.set(obj, 'parentId', geounitId)
    }
    res.push(obj)
    if (item.childrens?.length) {
      res.push(...this.treeToFlat(item.childrens, item.geounitId))
    }
  })
  return res
}

// 格式化数据-扁平化数据转树形结构
function flatToTree (data) {
  const result = [] // 存放结果集
  const itemMap = {}
  for (const item of data) {
    const geounitId = item.geounitId
    const parentId = item.parentId
    itemMap[geounitId] = { ...item }
    const treeItem = itemMap[geounitId]

    if (!parentId) {
      result.push(treeItem)
    } else {
      if (!itemMap[parentId]) {
        itemMap[parentId] = {
          childrens: []
        }
      }
      if (!itemMap[parentId].childrens) {
        itemMap[parentId].childrens = []
      }
      itemMap[parentId].childrens.push(treeItem)
    }
  }
  return result
}