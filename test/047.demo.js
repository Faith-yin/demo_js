
/**
 * 输出什么值？
 */
var name = 'World';
(function () {
  if (typeof name === 'undefined') {
    var name = 'AJ'
    console.log('HI ' + name)
  } else {
    console.log('Hello ' + name)
  }
})()

var name2 = 'World';
(function () {
  console.log('name2:',name2, typeof name2)
})()

function changeObjProperty(o) {
  o.siteUrl = 'https://ke.com'
  o = new Object()
  o.siteUrl = 'https://lianjia.com'
}
let webSite = new Object()
changeObjProperty(webSite)
console.log(webSite)


/**
 * 闭包、作用域链、this指向
 */
function createAdd (count) {
  const addFn = function (num) {
  // const addFn = (num) => {
    return num + count
  }
  return addFn
}

const add = createAdd(100)

const obj = {
  aa: 'yyy',
  fn: add
}

const res = obj.fn(5)




