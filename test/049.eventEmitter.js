/**
 * eventEmitter
 */

var eventEmitterMessageList = {} //临时存储的消息数据列表。每个消息事件10条
var agentsBased = null  //跨域代理层
/**
 * 保存指定条数最新数据到指定容器
 * @param {条数} len  type：Number
 * @param {容器} container type：Array
 * @param {数据} data   type: any
 */
var saveData = function (len, container, data) {
    if (container.length >= len) {
        container.splice(0, container.length - len + 1)
    }
    container.push(data)
}
// 需要一个方法批量处理包装数据
var arr = ['area', 'wells', 'units', 'surveys', 'geomodels', 'zonations', 'objTree', 'pointLinkage', 'lineLinkage', 'wellCrossLineLinkage', 'wellDepth', 'workArea', 'PreviewModelBuilding', 'CONNECT_WELL_INFO', 'loadTemplateFromUrlFileName', 'updateDataFromModel']
// 判断是否跨域（需要添加获取父元素主域名的逻辑）
function isCrossDomain() {
    let flag = false
    if (parent != window) {
        try {
            parent.location.href;
        } catch (e) {
            flag = true
        }
    }
    return flag
}
// 补全数据
function completeData(event, val) {
    let data = {
        eventEmitter_events_wells: {
            key: "WELL_ID",
            datasetId: "39afc70e91fd1465089e0d9031380194",
            datasetName: "井基本信息",
            datasetCode: "V_S_CD_WELL"
        },
        eventEmitter_events_units: {
            key: "PROJECT_ID",
            datasetId: "72edb2e665ead2a512e3c954238ea55b",
            datasetName: "地质单元",
            datasetCode: "V_S_CD_GEO_UNIT"
        },
        eventEmitter_events_surveys: {
            key: "SEIS_SURVEY_ID",
            datasetId: "549c762c6444030c8392a45b7bc41838",
            datasetName: "三维地震测网",
            datasetCode: "V_S_CD_SEIS_SURVEY"
        },
        eventEmitter_events_geomodels: {
            key: "GEOMODEL_ID",
            datasetId: "de79c04efbd8c100784e53466f7022f9",
            datasetName: "地质模型",
            datasetCode: "V_S_GR_GEOMODEL"
        },
    }
    let _val
    if (event in data) {
        if (val instanceof Array) {
            _val = val.map(item => Object.assign(item, data[event]))
        }
        else {
            throw new Error('The second argument to the function must be Array')
        }
    }
    else {
        _val = val
    }
    return _val
}

// 实时获取数据，支持多微件获取，支持开启和关闭 √
// 开启和关闭接收消息 √
// 场景隔离 √
// 广播数据发送标准化  √
// 标准事件 √ 
// 通信逻辑链式编辑
// 跨域通信（使用代理域）√

class EventEmitter {
    constructor() {
        this.sendList = [] //存储当前实例send过的事件名
        this.receivingSwitch = true // 接收消息开关
        this.sendSwitch = true      // 发送消息开关
        this.packetNumber = ''      // 分组编号（场景隔离分组标识，接收到消息后判断是否同组，如果非同组直接return不执行任何操作）
        this.events = Object.create(null)   //注册监听的所有消息
        this.broadcastListening = false      //是否实时接收广播消息
        this.eventsDistributed = []
        this.i = '' //同步iframed id用于区分同场景下的相同微件，同一场景内微件的i具有唯一性
        this.linkRelationship = {} //编排后的消息通信配置，用于判断响应来自于哪个微件的什么事件
        this.eventFingerprint = ''  // 事件指纹，用于主动获取数据时临时标识，用后即恢复初始化
        this.actived = true
        this.listening = this.listenFn.bind(this)
        this.defaultCallback = () => {
            console.log('No callbak binding')
        }
        this.onStateChange = () => {
            console.warn('onStateChange No callbak binding')
        }
        this.updateState = () => {
            console.warn('updateState No callbak binding')
            return false
        }
        this.isOnMessage = false
        this.onReceivingOff = () => {   //接收消息关闭状态如果有消息传入并且注册过当前事件则触发该函数不传递任何数据，只做提醒使用
            console.info('接收消息功能已关闭')
        }
        this.onSendingOff = () => {   //接收消息关闭状态如果有消息传入并且注册过当前事件则触发该函数不传递任何数据，只做提醒使用
            console.info('发送消息功能已关闭')
        }
    }
    // 监听本地存储变化并执行事件派发
    listenFn(e) {
        let event = e.key
        // 判断当前实例是否注册过当前事件，如果没注册过那么无需任何操作
        if (this.events[event]) {
            // 判断接收消息开关状态，如果关闭则触发钩子函数后直接终止任何操作
            if (this.receivingSwitch === false) {
                this.onReceivingOff()
                return
            }

            let _val
            let val
            // 反序列化捕获异常，避免空值或者无值报错
            try {
                _val = JSON.parse(e.newValue)
                val = _val.data
            } catch (error) {
                _val = ''
                val = ''
            }
            if ((JSON.stringify(this.linkRelationship) != '{}') && !this.linkRelationship[this.i]) {
                return
            } else if (JSON.stringify(this.linkRelationship) != '{}') {
                // _val.i
                // console.log(this.linkRelationship[this.i][e.key],'this.linkRelationship[this.i]')
                // console.log(
                //     this.linkRelationship[this.i],
                //     e.key.split('_')[2],
                //     _val.i,
                //     (this.linkRelationship[this.i][e.key.split('_')[2]].split(',').indexOf(String(_val.i)) != -1)
                //     )
                if (this.linkRelationship[this.i][e.key.split('_')[2]] && (this.linkRelationship[this.i][e.key.split('_')[2]].split(',').indexOf(String(_val.i)) != -1)) {
                    // console.log('zhixing')
                } else {
                    // console.log('return')
                    return
                }
            }
            // 判断数据分组的编号，如果编号不一致表示不同组，那么直接终止执行
            // 四种情况数据中有和无分组编号，当前实例有和无分组编号，无论哪种组合方式只要不相等那么肯定不是一组
            if (_val.packetNumber != undefined && _val.packetNumber != this.packetNumber) {
                return
            }
            // 判断当前本地存储值的变化是否是销毁动作，如果是则不需要任何操作
            let isDestroy = !Boolean(_val.key)
            if (isDestroy) {
                return
            }
            if (event === 'actively_send_data') {
                if (_val.fingerprint === this.eventFingerprint) {
                    this.events[event].forEach(fn => fn(val, isDestroy))
                    this.eventFingerprint = ''
                    this.events['actively_send_data'] = undefined
                    delete this.events.actively_send_data
                    localStorage.removeItem('actively_send_data')
                    localStorage.removeItem('active_data_acquisition')
                }
            }
            else {
                if (!(event in eventEmitterMessageList)) {
                    eventEmitterMessageList[event] = []
                }
                // val = completeData(event,val)
                saveData(10, eventEmitterMessageList[event], { key: _val.key, content: val })
                this.events[event].length && this.events[event].forEach(fn => fn(val, isDestroy))

            }
        }
    }
    // 开启监听本地存储，利用本地存储实现同源项目跨页面通信。
    listeningLocal() {
        let _that = this
        if (!this.isOnMessage) {
            window.removeEventListener("storage", _that.listening)
            window.addEventListener("storage", _that.listening)
        }
        this.isOnMessage = true
    }
    // 移除监听本地存储事件
    removeListeningLocal() {
        let _that = this
        window.removeEventListener("storage", _that.listening)
    }
    // EventEmitter订阅
    on(event, fn) {
        this.listeningLocal()
        if (!this.eventsDistributed.includes(event)) {
            this.eventsDistributed.push(event)
        }
        this.events[event] = this.events.event || []
        this.events[event].push(fn)
    }
    // EventEmitter移除监听
    off(event, fn) {
        const index = (this.events[event] || []).indexOf(fn)
        if (index === -1) {
            return
        } else {
            this.events[event].splice(index, 1)
        }
        if (JSON.stringify(this.events) === '{}') {
            this.removeListeningLocal()
        }
    }
    // EventEmitter事件派发
    emit(event, val, fingerprint = '') {
        if (this.sendSwitch === false) {
            this.onSendingOff()
            return
        }
        val = completeData(event, val)
        let _val = {
            key: (new Date()).valueOf(),
            data: val,
            fingerprint,
            packetNumber: this.packetNumber,
            i: this.i
        }
        if (!this.sendList.includes(event)) {
            this.sendList.push(event)
        }

        if (!!agentsBased) {
            agentsBased.contentWindow.postMessage({ commmandName: 'agentsBased', type: 'set', key: event, value: JSON.stringify(_val) }, "*")
        }
        else {
            localStorage.setItem(event, JSON.stringify(_val))
        }
        // this.events[event] && this.events[event].length && this.events[event].forEach(fn => fn(val))
    }
    // 主动获取数据
    getSelected(fn) {
        let _that = this
        this.eventFingerprint = (new Date()).valueOf()
        this.on('actively_send_data', fn || _that.defaultCallback)
        this.emit('active_data_acquisition', _that.eventFingerprint)
    }
    // 是否允许接收消息
    allowReceive(state) {
        if ((typeof state) != 'boolean') {
            throw new Error('Arguments to function allowReceive must be Boolean')
        }
        this.receivingSwitch = state
    }
    // 是否允许发送消息
    allowSending(state) {
        if ((typeof state) != 'boolean') {
            throw new Error('Arguments to function allowReceive must be Boolean')
        }
        this.sendSwitch = state
    }
}
// 实例化，页面内可调用，当前文件也有操作

window.eventEmitter = new EventEmitter()


var changeQuarantine = (e) => {
    // 当前事件为微件分组，有值则表示开启隔离分组，空字符串表示取消分组
    if (e.data.commmandName === 'changeQuarantine') {
        eventEmitter.packetNumber = e.data.args.fingerprint
    }
    // tab管理页面获取当前微件是否激活
    if (e.data.commmandName === 'changeActivation') {
        eventEmitter.actived = e.data.args.state
    }
    // 通过代理层转发过来的数据
    if (e.data.commmandName === 'agentsBased') {
        eventEmitter.listenFn(e.data.data)
    }
    // 接收以前保存的场景状态
    if (e.data.commmandName === 'sceneState') {
        // console.log(e.data.stateData,eventEmitter.i)
        if (e.data.stateData && e.data.stateData[`${eventEmitter.i}`]) {
            // console.log(eventEmitter.onStateChange,e.data.stateData[eventEmitter.i])
            eventEmitter.onStateChange(e.data.stateData[eventEmitter.i])
        }
    }
    // 更新保存当前场景状态
    if (e.data.commmandName === 'updateState') {
        // console.log('7878675665656')
        let data = {}
        data[eventEmitter.i] = eventEmitter.updateState()
        window.parent.postMessage(
            {
                commmandName: 'updateStateByWidget',
                data
            },
            "*"
        )
    }
    // 改变连接状态
    if (e.data.commmandName === 'changeLink') {
        eventEmitter.allowReceive(e.data.args.state)
        eventEmitter.allowSending(e.data.args.state)
    }
    // 动态注册标准事件
    if (e.data.commmandName === 'registerEvents') {
        let events = e.data.args.events
        arr.forEach(item => {
            delete eventEmitter[`${item}Change`]
            delete eventEmitter[`send${item}`]
            delete eventEmitter[`off${item}`]
        })
        this.events = Object.create(null)
        this.isOnMessage = false
        events.forEach(item => {
            eventEmitter[`${item}Change`] = (callback) => {
                return eventEmitter.on('eventEmitter_events_' + item, callback)
            }
            eventEmitter[`send${item}`] = (val) => {
                return eventEmitter.emit('eventEmitter_events_' + item, val)
            }
            eventEmitter[`off${item}`] = (val) => {
                return eventEmitter.off('eventEmitter_events_' + item, val)
            }
        })
        eventEmitter.i = e.data.args.key
        window.parent.postMessage(
            {
                commmandName: 'eventEmitterLoaded',
                key: e.data.args.key
            },
            "*"
        )
    }
    // linkRelationship 通信配置
    if (e.data.commmandName === 'linkRelationship') {
        // console.log(e.data.args, 'args')
        eventEmitter.linkRelationship = e.data.args
    }
}
// 监听message事件postmessage负责管理微件消息接收状态。（后面跨域转发也是这里处理）
window.removeEventListener('message', changeQuarantine)
window.addEventListener('message', changeQuarantine)
// 页面销毁的时候清除localstroge的监听事件，删除当前页面曾经发送过的数据，解除监听message事件（清除localstroge里自己发送过的数据为了清理本地存储的空间）
window.onbeforeunload = function () {
    // eventEmitter移除监听本地存储事件
    eventEmitter.removeListeningLocal()
    // 清空当前实例在通过localstroge发送的数据，释放本地存储空间
    eventEmitter.sendList.forEach(item => {
        localStorage.removeItem(item)
    })
    // 移除windwo监听message事件
    window.removeEventListener('message', changeQuarantine)
    // eventEmitter实例指向null释放内存
    // eventEmitter = null
    // 创建的代理层注销
    agentsBased && agentsBased.remove()
}
// 动态注册标准事件，假设arr是从页面配置来的标准事件，这里自动生成这些事件。分别为监听和发送。用这种方法动态注册可以避免标准事件增加而造成的修改代码和重新发版的成本。

arr.forEach(item => {
    eventEmitter[`${item}Change`] = (callback) => {
        return eventEmitter.on('eventEmitter_events_' + item, callback)
    }
    eventEmitter[`send${item}`] = (val) => {
        return eventEmitter.emit('eventEmitter_events_' + item, val)
    }
    eventEmitter[`off${item}`] = (val) => {
        return eventEmitter.off('eventEmitter_events_' + item, val)
    }
})
if (isCrossDomain()) {
    let url = document.referrer
    agentsBased = document.createElement("iframe")
    agentsBased.style = 'display:none'
    agentsBased.src = `${url}front/research-v2/agentsBased`
    if(url.includes('tlmyt.petrochina')) {
        agentsBased.src = `${url}front/assemblyPlatform/agentsBased`
    }
    document.body.appendChild(agentsBased)
}


// export default eventEmitter