
// 手写Promise
class MyPromise {
  constructor(executor) {
    this.status = 'pending'
    this.value = undefined
    this.reason = undefined

    this.onFulfilledCallbacks = []
    this.onRejectedCallbacks = []

    const resove = (value) => {
      if (this.status === 'pending') {
        this.status = 'fulfilled'
        this.value = value
        this.onFulfilledCallbacks.forEach(fn => fn(this.value))
      }
    }

    const reject = (reason) => {
      if (this.status === 'pending') {
        this.status = 'rejected'
        this.reason = reason
        this.onRejectedCallbacks.forEach(fn => fn(this.reason))
      }
    }

    executor(resove, reject)
  }

  then (onFulfilled, onRejected) {
    if (this.status === 'pending') {
      this.onFulfilledCallbacks.push(onFulfilled)
      this.onRejectedCallbacks.push(onRejected)
    } else if (this.status === 'fulfilled') {
      onFulfilled(this.value)
    } else if (this.status === 'rejected') {
      onRejected(this.reason)
    }
  }

  catch (onRejected) {
    // this.then(null, onRejected)
  }

  finally (callback) {
    if (this.status === 'pending') {
      this.onFulfilledCallbacks.push(callback)
      this.onRejectedCallbacks.push(callback)
    } else {
      callback()
    }
  }
}


const p1 = new MyPromise((resolve, reject) => {
  resolve('yyy')
})
p1.then(res => {
  console.log('p1成功:', res)
}, err => {
  console.log('p1失败:', err)
})
p1.catch(err => {
  console.log('p1失败:', err)
})
p1.finally(() => {
  console.log('p1finally')
})

const p2 = new MyPromise((resolve, reject) => {
  setTimeout(() => {
    resolve('yyy')
  }, 100);
})
p2.then(res => {
  console.log('p2成功:', res)
}, err => {
  console.log('p2失败:', err)
})
p2.catch(err => {
  console.log('p2失败:', err)
})
p2.finally(() => {
  console.log('p2finally')
})



