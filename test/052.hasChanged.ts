
// 比较某个值是否发生了变化，考虑NaN。 Vue2中的写法
function hasChanged (x: unknown, y: unknown): boolean {
  if (x === y) {
    return x === 0 && 1 /x !== 1 / (y as number)
  } else {
    return x === x && y === y
  }
}

// 等效于 Vue3中的写法
function hasChanged2 (value: any, oldValue: any): boolean {
  return !Object.is(value, oldValue)
}
