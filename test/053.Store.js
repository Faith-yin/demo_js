/**
 * 创建一个Store类，用于管理应用状态
 */
class Store {
  constructor (initState = {}) {
    // 初始化状态
    this.state = initState
    // 用于存储所有订阅者的集合
    this.listeners = new Set()
  }

  // 获取状态
  getState () {
    return this.state
  }

  // 设置新的状态并通知所有订阅者
  setState (newState) {
    this.state = {
      ...this.state,
      ...newState,
    }
    // 通知所有订阅者
    this.listeners.forEach(listener => listener(this.state))
  }

  // 订阅状态变化
  subscribe (listener) {
    this.listeners.add(listener)
    // 返回一个取消订阅的方法
    return () => this.listeners.delete(listener)
  }

}

// 测试
const store = new Store({
  count: 0,
})
// 订阅状态变化
store.subscribe(state => {
  console.log('count:', state.count)
})
// 设置新的状态
store.setState({ count: 1 })

