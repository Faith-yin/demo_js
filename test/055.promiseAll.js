function fn() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("success")
      // reject("error")
    }, 500)
  })
}

fn().then(
  (res) => {
    console.log('then', res)
  },
  // (err) => {
  //   console.log('then', err)
  // }
).catch((err) => {
  console.log('🔥 catch', err)
})


function all (promiseArr) {
  const values = new Array(promiseArr.length)
  let count = 0
  return new Promise((resolve, reject) => {
    for (const promise of promiseArr) {
      Promise.resolve(promise).then(
        res => {
          values[count] = res
          count++
          if (count === promiseArr.length) resolve(values)
        },
        err => {
          reject(err)
        }
      )
    }
  })
}

all([1, 2, Promise.resolve(3), fn()]).then((res) => {
  console.log('all', res)
}).catch((err) => {
  console.log('all', err)
})




