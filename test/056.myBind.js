Function.prototype.myBind = function (ctx, ...args) {
  const fn = this
  return function (...args2) {
    return fn.apply(ctx, [...args, ...args2])
  }
}


function fn (a, b, c, d) {
  return a + b + c + d
}

const newFn = fn.myBind(1, 2, 3)
const r = newFn(4, 5, 6)
console.log(r)
