/**
* @date 2022-04-30 15:01:45 星期六
* @information 043 的树形数据
*/

export const unit = [
  {
    geounitName: '南堡油田',
    geounitId: '16492960856316515',
    childrens: [
      {
        geounitName: '南堡1-29区浅层',
        geounitId: '16492963356349999',
        childrens: [
          { geounitName: '南堡1-29断块', geounitId: '16492963526285552' },
          { geounitName: '南堡102X16断块', geounitId: '16492963626283542' },
          { geounitName: '南堡109断块', geounitId: '16492963866285660' },
          { geounitName: '南堡12-162断块', geounitId: '16492964076379162' },
          { geounitName: '南堡12-X63断块', geounitId: '16492964216414121' },
          { geounitName: '南堡12-X66断块', geounitId: '16492964336436862' }
        ]
      },
      {
        geounitName: '南堡1-1中深层',
        geounitId: '16492964816285660',
        childrens: [
          { geounitName: '南堡101X20断块', geounitId: '16492964436301313' },
          { geounitName: '南堡101X2断块', geounitId: '16492964706306862' },
          { geounitName: '南堡101断块', geounitId: '16492964546299339' },
          { geounitName: '南堡116X1断块', geounitId: '16492964546296515' }
        ]
      },
      {
        geounitName: '南堡1-29区中深层',
        geounitId: '16492965166296515',
        childrens: [
          { geounitName: '南堡1-2断块', geounitId: '16492965326395660' }
        ]
      },
      {
        geounitName: '南堡1-3区中深层',
        geounitId: '16492965596321663',
        childrens: [
          { geounitName: '南堡1-3断块', geounitId: '16492965706289999' }
        ]
      },
      {
        geounitName: '南堡1-5区中深层',
        geounitId: '16492968146292760',
        childrens: [
          { geounitName: '南堡1-7断块', geounitId: '16492965816305660' },
          { geounitName: '南堡105X1断块', geounitId: '16492965936306515' },
          { geounitName: '南堡1-5断块', geounitId: '16492966066299162' },
          { geounitName: '南堡1090断块', geounitId: '16492966216306515' },
          { geounitName: '南堡13-X1102断块', geounitId: '16492966316299999' },
          { geounitName: '南堡13-X1150断块', geounitId: '16492966416325552' },
          { geounitName: '南堡105X5断块', geounitId: '16492966606353542' },
          { geounitName: '南堡13-X1067断块', geounitId: '16492966606356515' },
          { geounitName: '南堡105断块', geounitId: '16492967796303542' },
          { geounitName: '南堡105X3断块', geounitId: '16492967796305552' }
        ]
      },
      {
        geounitName: '南堡2-1区',
        geounitId: '16492968286326515',
        childrens: [
          { geounitName: '南堡206断块', geounitId: '16492967956289999' },
          { geounitName: '南堡208断块', geounitId: '16492967956286515' },
          { geounitName: '南堡23-2404断块', geounitId: '16492968366292761' },
          { geounitName: '南堡23-2420断块', geounitId: '16492968466345660' }
        ]
      },
      {
        geounitName: '南堡2-3区中深层',
        geounitId: '16492968596356515',
        childrens: [
          { geounitName: '南堡23-X2215断块', geounitId: '16492968696293542' },
          { geounitName: '南堡23-X2211断块', geounitId: '16492968696295552' },
          { geounitName: '南堡23-X2282断块', geounitId: '16492968696299999' },
          { geounitName: '南堡23-2617断块', geounitId: '16492968696296515' },
          { geounitName: '南堡23-X2298断块', geounitId: '16492968696297460' },
          { geounitName: '南堡2-3断块', geounitId: '16492968696297461' }
        ]
      },
      {
        geounitName: '南堡3-2区中深层',
        geounitId: '16492969126315552',
        childrens: [
          { geounitName: '南堡3-2区中深层断块', geounitId: '16492969126315553' }
        ]
      },
      {
        geounitName: '南堡4-1区中深层',
        geounitId: '16492969266319999',
        childrens: [
          { geounitName: '堡古1断块', geounitId: '16492969266319998' }
        ]
      },
      {
        geounitName: '堡古2区块',
        geounitId: '16492969506293542',
        childrens: [
          { geounitName: '南堡306X1断块', geounitId: '16492969506295552' },
          { geounitName: '南堡306X2断块', geounitId: '16492969506299999' },
          { geounitName: '南堡306X3断块', geounitId: '16492969506296515' },
          { geounitName: '南堡3-81断块', geounitId: '16492969506297460' },
          { geounitName: '南堡3612断块', geounitId: '16492969506297461' },
          { geounitName: '南堡306X6断块', geounitId: '16492969506297462' }
        ]
      },
      {
        geounitName: '南堡4-3区中深层',
        geounitId: '16492969866323542',
        childrens: [
          { geounitName: '南堡403-1断块', geounitId: '16492969866323543' }
        ]
      },
      {
        geounitName: '南堡5-11区中深层',
        geounitId: '16492970046355552',
        childrens: [
          { geounitName: '南堡5-11区中深层断块', geounitId: '16492970046355553' }
        ]
      },
      {
        geounitName: '南堡1-1区浅层',
        geounitId: '16492970216339998',
        childrens: [
          { geounitName: '南堡1-32浅层断块', geounitId: '16492970216339997' }
        ]
      },
      {
        geounitName: '南堡1-3区浅层',
        geounitId: '16492970386296515',
        childrens: [
          { geounitName: '南堡1-3区浅层断块', geounitId: '16492970386296516' }
        ]
      },
      {
        geounitName: '南堡2-3区浅层',
        geounitId: '16492972346315552',
        childrens: [
          { geounitName: '南堡2-3区浅层断块', geounitId: '16492972346315553' }
        ]
      },
      {
        geounitName: '南堡2号潜山',
        geounitId: '16492970556314121',
        childrens: [
          { geounitName: '老堡南1断块', geounitId: '16492970556314122' }
        ]
      },
      {
        geounitName: '南堡3-2区浅层',
        geounitId: '16492970706293542',
        childrens: [
          { geounitName: '南堡3-2区浅层断块', geounitId: '16492970706293543' }
        ]
      },
      {
        geounitName: '南堡4-2区浅层',
        geounitId: '16492970866285552',
        childrens: [
          { geounitName: '南堡4号浅层断块', geounitId: '16492970866285553' }
        ]
      },
      {
        geounitName: '南堡1-29区其它',
        geounitId: '16492972156282760',
        childrens: [
          { geounitName: '南堡1-29区其它断块', geounitId: '16492972156282761' }
        ]
      },
      {
        geounitName: '南堡1-5区其它',
        geounitId: '16492971026281792',
        childrens: [
          { geounitName: '南堡1-68断块', geounitId: '16492971026281793' },
          { geounitName: '南堡1-5区其它断块', geounitId: '16492971026281794' }
        ]
      },
      {
        geounitName: '南堡1号其它',
        geounitId: '16492971846291792',
        childrens: [
          { geounitName: '南堡1-3区其它断块', geounitId: '16492971846291793' },
          { geounitName: '南堡1-32其它断块', geounitId: '16492971846291794' },
          { geounitName: '南堡1-89断块', geounitId: '16492971846291795' }
        ]
      },
      {
        geounitName: '南堡2号其它',
        geounitId: '16492971316395550',
        childrens: [
          { geounitName: '南堡2号其它断块', geounitId: '16492971316395551' }
        ]
      },
      {
        geounitName: '南堡3号其它',
        geounitId: '16492971316395557',
        childrens: [
          { geounitName: '南堡3号其它断块', geounitId: '16492971316395552' }
        ]
      },
      {
        geounitName: '南堡4号其它',
        geounitId: '16492971316395556',
        childrens: [
          { geounitName: '南堡4号其它断块', geounitId: '16492971316395553' }
        ]
      },
      {
        geounitName: '南堡5号其它',
        geounitId: '16492971316395555',
        childrens: [
          { geounitName: '南堡5号其它断块', geounitId: '16492971316395554' }
        ]
      }
    ]
  }
]
