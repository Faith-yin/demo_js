
/**
 * 索引类型是 TypeScript 中的常见类型，它是聚合多个元素的类型，对象、类、元组等都是索引类型
 * https://mp.weixin.qq.com/s/36x-zQ2e0ZcVam0YdpR7_A
 */

type MyPerson = {
	name: string;
	age: number;
	hobbies: string[]
}

// 1. 给每个索引加上 readonly 修饰
type ToReadonly<Obj> = {
	readonly [Key in keyof Obj]: Obj[Key]
}

type res = ToReadonly<MyPerson>

// 2. 把每个索引给分开，处理映射类型为联合类型
type SplitObj<Obj> = {
	[Key in keyof Obj]: {
		[Keys2 in Key]: Obj[Keys2]
	}
}[keyof Obj]

type res2 = SplitObj<MyPerson>

// 3. 把每个索引给分开，处理映射类型为联合类型，带路径
type Template = {
	aa: string
	bb: {
		cc: {
			dd: string
		}
	},
	ee: {
		ff: string
		gg: number
	}
}

type DFS<Obj> = {
	[Key in keyof Obj]: Key
}[keyof Obj]

type res3 = DFS<Template>

type DFS2<Obj> = {
	[Key in keyof Obj]:
		Key extends string
			? Obj[Key] extends Record<string, any>
				? Key | `${Key}.${DFS2<Obj[Key]>}`
				: Key
		: never
}[keyof Obj]

type res4 = DFS2<Template>





